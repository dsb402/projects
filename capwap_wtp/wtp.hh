#include "TxRx.hh"
#include "wtp_sm.hh"

typedef struct {
	char *msg;
	short offset;
	short data_msgType;
} CWProtocolMessage;

typedef struct {
	int vendorIdentifier;
	/*enum {
		CW_WTP_MODEL_NUMBER	= 0,
		CW_WTP_SERIAL_NUMBER = 1,
		CW_BOARD_ID		= 2,
		CW_BOARD_REVISION	= 3,

		CW_WTP_HARDWARE_VERSION	= 0,
		CW_WTP_SOFTWARE_VERSION	= 1,
		CW_BOOT_VERSION		= 2
	}*/
	int type;
	int length;
	char *valuePtr;
} CWWTPVendorInfoValues;

typedef struct  {
	int vendorInfosCount;
	CWWTPVendorInfoValues *vendorInfos;
} CWWTPVendorInfos;

typedef struct {
	unsigned int radioID;
	//Elena Agostini - 07/2014
	unsigned int radioType;
} CWWTPRadioInfoValues;

typedef struct {
	int radioCount;
	CWWTPRadioInfoValues *radiosInfo;
} CWWTPRadiosInfo;

typedef struct {
	char RSSI;
	char SNR;
	int dataRate;
} CWBindingTransportHeaderValues;

typedef struct {
	int payloadType;
	int type;
	int isFragment;
	int last;
	int fragmentID;
	int fragmentOffset;
	int keepAlive;
	CWBindingTransportHeaderValues *bindingValuesPtr;
} CWProtocolTransportHeaderValues;

typedef struct {
	unsigned int messageTypeValue;
	unsigned char seqNum;
	unsigned short msgElemsLen;
//	unsigned int timestamp;
} CWControlHeaderValues;

typedef enum {
	CW_PRESHARED = 4,
	CW_X509_CERTIFICATE = 2
} CWAuthSecurity;

typedef struct {
	int vendorIdentifier;
	/*enum {
		CW_AC_HARDWARE_VERSION	= 4,
		CW_AC_SOFTWARE_VERSION	= 5
	} */
	int type;
	int length;
	int *valuePtr;
} CWACVendorInfoValues;

typedef struct  {
	int vendorInfosCount;
	CWACVendorInfoValues *vendorInfos;
} CWACVendorInfos;

typedef struct {
	int WTPCount;
	struct sockaddr_in addr;
} CWProtocolIPv4NetworkInterface;

typedef struct {
	int WTPCount;
	struct sockaddr_in6 addr;
} CWProtocolIPv6NetworkInterface;

typedef struct {
        int stations;
        int limit;
        int activeWTPs;
        int maxWTPs;
        int security;
        int RMACField;
        int WirelessField;
        int DTLSPolicy;
        CWACVendorInfos vendorInfos;
        char *name;
        CWProtocolIPv4NetworkInterface *IPv4Addresses;
        int IPv4AddressesCount;
        CWProtocolIPv6NetworkInterface *IPv6Addresses;
        int IPv6AddressesCount;
        /*ACIPv4ListValues ACIPv4ListInfo;
        ACIPv6ListValues ACIPv6ListInfo;
        CWNetworkLev4Address preferredAddress;
        CWNetworkLev4Address incomingAddress;*/
} CWACInfoValues;

typedef struct {
	void * bindingValues;
	/*Update 2009:
		add new non-binding specific values*/
	void * protocolValues;
} CWProtocolConfigurationUpdateRequestValues;

/*typedef enum {
	CW_PROTOCOL_SUCCESS				= 0, //	Success
	CW_PROTOCOL_FAILURE_AC_LIST			= 1, // AC List message MUST be present
	CW_PROTOCOL_SUCCESS_NAT				= 2, // NAT detected
	CW_PROTOCOL_FAILURE				= 3, // unspecified
	CW_PROTOCOL_FAILURE_RES_DEPLETION		= 4, // Resource Depletion
	CW_PROTOCOL_FAILURE_UNKNOWN_SRC			= 5, // Unknown Source
	CW_PROTOCOL_FAILURE_INCORRECT_DATA		= 6, // Incorrect Data
	CW_PROTOCOL_FAILURE_ID_IN_USE			= 7, // Session ID Alreadyin Use
	CW_PROTOCOL_FAILURE_WTP_HW_UNSUPP		= 8, // WTP Hardware not supported
	CW_PROTOCOL_FAILURE_BINDING_UNSUPP		= 9, // Binding not supported
	CW_PROTOCOL_FAILURE_UNABLE_TO_RESET		= 10, // Unable to reset
	CW_PROTOCOL_FAILURE_FIRM_WRT_ERROR		= 11, // Firmware write error
	CW_PROTOCOL_FAILURE_SERVICE_PROVIDED_ANYHOW	= 12, // Unable to apply requested configuration
	CW_PROTOCOL_FAILURE_SERVICE_NOT_PROVIDED	= 13, // Unable to apply requested configuration
	CW_PROTOCOL_FAILURE_INVALID_CHECKSUM		= 14, // Image Data Error: invalid checksum
	CW_PROTOCOL_FAILURE_INVALID_DATA_LEN		= 15, // Image Data Error: invalid data length
	CW_PROTOCOL_FAILURE_OTHER_ERROR			= 16, // Image Data Error: other error
	CW_PROTOCOL_FAILURE_IMAGE_ALREADY_PRESENT	= 17, // Image Data Error: image already present
	CW_PROTOCOL_FAILURE_INVALID_STATE		= 18, // Message unexpected: invalid in current state
	CW_PROTOCOL_FAILURE_UNRECOGNIZED_REQ		= 19, // Message unexpected: unrecognized request
	CW_PROTOCOL_FAILURE_MISSING_MSG_ELEM		= 20, // Failure: missing mandatory message element
	CW_PROTOCOL_FAILURE_UNRECOGNIZED_MSG_ELEM	= 21  // Failure: unrecognized message element

} CWProtocolResultCode;*/



typedef struct {
	CWACInfoValues ACInfoPtr;
	int code;
	//ACIPv4ListValues ACIPv4ListInfo;
	//ACIPv6ListValues ACIPv6ListInfo;
	/*
	 * Elena Agostini - 02/2014
	 *
	 * ECN Support Msg Elem MUST be included in Join Request/Response Messages
	 */
	int ECNSupport;
} CWProtocolJoinResponseValues;

typedef struct {
	//ACIPv4ListValues ACIPv4ListInfo;
	//ACIPv6ListValues ACIPv6ListInfo;
	int discoveryTimer;
	int echoRequestTimer;
	int radioOperationalInfoCount;
	//CWRadioOperationalInfoValues *radioOperationalInfo;
	//WTPDecryptErrorReport radiosDecryptErrorPeriod;
	int idleTimeout;
	int fallback;
	void * bindingValues;
	int resultCode;
} CWProtocolConfigureResponseValues;


/*typedef enum {
	CW_RESERVE = 1,
	CW_LOCAL_BRIDGING = 2,
	CW_802_DOT_3_BRIDGING = 4,
	CW_NATIVE_BRIDGING = 8,
	CW_ALL_ENC = 15
} CWframeTunnelMode;*/

/*typedef enum {
	CW_LOCAL_MAC = 0,
	CW_SPLIT_MAC = 1,
	CW_BOTH = 2
} CWMACType;*/

enum {
		CW_802_DOT_11b = 1,
		CW_802_DOT_11a = 2,
		CW_802_DOT_11g = 4,
		CW_802_DOT_11n = 8,
		CW_ALL_RADIO_TYPES = 0x0F
	} type;


#define CW_CONTROL_PORT 5246
#define CW_MAX_SEQ_NUM 255
#define CW_MSG_ELEMENT_DISCOVERY_TYPE_CW_TYPE 20
#define CW_WTP_MODEL_NUMBER 0;
#define CW_WTP_SERIAL_NUMBER 1;
#define CW_MSG_ELEMENT_WTP_BOARD_DATA_CW_TYPE 38
#define CW_WTP_HARDWARE_VERSION 0
#define CW_WTP_SOFTWARE_VERSION 1
#define CW_BOOT_VERSION 2
#define CW_MSG_ELEMENT_WTP_FRAME_TUNNEL_MODE_CW_TYPE 41
#define CW_MSG_ELEMENT_WTP_DESCRIPTOR_CW_TYPE 39
const int CW_MAX_RADIO = 2;
const int CW_MAX_RADIO_IN_USE = 2;
const int CW_MSG_ELEMENT_DISCOVERY_TYPE_BROADCAST = 0;
const int CW_LOCAL_BRIDGING = 2;
const int CW_LOCAL_MAC = 0;
#define CW_MSG_ELEMENT_WTP_MAC_TYPE_CW_TYPE	44
#define CW_MSG_ELEMENT_IEEE80211_WTP_RADIO_INFORMATION_CW_TYPE 1048
#define CW_MSG_ELEMENT_VENDOR_PAYLOAD_TYPE_CW_TYPE 37
#define	CW_PACKET_PLAIN	0
#define		CW_MSG_TYPE_VALUE_DISCOVERY_REQUEST				1
#define 	CW_MSG_TYPE_VALUE_DISCOVERY_RESPONSE 			2
#define  	CW_MSG_TYPE_VALUE_JOIN_REQUEST  				3
#define		CW_MSG_TYPE_VALUE_JOIN_RESPONSE					4
#define		CW_MSG_TYPE_VALUE_CONFIGURE_REQUEST				5
#define		CW_MSG_TYPE_VALUE_CONFIGURE_RESPONSE			6
#define		CW_MSG_TYPE_VALUE_CONFIGURE_UPDATE_REQUEST		7
#define		CW_MSG_TYPE_VALUE_CONFIGURE_UPDATE_RESPONSE		8
#define 	CW_MSG_TYPE_VALUE_WTP_EVENT_REQUEST				9
#define 	CW_MSG_TYPE_VALUE_WTP_EVENT_RESPONSE			10
#define CW_IEEE_802_3_FRAME_TYPE 4
#define CW_DATA_MSG_KEEP_ALIVE_TYPE 5
const int CW_CONTROL_HEADER_OFFSET_FOR_MSG_ELEMS = 3;
static const int gMaxCAPWAPHeaderSize = 8;
#define CW_TRANSPORT_HEADER_VERSION_START 0
#define CW_TRANSPORT_HEADER_VERSION_LEN	4
#define CW_PROTOCOL_VERSION	0
#define	CW_TRANSPORT_HEADER_TYPE_START 4
#define	CW_TRANSPORT_HEADER_TYPE_LEN 4

// Length of CAPWAP tunnel header in 4 byte words
#define CW_TRANSPORT_HEADER_HLEN_START 8
#define CW_TRANSPORT_HEADER_HLEN_LEN 5

// Radio ID number (for WTPs with multiple radios)
#define CW_TRANSPORT_HEADER_RID_START 13
#define CW_TRANSPORT_HEADER_RID_LEN	5

// Wireless Binding ID
#define CW_TRANSPORT_HEADER_WBID_START 18
#define CW_TRANSPORT_HEADER_WBID_LEN 5

// Format of the frame
#define CW_TRANSPORT_HEADER_T_START	23
#define CW_TRANSPORT_HEADER_T_LEN 1

// Is a fragment?
#define CW_TRANSPORT_HEADER_F_START	24
#define CW_TRANSPORT_HEADER_F_LEN 1

// Is NOT the last fragment?
#define CW_TRANSPORT_HEADER_L_START	25
#define CW_TRANSPORT_HEADER_L_LEN 1

// Is the Wireless optional header present?
#define CW_TRANSPORT_HEADER_W_START	26
#define CW_TRANSPORT_HEADER_W_LEN 1

// Is the Radio MAC Address optional field present?
#define CW_TRANSPORT_HEADER_M_START	27
#define CW_TRANSPORT_HEADER_M_LEN 1

// Is the message a keep alive?
#define CW_TRANSPORT_HEADER_K_START	28
#define CW_TRANSPORT_HEADER_K_LEN 1

// Set to 0 in this version of the protocol
#define CW_TRANSPORT_HEADER_FLAGS_START	29
#define CW_TRANSPORT_HEADER_FLAGS_LEN 3

// ID of the group of fragments
#define CW_TRANSPORT_HEADER_FRAGMENT_ID_START 0
#define CW_TRANSPORT_HEADER_FRAGMENT_ID_LEN	16

// Position of this fragment in the group
#define CW_TRANSPORT_HEADER_FRAGMENT_OFFSET_START 16
#define CW_TRANSPORT_HEADER_FRAGMENT_OFFSET_LEN	13

// Set to 0 in this version of the protocol
#define CW_TRANSPORT_HEADER_RESERVED_START 29
#define CW_TRANSPORT_HEADER_RESERVED_LEN 3

#define CW_BUFFER_SIZE 2000

// Message Elements Type Values
#define CW_MSG_ELEMENT_AC_DESCRIPTOR_CW_TYPE 1
#define CW_MSG_ELEMENT_AC_NAME_CW_TYPE	4
#define CW_MSG_ELEMENT_CW_CONTROL_IPV4_ADDRESS_CW_TYPE	10
#define CW_MSG_ELEMENT_CW_CONTROL_IPV6_ADDRESS_CW_TYPE	11

// IEEE 802.11 Message Element
#define 	CW_MSG_ELEMENT_IEEE80211_WTP_RADIO_INFORMATION_CW_TYPE			1048

#define CWSetField32(src,start,len,val)	src |= ((~(0xFFFFFFFF << len)) & val) << (32 - start - len)
#define CWGetField32(src,start,len)	((~(0xFFFFFFFF<<len)) & (src >> (32 - start - len)))
#define		CWParseMessageElementStart()				int oldOffset;												\
														if(msgPtr == NULL || valPtr == NULL) return false;	\
														oldOffset = msgPtr->offset

#define		CWParseMessageElementEnd()					return ((msgPtr->offset) - oldOffset) == len ? true :false;


class wtp
{    
    private:
    wtpContext _fsm;
    int disc_interval = 5;
    int max_disc_interval= 20;
    int max_disc_count= 10;
    int silent_interval = 30;
    int disc_count;
    int socket_c;
    CWACInfoValues gACInfoPtr;
    CWACInfoValues *ACInfoPtr = new CWACInfoValues();
    CWProtocolJoinResponseValues *valuesPtr = new CWProtocolJoinResponseValues();
    CWProtocolConfigureResponseValues *configValuesPtr = new CWProtocolConfigureResponseValues();
    pthread_t thread;
    public:
    wtp();
    void start();
    void outputErrorLog();
    void errorCheck();
    void resetDiscoveryCount();
    void clearAcInfo();
    void discReq();
    void setDiscIntervalTimer();
    void incrementDiscoveryCount();
    void discReqToNotRespondingAcs();
    void startSilentIntervalTimer();
    void ignoreCapwapMessag();
    void sendJoinReqtoSelectedAc();
    void sendConfigStatusReq();
   // void sendChangeStateEventReq();
    void startDataChannelKeepAliveTimer();
    void sendKeepAlivePacket();
    void startEchoIntervalTimer();
    void startDataChannelDeadIntervalTimer();
   // void sendChangeReq_or_Resp();
    void configUpdateResponse();
    void sendWtpReq();
   // void sendDataTransferReqorResp();
    //void resetEchoIntervalTimer();
   // void sendClearConfig
    void sendStationConfigResp();
    void ignoreCapwapMessage();
    bool CWParseDiscoveryResponseMessage(char *msg,int len,int *seqNumPtr);
    bool CWParseJoinResponseMessage(char *msg,int len,int *seqNumPtr);
    bool CWParseResultCode(CWProtocolMessage *msgPtr, int len, int *valPtr);
    bool CWParseConfigStatusResponseMessage(char *msg,int len,int *seqNumPtr);
};
