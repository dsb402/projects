#include "wtp_cfg_api.hh"
extern "C"
{
#include <uci.h>
}


#if 0
template<class T> inline T operator~ (T a) { return (T)~(int)a; }
template<class T> inline T operator| (T a, T b) { return (T)((int)a | (int)b); }
template<class T> inline T operator& (T a, T b) { return (T)((int)a & (int)b); }
template<class T> inline T operator^ (T a, T b) { return (T)((int)a ^ (int)b); }
template<class T> inline T& operator|= (T& a, T b) { return (T&)((int&)a |= (int)b); }
template<class T> inline T& operator&= (T& a, T b) { return (T&)((int&)a &= (int)b); }
template<class T> inline T& operator^= (T& a, T b) { return (T&)((int&)a ^= (int)b); }
#endif

struct uci_type_list {
	unsigned int idx;
	const char *name;
	struct uci_type_list *next;
};

class WtpWirelessDB
{
    static WtpWirelessDB *pWtpWirelessDB;
    struct uci_type_list *type_list = NULL;

        WtpWirelessDB()
        {
            pUciCtx = uci_alloc_context();
            pUciPkg = NULL;
				cout << "enter pWtpWirelessDB constructor" << endl;     //for debugging:  charles
        };

    public:
        const string  DevTypeName = "wifi-device";
        const string  DevIfName   = "wifi-iface";
        struct uci_context  *pUciCtx;
        struct uci_package  *pUciPkg;
		  
        static WtpWirelessDB *getInstance()
        {   if(pWtpWirelessDB==nullptr)
			   {				
                pWtpWirelessDB = new WtpWirelessDB();
            }
            return pWtpWirelessDB;
        };
		  
        ~WtpWirelessDB()
        {
			   cout << "enter WtpWirelessDB destructor" << endl;     //for debugging:  charles
            if(pWtpWirelessDB==NULL)
            {
                delete pWtpWirelessDB;
            }
            pWtpWirelessDB = NULL;
            if(pUciCtx!=NULL)
            {
                uci_free_context(pUciCtx);
            }
            pUciCtx = NULL;
        };

        //package load and commit
        bool load_wireless_pkg();
        bool commit_wireless_pkg();
        //for assist session load update
        void wtp_reset_typelist(void);
        void wtp_lookup_section_ref(struct uci_section *s, string &sPath);		 
        bool load_device_session(WtpWiFiDeviceSession &apiDevSec, struct uci_section *pUciSec);
        bool load_iface_session(WtpWiFiIfaceSession &apiIfSec, struct uci_section *pUciSec);
        //for option parameter set and get
        bool wtp_lookup_ptr(struct uci_ptr *ptr, const string &sPath);        //uci_lookup_ptr()
        bool wtp_ptr_set(struct uci_ptr *ptr, const char *pVal);      //uci_set()
};





