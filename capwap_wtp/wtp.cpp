#include <iostream>
#include "wtp_ie_api.hh"
#include "linux/types.h"
#include "wtp.hh"
#include "unistd.h"

#include <stdio.h>
#include <stdlib.h>
#include <sstream>
#include <string>
#include <pthread.h>



using namespace std;
using namespace statemap;

wtp::wtp():_fsm(*this){}

__inline__ unsigned int CWGetSeqNum()
{
	static unsigned int seqNum = 0;

	if (seqNum==CW_MAX_SEQ_NUM) seqNum=0;
	else seqNum++;
	return seqNum;
}

__inline__ int CWWTPGetEncCapabilities() {
	return 0;
}

// stores 16 bits in the message, increments the current offset in bytes
void CWProtocolStore16(CWProtocolMessage *msgPtr, unsigned short val) {
	val = htons(val);
	memcpy(msgPtr->msg + msgPtr->offset, &(val), 2);
	(msgPtr->offset) += 2;
}

// stores 32 bits in the message, increments the current offset in bytes
void CWProtocolStore32(CWProtocolMessage *msgPtr, unsigned int val) {
	val = htonl(val);
	memcpy(msgPtr->msg + msgPtr->offset, &(val), 4);
	(msgPtr->offset) += 4;
}

// retrieves 8 bits from the message, increments the current offset in bytes.
unsigned char CWProtocolRetrieve8(CWProtocolMessage *msgPtr) {
	unsigned char val;

	memcpy(&val,msgPtr->msg + msgPtr->offset,1);
	(msgPtr->offset) += 1;

	return val;
}

// retrieves 16 bits from the message, increments the current offset in bytes.
unsigned short CWProtocolRetrieve16(CWProtocolMessage *msgPtr) {
	unsigned short val;

	memcpy(&val,msgPtr->msg + msgPtr->offset,2);
	(msgPtr->offset) += 2;

	return ntohs(val);
}

unsigned int CWProtocolRetrieve32(CWProtocolMessage *msgPtr) {
	unsigned int val;

	memcpy(&val,msgPtr->msg + msgPtr->offset,4);
	(msgPtr->offset) += 4;

	return ntohl(val);
}

// retrieves len bytes from the message, increments the current offset in bytes.
char *CWProtocolRetrieveRawBytes(CWProtocolMessage *msgPtr, int len) {
	char *bytes;

	bytes = new char[len];
	//CW_CREATE_OBJECT_SIZE_ERR(bytes, len, return NULL;);

	memcpy(bytes, msgPtr->msg +msgPtr->offset, len);
	(msgPtr->offset) += len;

	return bytes;
}

// retrieves a string (not null-terminated) from the message, increments the current offset in bytes.
// Adds the '\0' char at the end of the string which is returned
char *CWProtocolRetrieveStr(CWProtocolMessage *msgPtr, int len) {
	char *str;

	str = new char[len+1];

	memcpy(str, msgPtr->msg + msgPtr->offset, len);
	str[len] = '\0';
	(msgPtr->offset) += len;

	return str;
}

// Assembles the Control Header
bool CWAssembleControlHeader(CWProtocolMessage *controlHdrPtr, CWControlHeaderValues *valPtr)
{
	if(controlHdrPtr == NULL || valPtr == NULL)
		return 0;
	controlHdrPtr->msg = new char[8];
	controlHdrPtr->offset = 0;

	//CW_CREATE_PROTOCOL_MESSAGE(*controlHdrPtr, 8,	 // meaningful bytes of the headerreturn CWErrorRaise(CW_ERROR_OUT_OF_MEMORY, NULL););
	//CWProtocolStore32(controlHdrPtr, valPtr->messageTypeValue);
	CWProtocolStore32(controlHdrPtr,valPtr->messageTypeValue);
	//CWProtocolStore8(controlHdrPtr, valPtr->seqNum);
	memcpy(controlHdrPtr->msg+controlHdrPtr->offset,&valPtr->seqNum,1);
	controlHdrPtr->offset += 1;
	int y = CW_CONTROL_HEADER_OFFSET_FOR_MSG_ELEMS+valPtr->msgElemsLen;
	//cout <<y<<endl;
	CWProtocolStore16(controlHdrPtr,y);
	//CWProtocolStore16(controlHdrPtr, (CW_CONTROL_HEADER_OFFSET_FOR_MSG_ELEMS+(valPtr->msgElemsLen))); // 7 is for the next 8+32+16 bits (= 7 bytes), MessageElementsLength+flags + timestamp
	//CWProtocolStore8(controlHdrPtr, 0); // flags
	y = 0;
	memcpy(controlHdrPtr->msg+controlHdrPtr->offset,&y,1);
	controlHdrPtr->offset += 1;
	//CWProtocolStore32(controlHdrPtr, ((unsigned int)(time(NULL))) ); // timestamp
	//cout<<"end of header assembly"<<endl;
	return true;
}

// Assembles the Transport Header
bool CWAssembleTransportHeader(CWProtocolMessage *transportHdrPtr, CWProtocolTransportHeaderValues *valuesPtr)
{
	//cout <<"beginning of transport headet assembly"<<endl;
	char radio_mac_present = 0;
	//int i;

	/*for(i=0;i<6;i++){
		//printf(":::: %02X\n",gRADIO_MAC[i]);
		if( gRADIO_MAC[i]!=0 ) {
			radio_mac_present = 8;
			break;
		}
	}*/

	unsigned int val = 0;
	if(transportHdrPtr == NULL || valuesPtr == NULL)
		return 0;

	/*if(valuesPtr->bindingValuesPtr != NULL)
		{CW_CREATE_PROTOCOL_MESSAGE(*transportHdrPtr,gMaxCAPWAPHeaderSizeBinding+radio_mac_present, return CWErrorRaise(CW_ERROR_OUT_OF_MEMORY, NULL););}
	else {}*/

	transportHdrPtr->msg = new char[8 + radio_mac_present];
	transportHdrPtr->offset = 0;
	//CW_CREATE_PROTOCOL_MESSAGE(*transportHdrPtr,8 + radio_mac_present, return CWErrorRaise(CW_ERROR_OUT_OF_MEMORY, NULL););	 // meaningful bytes of the header (no wirless header and MAC address)
	CWSetField32(val,
		     CW_TRANSPORT_HEADER_VERSION_START,
		     CW_TRANSPORT_HEADER_VERSION_LEN,
		     CW_PROTOCOL_VERSION); // current version of CAPWAP


	CWSetField32(val,
		     CW_TRANSPORT_HEADER_TYPE_START,
		     CW_TRANSPORT_HEADER_TYPE_LEN,
		     (valuesPtr->payloadType == CW_PACKET_PLAIN) ? 0 : 1);

	/*if(radio_mac_present)
		if(valuesPtr->bindingValuesPtr != NULL)
			CWSetField32(val,
					 CW_TRANSPORT_HEADER_HLEN_START,
					 CW_TRANSPORT_HEADER_HLEN_LEN,
					 CW_BINDING_HLEN + 2);
		else
			CWSetField32(val,
					 CW_TRANSPORT_HEADER_HLEN_START,
					 CW_TRANSPORT_HEADER_HLEN_LEN,
					 2 + 2);
	else
		if(valuesPtr->bindingValuesPtr != NULL)
			CWSetField32(val,
					 CW_TRANSPORT_HEADER_HLEN_START,
					 CW_TRANSPORT_HEADER_HLEN_LEN,
					 CW_BINDING_HLEN);
		else*/
			CWSetField32(val,
					 CW_TRANSPORT_HEADER_HLEN_START,
					 CW_TRANSPORT_HEADER_HLEN_LEN,
					 2);

	CWSetField32(val,
		     CW_TRANSPORT_HEADER_RID_START,
		     CW_TRANSPORT_HEADER_RID_LEN,
		     0); // only one radio per WTP?

	CWSetField32(val,
		     CW_TRANSPORT_HEADER_WBID_START,
		     CW_TRANSPORT_HEADER_WBID_LEN,
		     1); // Wireless Binding ID




	/*if(valuesPtr->bindingValuesPtr != NULL)
		CWSetField32(val,
		     CW_TRANSPORT_HEADER_T_START,
		     CW_TRANSPORT_HEADER_T_LEN,
		     1);

	else*/ if(valuesPtr->type==1)
		CWSetField32(val,
		     CW_TRANSPORT_HEADER_T_START,
		     CW_TRANSPORT_HEADER_T_LEN,
		     1);
	else
		CWSetField32(val,
		     CW_TRANSPORT_HEADER_T_START,
		     CW_TRANSPORT_HEADER_T_LEN,
		     0);



	CWSetField32(val,
		     CW_TRANSPORT_HEADER_F_START,
		     CW_TRANSPORT_HEADER_F_LEN,
		     valuesPtr->isFragment); // is fragment

	CWSetField32(val,
		     CW_TRANSPORT_HEADER_L_START,
		     CW_TRANSPORT_HEADER_L_LEN,
		     valuesPtr->last); // last fragment

	/*if(valuesPtr->bindingValuesPtr != NULL)
		CWSetField32(val,
			     CW_TRANSPORT_HEADER_W_START,
			     CW_TRANSPORT_HEADER_W_LEN,
			     1); //wireless header
	else*/
		CWSetField32(val,
			     CW_TRANSPORT_HEADER_W_START,
			     CW_TRANSPORT_HEADER_W_LEN,
			     0);

	/*if(radio_mac_present)
		CWSetField32(val,
				 CW_TRANSPORT_HEADER_M_START,
				 CW_TRANSPORT_HEADER_M_LEN,
				 1); //radio MAC address
	else*/
		CWSetField32(val,
				 CW_TRANSPORT_HEADER_M_START,
				 CW_TRANSPORT_HEADER_M_LEN,
				 0); // no radio MAC address
	CWSetField32(val,
		     CW_TRANSPORT_HEADER_K_START,
		     CW_TRANSPORT_HEADER_K_LEN,
		     0); // Keep alive flag

	CWSetField32(val,
		     CW_TRANSPORT_HEADER_FLAGS_START,
		     CW_TRANSPORT_HEADER_FLAGS_LEN,
		     0); // required
	CWProtocolStore32(transportHdrPtr,val);


	//CWProtocolStore32(transportHdrPtr, val);
	// end of first 32 bits

	val = 0;

	CWSetField32(val,
		     CW_TRANSPORT_HEADER_FRAGMENT_ID_START,
		     CW_TRANSPORT_HEADER_FRAGMENT_ID_LEN,
		     valuesPtr->fragmentID); // fragment ID

	CWSetField32(val,
		     CW_TRANSPORT_HEADER_FRAGMENT_OFFSET_START,
		     CW_TRANSPORT_HEADER_FRAGMENT_OFFSET_LEN,
		     valuesPtr->fragmentOffset); // fragment offset

	CWSetField32(val,
		     CW_TRANSPORT_HEADER_RESERVED_START,
		     CW_TRANSPORT_HEADER_RESERVED_LEN,
		     0); // required

	CWProtocolStore32(transportHdrPtr,val);

	//CWProtocolStore32(transportHdrPtr, val);
	// end of second 32 bits

	/*if(radio_mac_present){
		CWProtocolStore8(transportHdrPtr,6);

		CWThreadMutexLock(&gRADIO_MAC_mutex);
			CWProtocolStoreRawBytes(transportHdrPtr,gRADIO_MAC,6);
		CWThreadMutexUnlock(&gRADIO_MAC_mutex);

		CWProtocolStore8(transportHdrPtr,0);
	}


	if(valuesPtr->bindingValuesPtr != NULL){
		if (!CWAssembleTransportHeaderBinding(transportHdrPtr, valuesPtr->bindingValuesPtr))
			return 0;
	}*/
	//cout <<"end of transport header assembly"<<endl;
	return true;
}


bool CWAssembleMessage(CWProtocolMessage **completeMsgPtr, int *fragmentsNumPtr, int PMTU, int seqNum, int msgTypeValue, CWProtocolMessage *msgElems, const int msgElemNum, CWProtocolMessage *msgElemsBinding, const int msgElemBindingNum, int is_crypted) {
	printf("CWAssembleMessage:Enter");

	CWProtocolMessage transportHdr, controlHdr, msg;
	int msgElemsLen = 0;
	int i;

	CWProtocolTransportHeaderValues transportVal;
	CWControlHeaderValues controlVal;
	printf("CWAssembleMessage:before if");
	if(completeMsgPtr == NULL || fragmentsNumPtr == NULL || (msgElems == NULL && msgElemNum > 0) || (msgElemsBinding == NULL && msgElemBindingNum > 0))
		return 0;
	printf("CWProtocolMessage:after if");
	//Calculate the whole size of the Msg Elements
	for(i = 0; i < msgElemNum; i++) msgElemsLen += msgElems[i].offset;
	for(i = 0; i < msgElemBindingNum; i++) msgElemsLen += msgElemsBinding[i].offset;

	//Assemble Control Header
	controlVal.messageTypeValue = msgTypeValue;
	controlVal.msgElemsLen = msgElemsLen;
	controlVal.seqNum = seqNum;


	if(!(CWAssembleControlHeader(&controlHdr, &controlVal))) {
		delete controlHdr.msg;
			//CW_FREE_PROTOCOL_MESSAGE(controlHdr);
			for(i = 0; i < msgElemNum; i++)
				delete msgElems[i].msg;
			delete msgElems;
			//CW_FREE_OBJECT(msgElems);
			for(i = 0; i < msgElemBindingNum; i++)
				delete msgElemsBinding[i].msg;
				delete msgElemsBinding;
			return 0; // will be handled by the caller
		}
	// assemble the message putting all the data consecutively
		//CW_CREATE_PROTOCOL_MESSAGE(msg, controlHdr.offset + msgElemsLen, return CWErrorRaise(CW_ERROR_OUT_OF_MEMORY, NULL););
	msg.msg = new char[controlHdr.offset + msgElemsLen];
	msg.offset = 0;
	//CWProtocolStoreMessage(&msg, &controlHdr);
	memcpy(msg.msg+msg.offset,controlHdr.msg,controlHdr.offset);
	msg.offset += controlHdr.offset;
	for(i = 0; i < msgElemNum; i++)
		{ // store in the request all the Message Elements
		memcpy(msg.msg+msg.offset,msgElems[i].msg,msgElems[i].offset);
		msg.offset += msgElems[i].offset;
		//CWProtocolStoreMessage(&msg, &(msgElems[i]));
		}
		for(i = 0; i < msgElemBindingNum; i++) { // store in the request all the Message Elements
			memcpy(msg.msg+msg.offset,msgElemsBinding[i].msg,msgElemsBinding[i].offset);
			msg.offset += msgElemsBinding[i].offset;
			//CWProtocolStoreMessage(&msg, &(msgElemsBinding[i]));
		}
		//Free memory not needed anymore
		delete controlHdr.msg;
		//CW_FREE_PROTOCOL_MESSAGE(controlHdr);
		for(i = 0; i < msgElemNum; i++)
			delete msgElems[i].msg;
		//{ CW_FREE_PROTOCOL_MESSAGE(msgElems[i]);}
		//delete msgElems;
		//CW_FREE_OBJECT(msgElems);
		for(i = 0; i < msgElemBindingNum; i++)
		{
			delete msgElemsBinding[i].msg;
		}
		delete msgElemsBinding;
		/*{ CW_FREE_PROTOCOL_MESSAGE(msgElemsBinding[i]);}
		CW_FREE_OBJECT(msgElemsBinding);*/

	//	CWDebugLog("PMTU: %d", PMTU);
		// handle fragmentation
		//PMTU = PMTU - gMaxDTLSHeaderSize - gMaxCAPWAPHeaderSize;
		/*PMTU = PMTU - gMaxCAPWAPHeaderSize;

		if(PMTU > 0) {
			PMTU = (PMTU/8)*8; // CAPWAP fragments are made of groups of 8 bytes
			if(PMTU == 0) goto cw_dont_fragment;

			//CWDebugLog("Aligned PMTU: %d", PMTU);
			*fragmentsNumPtr = msg.offset / PMTU;
			if((msg.offset % PMTU) != 0) (*fragmentsNumPtr)++;
			//CWDebugLog("Fragments #: %d", *fragmentsNumPtr);
		} else {
		cw_dont_fragment:
			*fragmentsNumPtr = 1;
		}*/
		*fragmentsNumPtr = 1;
		transportVal.bindingValuesPtr = NULL;

		if(*fragmentsNumPtr == 1) {
			//CWDebugLog("1 Fragment");

			*completeMsgPtr = new CWProtocolMessage();
			//CW_CREATE_OBJECT_ERR(*completeMsgPtr, CWProtocolMessage, return CWErrorRaise(CW_ERROR_OUT_OF_MEMORY, NULL););

			transportVal.isFragment = transportVal.last = transportVal.fragmentOffset = transportVal.fragmentID = 0;
			transportVal.payloadType = is_crypted;
	//		transportVal.last = 1;

			/*
			 * Elena Agostini - 02/2014
			 *
			 * BUG Valgrind: Missing inizialization
			 */
			transportVal.type = 0;

			// Assemble Message Elements
			if	(!(CWAssembleTransportHeader(&transportHdr, &transportVal))) {
				delete msg.msg;
				//CW_FREE_PROTOCOL_MESSAGE(msg);
				//CW_FREE_PROTOCOL_MESSAGE(transportHdr);
				delete transportHdr.msg;
				//CW_FREE_OBJECT(completeMsgPtr);
				delete completeMsgPtr;
				return 0; // will be handled by the caller
			}

			// assemble the message putting all the data consecutively
			(*completeMsgPtr)[0].msg = new char[transportHdr.offset + msg.offset];
			(*completeMsgPtr)[0].offset = 0;

			//CW_CREATE_PROTOCOL_MESSAGE(((*completeMsgPtr)[0]), transportHdr.offset + msg.offset, return CWErrorRaise(CW_ERROR_OUT_OF_MEMORY, NULL););

			memcpy((*completeMsgPtr)[0].msg + (*completeMsgPtr)[0].offset,transportHdr.msg,transportHdr.offset);
			(*completeMsgPtr)[0].offset += transportHdr.offset;
			memcpy((*completeMsgPtr)[0].msg + (*completeMsgPtr)[0].offset,msg.msg,msg.offset);
			(*completeMsgPtr)[0].offset += msg.offset;
			/*CWProtocolStoreMessage(&((*completeMsgPtr)[0]), &transportHdr);
			CWProtocolStoreMessage(&((*completeMsgPtr)[0]), &msg);*/

			delete transportHdr.msg;
			delete msg.msg;

			/*CW_FREE_PROTOCOL_MESSAGE(transportHdr);
			CW_FREE_PROTOCOL_MESSAGE(msg);*/
		} /*else {
			int fragID = CWGetFragmentID();
			int totalSize = msg.offset;
			//CWDebugLog("%d Fragments", *fragmentsNumPtr);

			CW_CREATE_PROTOCOL_MSG_ARRAY_ERR(*completeMsgPtr, *fragmentsNumPtr, return CWErrorRaise(CW_ERROR_OUT_OF_MEMORY, NULL););
			msg.offset = 0;

			for(i = 0; i < *fragmentsNumPtr; i++) { // for each fragment to assemble
				int fragSize;

				transportVal.isFragment = 1;
				transportVal.fragmentOffset = msg.offset / 8;
				transportVal.fragmentID = fragID;
				transportVal.payloadType = is_crypted;

				if(i < ((*fragmentsNumPtr)-1)) { // not last fragment
					fragSize = PMTU;
					transportVal.last = 0;
				} else { // last fragment
					fragSize = totalSize - (((*fragmentsNumPtr)-1) * PMTU);
					transportVal.last = 1;
				}

				//CWDebugLog("Fragment #:%d, offset:%d, bytes stored:%d/%d", i, transportVal.fragmentOffset, fragSize, totalSize);

				// Assemble Transport Header for this fragment
				if(!(CWAssembleTransportHeader(&transportHdr, &transportVal))) {
					CW_FREE_PROTOCOL_MESSAGE(msg);
					CW_FREE_PROTOCOL_MESSAGE(transportHdr);
					CW_FREE_OBJECT(completeMsgPtr);
					return CW_FALSE; // will be handled by the caller
				}

				CW_CREATE_PROTOCOL_MESSAGE(((*completeMsgPtr)[i]), transportHdr.offset + fragSize, return CWErrorRaise(CW_ERROR_OUT_OF_MEMORY, NULL););

				CWProtocolStoreMessage(&((*completeMsgPtr)[i]), &transportHdr);
				CWProtocolStoreRawBytes(&((*completeMsgPtr)[i]), &((msg.msg)[msg.offset]), fragSize);
				msg.offset += fragSize;

				CW_FREE_PROTOCOL_MESSAGE(transportHdr);
			}
			CW_FREE_PROTOCOL_MESSAGE(msg);
		}*/
	return true;
}

bool CWAssembleMsgElem(CWProtocolMessage *msgPtr, unsigned int type) {
	CWProtocolMessage completeMsg;
	completeMsg.offset = 0;
	//cout<<msgPtr->offset<<endl;

	if(msgPtr == NULL)
		return 0;

	printf("***CWAssembleMsgElem line=%d ----enter\n", __LINE__);
	completeMsg.msg =  new char[6+(msgPtr->offset)];
	//CW_CREATE_PROTOCOL_MESSAGE(completeMsg, 6+(msgPtr->offset), return CWErrorRaise(CW_ERROR_OUT_OF_MEMORY, NULL););

	printf("***CWAssembleMsgElem line=%d ----after alloc\n", __LINE__);
	// store header
	//CWProtocolStore16(&completeMsg, type);
	CWProtocolStore16(&completeMsg,type);
	printf("***CWAssembleMsgElem line=%d ----put message type\n", __LINE__);
	//CWProtocolStore16(&completeMsg, msgPtr->offset); // size of the body
	CWProtocolStore16(&completeMsg,msgPtr->offset);

	printf("***CWAssembleMsgElem line=%d\n", __LINE__);
	// store body
	//CWProtocolStoreMessage(&completeMsg, msgPtr);
	memcpy((completeMsg.msg+completeMsg.offset),msgPtr->msg,msgPtr->offset);
	completeMsg.offset += msgPtr->offset;

	printf("***CWAssembleMsgElem line=%d\n", __LINE__);
	delete msgPtr->msg;
	//msgPtr->offset = 0;
	//CW_FREE_PROTOCOL_MESSAGE(*msgPtr);

	//msgPtr->msg = new char[completeMsg.offset];
	printf("***CWAssembleMsgElem line=%d\n", __LINE__);
	msgPtr->msg = completeMsg.msg;
	msgPtr->offset = completeMsg.offset;
	/*
	 * Elena Agostini - 02/2014
	 *
	 * BUG Valgrind: if msgPtr->data_Type is not initialized here,
	 * it may be a problem in CWBinding.c:84
	 */
	//cout<<msgPtr->offset<<endl;
	msgPtr->data_msgType = type;
	printf("***CWAssembleMsgElem line=%d ----exit\n", __LINE__);
	return true;
}


bool CWWTPGetBoardData(CWWTPVendorInfos *valPtr)
{
	if(valPtr == NULL)
	{
		printf("wrong argument in board data");
		return 0;
	}
	valPtr->vendorInfos = new CWWTPVendorInfoValues[2];
	for(int i=0; i<=1 ;i++){
		((valPtr->vendorInfos)[i]).valuePtr = new char[10];
}
		valPtr->vendorInfosCount = 2; // we fill 2 information (just the required ones)

		//valPtr = new CWWTPVendorInfos[valPtr->vendorInfosCount];

		// my vendor identifier (IANA assigned "SMI Network Management Private Enterprise Code")
		valPtr->vendorInfos[0].vendorIdentifier = 23456;

		(valPtr->vendorInfos)[0].type = CW_WTP_MODEL_NUMBER;
		(valPtr->vendorInfos)[0].length = sizeof(long int); // just one int
		//*(int *)
		*(int *)(((valPtr->vendorInfos)[0]).valuePtr) = 123456; // MODEL NUMBER

		// my vendor identifier (IANA assigned "SMI Network Management Private Enterprise Code")
		(valPtr->vendorInfos)[1].vendorIdentifier = 23456;
		(valPtr->vendorInfos)[1].type = CW_WTP_SERIAL_NUMBER;
		(valPtr->vendorInfos)[1].length = sizeof(long int); // just one int
		//*(int *)
		*(int *)(((valPtr->vendorInfos)[1]).valuePtr) = 123456; // SERIAL NUMBER

	return true;
}

bool CWWTPGetVendorInfos(CWWTPVendorInfos *valPtr) {
	if(valPtr == NULL)
	{
		printf("error getting wtp descriptor data");
		return false;
	}

	valPtr->vendorInfosCount = 3; // we fill 3 information (just the required ones)
	valPtr->vendorInfos = new CWWTPVendorInfoValues[valPtr->vendorInfosCount];
	for(int i=0; i<=2 ;i++){
			((valPtr->vendorInfos)[i]).valuePtr = new char[10];
	}

	//CW_CREATE_ARRAY_ERR((valPtr->vendorInfos), valPtr->vendorInfosCount, CWWTPVendorInfoValues, return CWErrorRaise(CW_ERROR_OUT_OF_MEMORY, NULL););

	// my vendor identifier (IANA assigned "SMI Network Management Private Enterprise Code")
	(valPtr->vendorInfos)[0].vendorIdentifier = 23456;
	(valPtr->vendorInfos)[0].type = CW_WTP_HARDWARE_VERSION;
	(valPtr->vendorInfos)[0].length = sizeof(long int); // just one int
	//CW_CREATE_OBJECT_SIZE_ERR(( ( (valPtr->vendorInfos)[0] ).valuePtr), (valPtr->vendorInfos)[0].length, return CWErrorRaise(CW_ERROR_OUT_OF_MEMORY, NULL););
	*(int *)(((valPtr->vendorInfos)[0]).valuePtr) = 123456; // HW version

	// my vendor identifier (IANA assigned "SMI Network Management Private Enterprise Code")
	((valPtr->vendorInfos)[1]).vendorIdentifier = 23456;
	((valPtr->vendorInfos)[1]).type = CW_WTP_SOFTWARE_VERSION;
	((valPtr->vendorInfos)[1]).length = sizeof(long int); // just one int
	//CW_CREATE_OBJECT_SIZE_ERR(( ( (valPtr->vendorInfos)[1] ).valuePtr), (valPtr->vendorInfos)[1].length, return CWErrorRaise(CW_ERROR_OUT_OF_MEMORY, NULL););
	*(int *)(((valPtr->vendorInfos)[1]).valuePtr) = 12347; // SW version

	// my vendor identifier (IANA assigned "SMI Network Management Private Enterprise Code")
	(valPtr->vendorInfos)[2].vendorIdentifier = 23456;
	(valPtr->vendorInfos)[2].type = CW_BOOT_VERSION;
	(valPtr->vendorInfos)[2].length = sizeof(long int); // just one int
	//CW_CREATE_OBJECT_SIZE_ERR(( ( (valPtr->vendorInfos)[2] ).valuePtr), (valPtr->vendorInfos)[2].length, return CWErrorRaise(CW_ERROR_OUT_OF_MEMORY, NULL););
	*(int *)(((valPtr->vendorInfos)[2]).valuePtr) = 1234568; //1234568; // Boot version
	return true;
}

/*bool CWAssembleMsgElem(CWProtocolMessage *msgPtr, unsigned int type)
{
	CWProtocolMessage completeMsg;
	if(msgPtr == NULL)
	{
		cout<<"Invalid argument in assemble message\n";
		return false;
	}

	completeMsg.msg

	return true;
}*/

bool CWAssembleMsgElemDiscoveryType(CWProtocolMessage *msgPtr)
{

	printf("***CWAssembleMsgElemDiscoveryType---enter\n");		//????charles
	if(msgPtr == NULL)
	{
		printf("Wrong arguument for discovery type");
		return 0;
	}
	//cout<<sizeof(long int)<<endl;;
	msgPtr->msg = new char[1];
	memcpy(msgPtr->msg+msgPtr->offset,&CW_MSG_ELEMENT_DISCOVERY_TYPE_BROADCAST,1);
	//msgPtr->msg = CW_MSG_ELEMENT_DISCOVERY_TYPE_BROADCAST;
	msgPtr->offset += 1;
	//msgPtr->data_msgType = CW_MSG_ELEMENT_DISCOVERY_TYPE_CW_TYPE;
	//cout << (int)(*msgPtr->msg) << endl;
	printf("***CWAssembleMsgElemDiscoveryType---exit by calling CWAssembleMsgElem\n");		//?????charles
	return CWAssembleMsgElem(msgPtr, CW_MSG_ELEMENT_DISCOVERY_TYPE_CW_TYPE);
}
bool CWAssembleMsgElemWTPBoardData(CWProtocolMessage *msgPtr)
{
	const int VENDOR_ID_LENGTH = 4; 	//Vendor Identifier is 4 bytes long
	const int TLV_HEADER_LENGTH = 4; 	//Type and Length of a TLV field is 4 byte long
	int i, size = 0;
	CWWTPVendorInfos infos;
	
	printf("***CWAssembleMsgElemWTPBoardData---enter\n");		//?????charles
	if(msgPtr == NULL)
	{
		printf("Wrong arguument for discovery type");
		return 0;
	}
	if(!CWWTPGetBoardData(&infos)) {
			return 0;
		}
		   //Calculate msg elem size
		size = VENDOR_ID_LENGTH;
		for(i = 0; i < infos.vendorInfosCount; i++)
			{size += (TLV_HEADER_LENGTH + ((infos.vendorInfos)[i]).length);}
		msgPtr->msg = new char[size];
		CWProtocolStore32(msgPtr,(infos.vendorInfos)[0].vendorIdentifier);
		for(i = 0; i < infos.vendorInfosCount; i++)
		{

			CWProtocolStore16(msgPtr,(infos.vendorInfos)[i].type);
			//cout <<"Hello"<<endl;
			//memcpy(&((infos.vendorInfos)[i].type),&((msgPtr->msg)[msgPtr->offset]),4);
			//cout << &(msgPtr->msg[msgPtr->offset]) << '\n';
			CWProtocolStore16(msgPtr,(infos.vendorInfos)[i].length);
			//msgPtr->msg[msgPtr->offset]= ((infos.vendorInfos)[i].length);
			if((infos.vendorInfos)[i].length == 4)
			{
				*((infos.vendorInfos)[i].valuePtr) = htonl(*((infos.vendorInfos)[i].valuePtr));
			}
			memcpy(msgPtr->msg+msgPtr->offset,((infos.vendorInfos)[i].valuePtr),(infos.vendorInfos)[i].length);
			(msgPtr->offset) += (infos.vendorInfos)[i].length;
		}
		//msgPtr->data_msgType = CW_MSG_ELEMENT_WTP_BOARD_DATA_CW_TYPE;
		//cout<<(int)msgPtr->msg[9]<<endl;
		/*for (int i=0;i<32;i++)
		{
			cout<<(int)*((msgPtr->msg)+i)<<endl;
		}*/
	printf("***CWAssembleMsgElemWTPBoardData---exit by calling CWAssembleMsgElem\n");		//?????charles
	return CWAssembleMsgElem(msgPtr, CW_MSG_ELEMENT_WTP_BOARD_DATA_CW_TYPE);
}

bool CWAssembleMsgElemWTPDescriptor(CWProtocolMessage *msgPtr)
{
		const int GENERIC_RADIO_INFO_LENGTH = 4;//First 4 bytes for Max Radios, Radios In Use and Encryption Capability
		const int VENDOR_ID_LENGTH = 4; 	//Vendor Identifier is 4 bytes long
		const int TLV_HEADER_LENGTH = 4; 	//Type and Length of a TLV field is 4 byte long
		CWWTPVendorInfos infos;
		int i, size = 0;

		printf("***CWAssembleMsgElemWTPDescriptor---enter\n");		//?????charles
		if(msgPtr == NULL)
		{
			printf("wrong arg in WTP Descriptor");
			return false;
		}

		// get infos
		if(!CWWTPGetVendorInfos(&infos)) {
			return false;
		}

		//Calculate msg elem size
		size = GENERIC_RADIO_INFO_LENGTH;
		for(i = 0; i < infos.vendorInfosCount; i++)
			{size += (VENDOR_ID_LENGTH + TLV_HEADER_LENGTH + ((infos.vendorInfos)[i]).length);}
		/*
		 * Elena Agostini - 02/2014
		 *
		 * BUG Valgrind: Real size of msgPrt is 42, not 40. With 40 bytes there's
		 * an heap overrun.
		 */
		size += 2;

		// create message
		//CW_CREATE_PROTOCOL_MESSAGE(*msgPtr, size, return CWErrorRaise(CW_ERROR_OUT_OF_MEMORY, NULL););
		msgPtr->msg = new char[size];

		//*msgPtr->msg = CW_MAX_RADIO;
		//cout << msgPtr->msg <<'\n';
		memcpy((msgPtr->msg+msgPtr->offset),&CW_MAX_RADIO,1);
		msgPtr->offset += 1;
		memcpy((msgPtr->msg+msgPtr->offset),&CW_MAX_RADIO_IN_USE,1);
		msgPtr->offset += 1;
		int x=1;
		memcpy((msgPtr->msg+msgPtr->offset),&x,1);
		msgPtr->offset += 1;
		memcpy((msgPtr->msg+msgPtr->offset),&x,1);
		msgPtr->offset += 1;// IEEE802.11 binding
		int y = 0;
		CWProtocolStore16(msgPtr,y);
		//CWProtocolStore16(msgPtr, CWWTPGetEncCapabilities());

		for(i = 0; i < infos.vendorInfosCount; i++) {
			CWProtocolStore32(msgPtr,(infos.vendorInfos)[i].vendorIdentifier);
			//CWProtocolStore32(msgPtr, ((infos.vendorInfos)[i].vendorIdentifier));
			CWProtocolStore16(msgPtr,(infos.vendorInfos)[i].type);
			//CWProtocolStore16(msgPtr, ((infos.vendorInfos)[i].type));
			CWProtocolStore16(msgPtr,(infos.vendorInfos)[i].length);
			//CWProtocolStore16(msgPtr, ((infos.vendorInfos)[i].length));

			if((infos.vendorInfos)[i].length == 4) {
				*((infos.vendorInfos)[i].valuePtr) = htonl(*((infos.vendorInfos)[i].valuePtr));
			}
			memcpy(msgPtr->msg+msgPtr->offset,((infos.vendorInfos)[i].valuePtr),(infos.vendorInfos)[i].length);
			(msgPtr->offset) += (infos.vendorInfos)[i].length;

			//CWProtocolStoreRawBytes(msgPtr, (char*) ((infos.vendorInfos)[i].valuePtr), (infos.vendorInfos)[i].length);
	/*
			CWDebugLog("WTP Descriptor Vendor ID: %d", (infos.vendorInfos)[i].vendorIdentifier);
			CWDebugLog("WTP Descriptor Type: %d", (infos.vendorInfos)[i].type);
			CWDebugLog("WTP Descriptor Length: %d", (infos.vendorInfos)[i].length);
			CWDebugLog("WTP Descriptor Value: %d", *((infos.vendorInfos)[i].valuePtr));
	*/
			//CWDebugLog("Vendor Info \"%d\" = %d - %d - %d", i, (infos.vendorInfos)[i].vendorIdentifier, (infos.vendorInfos)[i].type, (infos.vendorInfos)[i].length);
		}

		//CWWTPDestroyVendorInfos(&infos);

		if(&infos == NULL)
			return 0;

		for(int i = 0; i < infos.vendorInfosCount; i++) {
			delete((infos.vendorInfos)[i].valuePtr);
		}

		delete infos.vendorInfos;
		printf("***CWAssembleMsgElemWTPDescriptor---exit by calling CWAssembleMsgElem\n");		//?????charles
		return CWAssembleMsgElem(msgPtr, CW_MSG_ELEMENT_WTP_DESCRIPTOR_CW_TYPE);
}

bool CWAssembleMsgElemWTPFrameTunnelMode(CWProtocolMessage *msgPtr)
{
	printf("***CWAssembleMsgElemWTPFrameTunnelMode---enter\n");		//?????charles
	if(msgPtr == NULL)
		return 0;

		// create message
		msgPtr->msg = new char[1];
		//CW_CREATE_PROTOCOL_MESSAGE(*msgPtr, 1, return CWErrorRaise(CW_ERROR_OUT_OF_MEMORY, NULL););

		//	CWDebugLog("Frame Tunnel Mode: %d", CWWTPGetFrameTunnelMode());
		memcpy(msgPtr->msg+msgPtr->offset,&CW_LOCAL_BRIDGING,1);
		msgPtr->offset += 1;
		//msgPtr->data_msgType = CW_MSG_ELEMENT_WTP_FRAME_TUNNEL_MODE_CW_TYPE;
		/*cout << (int)*msgPtr->msg<<endl;
		cout<<msgPtr->offset<<msgPtr->data_msgType<<endl;*/
		//CWProtocolStore8(msgPtr, CWWTPGetFrameTunnelMode()); // frame encryption
		printf("***CWAssembleMsgElemWTPFrameTunnelMode---exit by calling CWAssembleMsgElem\n");		//?????charles
		return CWAssembleMsgElem(msgPtr, CW_MSG_ELEMENT_WTP_FRAME_TUNNEL_MODE_CW_TYPE);
}
bool CWAssembleMsgElemWTPMACType(CWProtocolMessage *msgPtr)
{
	if(msgPtr == NULL)
		return 0;

	msgPtr->msg = new char[1];
	// create message
		//CW_CREATE_PROTOCOL_MESSAGE(*msgPtr, 1, return CWErrorRaise(CW_ERROR_OUT_OF_MEMORY, NULL););

	//	CWDebugLog("WTP MAC Type: %d", CWWTPGetMACType());
		//CWProtocolStore8(msgPtr, CWWTPGetMACType()); // mode of operation of the WTP (local, split, ...)
	memcpy(msgPtr->msg+msgPtr->offset,&CW_LOCAL_MAC,1);
	msgPtr->offset += 1;
	//msgPtr->data_msgType = CW_MSG_ELEMENT_WTP_MAC_TYPE_CW_TYPE;

	return CWAssembleMsgElem(msgPtr, CW_MSG_ELEMENT_WTP_MAC_TYPE_CW_TYPE);
}

/* Elena Agostini - 07/2014: WTP Radio Info: nl80211 support */
bool CWAssembleMsgElemWTPRadioInformation(CWProtocolMessage *msgPtr, int radioID, char radioType) {

	printf("***CWAssembleMsgElemWTPRadioInformation---enter\n");		//?????charles
	if(msgPtr == NULL)
		return 0;

	msgPtr->msg=new char[5];
	//cout<< msgPtr->offset<<endl;
	//CW_CREATE_PROTOCOL_MESSAGE(*msgPtr, 5, return CWErrorRaise(CW_ERROR_OUT_OF_MEMORY, NULL););

	//RadioID - 1 byte
	memcpy(msgPtr->msg+msgPtr->offset,&radioID,1);
	msgPtr->offset += 1;
	//CWProtocolStore8(msgPtr, radioID);
	//Reserved - 3 byte
	int reserved = 0;
	memcpy(msgPtr->msg+msgPtr->offset,&reserved,3);
	msgPtr->offset += 3;
	/*CWProtocolStore8(msgPtr, 0);
	CWProtocolStore8(msgPtr, 0);
	CWProtocolStore8(msgPtr, 0);*/
	//Radio Type
	//CWProtocolStore8(msgPtr, radioType);
	memcpy(msgPtr->msg+msgPtr->offset,&radioType,1);
	msgPtr->offset += 1;

	printf("***CWAssembleMsgElemWTPRadioInformation---exit by calling CWAssembleMsgElem at line=%d\n", __LINE__);		//?????charles	
	return CWAssembleMsgElem(msgPtr, CW_MSG_ELEMENT_IEEE80211_WTP_RADIO_INFORMATION_CW_TYPE);
}

bool CWAssembleDiscoveryRequest(CWProtocolMessage **msgPtr,int seqNum)
{
	CWProtocolMessage *msgElems = new CWProtocolMessage[6];
	memset(msgElems, 0, (sizeof(CWProtocolMessage) * 6));
	CWWTPRadiosInfo gRadiosInfo;
	gRadiosInfo.radioCount = 2;
	gRadiosInfo.radiosInfo = new CWWTPRadioInfoValues[gRadiosInfo.radioCount];
	gRadiosInfo.radiosInfo[0].radioID = 1;
	gRadiosInfo.radiosInfo[1].radioID = 2;
	gRadiosInfo.radiosInfo[0].radioType = CW_802_DOT_11n;
	gRadiosInfo.radiosInfo[1].radioType = CW_802_DOT_11n;

	const int msgElemCount = 6;
	CWProtocolMessage *msgElemsBinding= NULL;
	const int msgElemBindingCount=0;
	int fragmentsNum;
	//cout<<msgElems[0].offset<<'\n';
	int k = -1;
	printf("***CWAssembleDiscoveryRequest: start access the structure\n");
	if(
		   (!(CWAssembleMsgElemDiscoveryType(&(msgElems[++k])))) ||
		   (!(CWAssembleMsgElemWTPBoardData(&(msgElems[++k]))))	 ||
		   (!(CWAssembleMsgElemWTPDescriptor(&(msgElems[++k])))) ||
		   (!(CWAssembleMsgElemWTPFrameTunnelMode(&(msgElems[++k])))) ||
		   (!(CWAssembleMsgElemWTPMACType(&(msgElems[++k]))))
		   )
	{
		int i;
		printf("***CWAssembleDiscoveryRequest: check access the structure checking#1\n");
		for(i=0;i<=k;i++)
		{
			printf("***CWAssembleDiscoveryRequest: check access the structure checking#2\n");
			delete msgElems[i].msg;
		}
		delete msgElems;
		printf("***CWAssembleDiscoveryRequest: check access the structure failed\n");
		return false;
	}
	
	int indexWTPRadioInfo=0;
		for(indexWTPRadioInfo=0; indexWTPRadioInfo<gRadiosInfo.radioCount; indexWTPRadioInfo++)
		{	printf("***CWAssembleDiscoveryRequest: access the structure CWAssembleMsgElemWTPRadioInformation at indexWTPRadioInfo=%d\n", indexWTPRadioInfo);
			if(!(CWAssembleMsgElemWTPRadioInformation( &(msgElems[++k]), gRadiosInfo.radiosInfo[indexWTPRadioInfo].radioID, gRadiosInfo.radiosInfo[indexWTPRadioInfo].radioType)))
			{
				int i;				
				for(i=0;i<=k;i++){
					printf("***CWAssembleDiscoveryRequest: access the structure CWAssembleMsgElemWTPRadioInformation at indexWTPRadioInfo inner loop begin=%d\n", i);
					delete msgElems[i].msg;
				}
				delete msgElems;
				printf("***CWAssembleDiscoveryRequest: access the structure CWAssembleMsgElemWTPRadioInformation at indexWTPRadioInfo=%d ---done\n", indexWTPRadioInfo);
				return false;
			}
		}
	printf("***CWAssembleDiscoveryRequest: calling CWAssembleMessage\n");
	return CWAssembleMessage(msgPtr,&fragmentsNum,0,seqNum,CW_MSG_TYPE_VALUE_DISCOVERY_REQUEST,msgElems,msgElemCount,msgElemsBinding,msgElemBindingCount,CW_PACKET_PLAIN);
}

int main()
{
	wtp thisContext;
	
	if(wtp_initialize_cfg_access()==false)
	{
		cout << "CAPWAP: wtp vendor specific IE parsing initializtion failed, can not proceed ..." << endl;
		return -1;
	}
	//cout<<"Hello world\n";
	thisContext.start();
	}

void wtp::start()
{
	_fsm.Init_No_Error();
Initial:
{
	_fsm.Start_Discover();
if(disc_count > max_disc_count)
	_fsm.Ac_Discovery_Fail();
else
	_fsm.Ac_Selection_Complete();
if(valuesPtr->code == 0)
	_fsm.Join_Response_Success();
else
	goto Initial;
//if(configValuesPtr->resultCode == 0)
	_fsm.Config_Status_Response_Success();
//else
	//goto Initial;
	_fsm.Receive_Update_Request();
}
	return;
}

void wtp::outputErrorLog()
{
	cout<<"Error occured"<<endl;
}
void wtp::errorCheck()
{
	printf("WTP initialized successfully\n");
	//_fsm.Init_No_Error();
}
void wtp::resetDiscoveryCount()
{
	disc_count = 0;
	//cout<<disc_count<<'\n';
	closeSocket(&socket_c);
}
void wtp::clearAcInfo()
{

}

bool CWParseFormatMsgElem(CWProtocolMessage *completeMsg,unsigned short int *type,unsigned short int *len)
{
	*type = CWProtocolRetrieve16(completeMsg);
	*len = CWProtocolRetrieve16(completeMsg);
	return true;
}

bool CWParseTransportHeader(CWProtocolMessage *msgPtr, CWProtocolTransportHeaderValues *valuesPtr, bool *dataFlagPtr, char *RadioMAC) {

	int transport4BytesLen;
	int val;
	int optionalWireless = 0;
	int version, rid;
	int m=0;
	int KeepAliveLenght=0;

	if(msgPtr == NULL || valuesPtr == NULL) return 0;

	//CWDebugLog("Parse Transport Header");
	val = CWProtocolRetrieve32(msgPtr);

	if(CWGetField32(val, CW_TRANSPORT_HEADER_VERSION_START, CW_TRANSPORT_HEADER_VERSION_LEN) != CW_PROTOCOL_VERSION)
	{
		printf("Wrong Protocol Version");
		return false;
	}

	version = CWGetField32(val, CW_TRANSPORT_HEADER_VERSION_START, CW_TRANSPORT_HEADER_VERSION_LEN);
//	CWDebugLog("VERSION: %d", version);

	valuesPtr->payloadType = CWGetField32(val, CW_TRANSPORT_HEADER_TYPE_START, CW_TRANSPORT_HEADER_TYPE_LEN);
//	CWDebugLog("PAYLOAD TYPE: %d", valuesPtr->payloadType);


	transport4BytesLen = CWGetField32(val,	CW_TRANSPORT_HEADER_HLEN_START, CW_TRANSPORT_HEADER_HLEN_LEN);
//	CWDebugLog("HLEN: %d", transport4BytesLen);

	rid = CWGetField32(val, CW_TRANSPORT_HEADER_RID_START, CW_TRANSPORT_HEADER_RID_LEN);
//	CWDebugLog("RID: %d", rid);

//	CWDebugLog("WBID: %d", CWGetField32(val, CW_TRANSPORT_HEADER_WBID_START, CW_TRANSPORT_HEADER_WBID_LEN));

	valuesPtr->type = CWGetField32(val, CW_TRANSPORT_HEADER_T_START, CW_TRANSPORT_HEADER_T_LEN);
//	CWDebugLog("TYPE: %d", valuesPtr->type);
	//CWDebugLog("TYPE: %d", valuesPtr->type);

	valuesPtr->isFragment = CWGetField32(val, CW_TRANSPORT_HEADER_F_START, CW_TRANSPORT_HEADER_F_LEN);
	//CWDebugLog("IS FRAGMENT: %d", valuesPtr->isFragment);

	valuesPtr->last = CWGetField32(val, CW_TRANSPORT_HEADER_L_START, CW_TRANSPORT_HEADER_L_LEN);
	//CWDebugLog("NOT LAST: %d", valuesPtr->last);

	optionalWireless = CWGetField32(val, CW_TRANSPORT_HEADER_W_START, CW_TRANSPORT_HEADER_W_LEN);
//	CWDebugLog("OPTIONAL WIRELESS: %d", optionalWireless);
	m = CWGetField32(val, CW_TRANSPORT_HEADER_M_START, CW_TRANSPORT_HEADER_M_LEN);


	valuesPtr->keepAlive = CWGetField32(val, CW_TRANSPORT_HEADER_K_START, CW_TRANSPORT_HEADER_K_LEN);
//	CWDebugLog("KEEP ALIVE: %d", valuesPtr->keepAlive);

	val = CWProtocolRetrieve32(msgPtr);

	valuesPtr->fragmentID = CWGetField32(val, CW_TRANSPORT_HEADER_FRAGMENT_ID_START, CW_TRANSPORT_HEADER_FRAGMENT_ID_LEN);
//	CWDebugLog("FRAGMENT_ID: %d", valuesPtr->fragmentID);

	valuesPtr->fragmentOffset = CWGetField32(val, CW_TRANSPORT_HEADER_FRAGMENT_OFFSET_START, CW_TRANSPORT_HEADER_FRAGMENT_OFFSET_LEN);
//	CWDebugLog("FRAGMENT_OFFSET: %d", valuesPtr->fragmentOffset);

	valuesPtr->bindingValuesPtr = NULL;

	msgPtr->data_msgType=CW_IEEE_802_3_FRAME_TYPE;


	//CWDebugLog(NULL);
	return (transport4BytesLen == 2 || (transport4BytesLen == 4 && optionalWireless == 1) || m ) ? true : false; //TEMP?
}

// Parse Control Header
bool CWParseControlHeader(CWProtocolMessage *msgPtr, CWControlHeaderValues *valPtr) {
	unsigned char flags=0;

	if(msgPtr == NULL|| valPtr == NULL) return 0;

//	CWDebugLog("Parse Control Header");
	valPtr->messageTypeValue = CWProtocolRetrieve32(msgPtr);
//	CWDebugLog("MESSAGE_TYPE: %u",	valPtr->messageTypeValue);

	valPtr->seqNum = CWProtocolRetrieve8(msgPtr);
//	CWDebugLog("SEQUENCE_NUMBER: %u", valPtr->seqNum );

	valPtr->msgElemsLen = CWProtocolRetrieve16(msgPtr);
//	CWDebugLog("MESSAGE_ELEMENT_LENGTH: %u", valPtr->msgElemsLen );

	flags=CWProtocolRetrieve8(msgPtr);
//	CWDebugLog("FLAGS: %u",	flags);

//	valPtr->timestamp = CWProtocolRetrieve32(msgPtr);
//	CWDebugLog("TIME_STAMP: %u",	valPtr->timestamp);

	//CWDebugLog(NULL);

	return true;
}

bool CWParseACDescriptor(CWProtocolMessage *msgPtr, int len, CWACInfoValues *valPtr) {
	int i=0, theOffset=0;

	CWParseMessageElementStart();
	valPtr->stations= CWProtocolRetrieve16(msgPtr);
//	CWDebugLog("AC Descriptor Stations: %d", valPtr->stations);

	valPtr->limit	= CWProtocolRetrieve16(msgPtr);
//	CWDebugLog("AC Descriptor Limit: %d", valPtr->limit);

	valPtr->activeWTPs= CWProtocolRetrieve16(msgPtr);
//	CWDebugLog("AC Descriptor Active WTPs: %d", valPtr->activeWTPs);

	valPtr->maxWTPs	= CWProtocolRetrieve16(msgPtr);
//	CWDebugLog("AC Descriptor Max WTPs: %d",	valPtr->maxWTPs);

	valPtr->security= CWProtocolRetrieve8(msgPtr);
//	CWDebugLog("AC Descriptor Security: %d",	valPtr->security);

	valPtr->RMACField= CWProtocolRetrieve8(msgPtr);
//	CWDebugLog("AC Descriptor Radio MAC Field: %d",	valPtr->security);

//	valPtr->WirelessField= CWProtocolRetrieve8(msgPtr);
//	CWDebugLog("AC Descriptor Wireless Field: %d",	valPtr->security);

	CWProtocolRetrieve8(msgPtr);			//Reserved

	valPtr->DTLSPolicy= CWProtocolRetrieve8(msgPtr); // DTLS Policy
//	CWDebugLog("DTLS Policy: %d",	valPtr->DTLSPolicy);

	valPtr->vendorInfos.vendorInfosCount = 0;

	theOffset = msgPtr->offset;

	// see how many vendor ID we have in the message
	while((msgPtr->offset-oldOffset) < len) {	// oldOffset stores msgPtr->offset's value at the beginning of this function.
							// See the definition of the CWParseMessageElementStart() macro.
		int tmp, id=0, type=0;

		//CWDebugLog("differenza:%d, offset:%d, oldOffset:%d", (msgPtr->offset-oldOffset), (msgPtr->offset), oldOffset);

		id=CWProtocolRetrieve32(msgPtr);
//		CWDebugLog("ID: %d", id); // ID

		type=CWProtocolRetrieve16(msgPtr);
//		CWDebugLog("TYPE: %d",type); // type

		tmp = CWProtocolRetrieve16(msgPtr);
		msgPtr->offset += tmp; // len
//		CWDebugLog("offset %d", msgPtr->offset);
		valPtr->vendorInfos.vendorInfosCount++;
	}

	msgPtr->offset = theOffset;
	// actually read each vendor ID
	//valPtr->vendorInfos.vendorInfos = (CWACVendorInfoValues*) (malloc(sizeof(CWACVendorInfoValues) * (valPtr->vendorInfos.vendorInfosCount)));
	valPtr->vendorInfos.vendorInfos = new CWACVendorInfoValues[3];

	/*CW_CREATE_ARRAY_ERR(valPtr->vendorInfos.vendorInfos, valPtr->vendorInfos.vendorInfosCount, CWACVendorInfoValues,
		return CWErrorRaise(CW_ERROR_OUT_OF_MEMORY, NULL););*/

	for(int i=0; i<= valPtr->vendorInfos.vendorInfosCount;i++){
			valPtr->vendorInfos.vendorInfos->valuePtr = new int[10];
	}

//	CWDebugLog("len %d", len);
//	CWDebugLog("vendorInfosCount %d", valPtr->vendorInfos.vendorInfosCount);
	for(i = 0; i < valPtr->vendorInfos.vendorInfosCount; i++) {
//		CWDebugLog("vendorInfosCount %d vs %d", i, valPtr->vendorInfos.vendorInfosCount);
		(valPtr->vendorInfos.vendorInfos)[i].vendorIdentifier = CWProtocolRetrieve32(msgPtr);
		(valPtr->vendorInfos.vendorInfos)[i].type = CWProtocolRetrieve16(msgPtr);
		(valPtr->vendorInfos.vendorInfos)[i].length = CWProtocolRetrieve16(msgPtr);
		(valPtr->vendorInfos.vendorInfos)[i].valuePtr = (int*) (CWProtocolRetrieveRawBytes(msgPtr, valPtr->vendorInfos.vendorInfos[i].length));

		if((valPtr->vendorInfos.vendorInfos)[i].valuePtr == NULL)
			return false;

		if((valPtr->vendorInfos.vendorInfos)[i].length == 4) {
			*((valPtr->vendorInfos.vendorInfos)[i].valuePtr) = ntohl(*((valPtr->vendorInfos.vendorInfos)[i].valuePtr));
		}

//		CWDebugLog("AC Descriptor Vendor ID: %d", (valPtr->vendorInfos.vendorInfos)[i].vendorIdentifier);
//		CWDebugLog("AC Descriptor Type: %d", (valPtr->vendorInfos.vendorInfos)[i].type);
//		CWDebugLog("AC Descriptor Value: %d", *((valPtr->vendorInfos.vendorInfos)[i].valuePtr));
	}
//	CWDebugLog("AC Descriptor Out");
	CWParseMessageElementEnd();
}

bool CWParseWTPRadioInformation_FromAC(CWProtocolMessage *msgPtr, int len, char *valPtr) {
	//CWParseMessageElementStart();

	CWProtocolRetrieve8(msgPtr);

	CWProtocolRetrieve8(msgPtr);
	CWProtocolRetrieve8(msgPtr);
	CWProtocolRetrieve8(msgPtr);
	*valPtr = CWProtocolRetrieve8(msgPtr);
	return true;
	//CWParseMessageElementEnd();
}

bool CWParseACName(CWProtocolMessage *msgPtr, int len, char **valPtr) {
	CWParseMessageElementStart();

	*valPtr = CWProtocolRetrieveStr(msgPtr, len);
	if(valPtr == NULL) return false;
//	CWDebugLog("AC Name:%s", *valPtr);

	CWParseMessageElementEnd();
}

bool CWParseCWControlIPv4Addresses(CWProtocolMessage *msgPtr, int len, CWProtocolIPv4NetworkInterface *valPtr) {
	CWParseMessageElementStart();
	memset(&valPtr->addr, 0, sizeof(valPtr->addr));
	valPtr->addr.sin_addr.s_addr = htonl(CWProtocolRetrieve32(msgPtr));
	valPtr->addr.sin_family = AF_INET;
	valPtr->addr.sin_port = htons(CW_CONTROL_PORT);

	//CWUseSockNtop((&(valPtr->addr)), CWDebugLog("Interface Address: %s", str););

	valPtr->WTPCount = CWProtocolRetrieve16(msgPtr);
//	CWDebugLog("WTP Count: %d",	valPtr->WTPCount);

	return true;
	//CWParseMessageElementEnd();
}

bool wtp:: CWParseDiscoveryResponseMessage(char *msg,int len,int *seqNumPtr)
	{
		CWControlHeaderValues controlVal;
		CWProtocolTransportHeaderValues transportVal;
		int offsetTillMessages, i, j;
		char tmp_ABGNTypes;
		CWProtocolMessage completeMsg;

		printf("***CWParseDiscoveryResponseMessage enter\n");
		if(msg == NULL || seqNumPtr == NULL)
			return 0;

		completeMsg.msg = msg;
		completeMsg.offset = 0;

		bool dataFlag = false;

		if(!(CWParseTransportHeader(&completeMsg, &transportVal, &dataFlag, NULL))) return false;
		/* will be handled by the caller */
		if(!(CWParseControlHeader(&completeMsg, &controlVal))) return false;

		/* different type */
		if(controlVal.messageTypeValue != CW_MSG_TYPE_VALUE_DISCOVERY_RESPONSE)
			cout<<"Message is not Discovery Response as Expected"<<endl;
		*seqNumPtr = controlVal.seqNum;

		/* skip timestamp */
		controlVal.msgElemsLen -= CW_CONTROL_HEADER_OFFSET_FOR_MSG_ELEMS;

		ACInfoPtr->name = new char[1];

		cout<<completeMsg.offset<<endl;
		ACInfoPtr->IPv4Addresses = new CWProtocolIPv4NetworkInterface[ACInfoPtr->IPv4AddressesCount];
		i = 0, j = 0;
			//completeMsg.offset = offsetTillMessages;
			//while((completeMsg.offset-offsetTillMessages) < controlVal.msgElemsLen) {

			unsigned short int type=0;	/* = CWProtocolRetrieve32(&completeMsg); */
			unsigned short int length=0;	/* = CWProtocolRetrieve16(&completeMsg); */

			CWParseFormatMsgElem(&completeMsg,&type,&length);

			//switch(type) {
				//case CW_MSG_ELEMENT_CW_CONTROL_IPV4_ADDRESS_CW_TYPE:
					/* will be handled by the caller */
					if(!(CWParseCWControlIPv4Addresses(&completeMsg,len,&(ACInfoPtr->IPv4Addresses[0]))))
							return false;
							/*i++;
							break;
				default:
					completeMsg.offset += len;
					break;
						 }
			}*/
		cout<<"Discovery Response Parsed successfully"<<endl;
		
		return true;
	}

void wtp::discReq()
{

	int min = disc_interval;
	int max = max_disc_interval;

	/*Prameters to receive response */
	char buf[CW_BUFFER_SIZE];
	int readBytes;

	bool sentsomething = false;


		CWProtocolMessage *msgPtr = NULL;
		int seqNum = CWGetSeqNum();
		printf("***discReq call CWAssembleDiscoveryRequest \n");		//??????debug
		
		if(!(CWAssembleDiscoveryRequest(&msgPtr,seqNum))) 
		{
			exit(1);
		}
	   printf("***discReq call CWAssembleDiscoveryRequest done \n");		//??????debug
		do
		{
		if(socket_c)
			closeSocket(&socket_c);
		
		printf("***discReq call CWNetworkInitSocketClientWithPort\n");		//??????debug
		if(!(CWNetworkInitSocketClientWithPort(&socket_c, CW_CONTROL_PORT)))
			cout<<"Could not initialize socket"<<endl;
		
		printf("***discReq call CWNetworkInitSocketClientWithPort done\n");		//??????debug
		
		sleep( min + (rand() % (max-min)));

		printf("***discReq call sendReceiveDiscovery\n");		//??????debug
		if(!(sendReceiveDiscovery(&socket_c,CW_CONTROL_PORT,msgPtr->msg,msgPtr->offset,buf,CW_BUFFER_SIZE-1, &readBytes)))
			cout<<"discovery request failed"<<endl;
		else
			{
				sentsomething = true;
				cout<<"discovery request sent successfully and response received"<<endl;
				//_fsm.Disc_Int_Timer_Expire();
			}
		printf("***discReq call sendReceiveDiscovery done\n");		//??????debug
			incrementDiscoveryCount();
		}while(!(CWParseDiscoveryResponseMessage(buf, readBytes, &seqNum)||(disc_count > max_disc_count)));
		printf("***discReq exit\n");		//??????debug

}

//CWVendorSpecificIeEncode(CWProtocolMessage *msgPtr)
//Input: msgPtr  points the CWProtocolMessage structure allocated by the caller, the content
//			content of the structure was assume invalid frim the input.
//Output:
//			msgPtr->msg:	point the beginning of the total vendor specific IE.
//								this pointer will points the allocate buffer with this function
//			msgPtr->offset:	have the value of the total length of the vendor specific IE.
//									which means that it point the end of the vendor specific IE.
//			Note: IE-type and IE length are update within the byte stream buffer.
bool CWAssembleMsgElemVendorSpecificPayload(CWProtocolMessage *msgPtr)
{

	if(msgPtr == NULL)
	{
		printf("CAPWAP: Wrong arguument for vendor payload");
		return false;
	}
	
	WTP_cIE_Channel  r0_chan(0);
	r0_chan.get();
	int ie_sz_chan0 = r0_chan.getPtrOffset4NextIE();
	WTP_cIE_Txpower  r0_pwr(0);
	r0_pwr.get();
	int ie_sz_pwr0  = r0_pwr.getPtrOffset4NextIE();
	WTP_cIE_SSID	  r0_ssid(0);
	r0_ssid.get();
	int ie_sz_ssid0  = r0_ssid.getPtrOffset4NextIE();
	WTP_cIE_MAC  r0_mac(0);
	r0_mac.get();
	int ie_sz_mac0 = r0_mac.getPtrOffset4NextIE();
	
	WTP_cIE_Channel  r1_chan(1);	
	int ie_sz_chan1  = r1_chan.getPtrOffset4NextIE();
	WTP_cIE_Txpower  r1_pwr(1);
	int ie_sz_pwr1  = r1_pwr.getPtrOffset4NextIE();
	WTP_cIE_SSID	  r1_ssid(1);
	int ie_sz_ssid1 = r1_ssid.getPtrOffset4NextIE();
	WTP_cIE_MAC  	  r1_mac(1);
	int ie_sz_mac1 = r1_mac.getPtrOffset4NextIE();
	if(GetNumOfWiFiRadio() > 1)
	{
		r1_chan.get();
		r1_pwr.get();
		r1_ssid.get();
		r1_mac.get();
	}
	
	//cout<<sizeof(long int)<<endl;
	int total_cIE_Len  = CW_IE_VENDOR_SPECIFIC_HRD_LEN + ie_sz_mac1 + ie_sz_ssid1 + ie_sz_pwr1 + ie_sz_chan1 + ie_sz_mac0 + ie_sz_ssid0 + ie_sz_pwr0 + ie_sz_chan0;
	msgPtr->msg = new char[total_cIE_Len ];
	msgPtr->offset = 0;

	//Vendor Specific IE type---2 bytes
	CWProtocolStore16(msgPtr, CW_IE_VENDOR_SPECIFIC_TYPE);
	//Vendor Specific IE Length:  not include the capwap ie type and the capwap ie length field---2 bytes
	CWProtocolStore16(msgPtr, (total_cIE_Len - CW_IE_VENDOR_SPECIFIC_ID_LEN));
	//Vendor Specific ID---4 bytes
	CWProtocolStore32(msgPtr, CW_IE_VENDOR_SPECIFIC_ID);
	
	r0_chan.serialize( (uint8_t *)(msgPtr->msg + msgPtr->offset));
	msgPtr->offset += ie_sz_chan0;
	r0_pwr.serialize( (uint8_t *)(msgPtr->msg + msgPtr->offset));
	msgPtr->offset += ie_sz_pwr0;	
	r0_ssid.serialize( (uint8_t *)(msgPtr->msg + msgPtr->offset));
	msgPtr->offset += ie_sz_ssid0;	
	r0_mac.serialize( (uint8_t *)(msgPtr->msg + msgPtr->offset));
	msgPtr->offset += ie_sz_mac0;
	if(GetNumOfWiFiRadio() > 1)
	{
		r1_chan.serialize( (uint8_t *)(msgPtr->msg + msgPtr->offset));
		msgPtr->offset += ie_sz_chan1;	
		r1_pwr.serialize( (uint8_t *)(msgPtr->msg + msgPtr->offset));
		msgPtr->offset += ie_sz_pwr1;	
		r1_ssid.serialize( (uint8_t *)(msgPtr->msg + msgPtr->offset));
		msgPtr->offset += ie_sz_ssid1;	
		r1_mac.serialize( (uint8_t *)(msgPtr->msg + msgPtr->offset));
		msgPtr->offset += ie_sz_mac1;	
	}
	return true;
}


bool CWAssembleJoinRequest(CWProtocolMessage **msgPtr,int seqNum)
{
	CWProtocolMessage *msgElems = new CWProtocolMessage;
	printf("****CWAssembleJoinRequest \n");
	const int msgElemCount = 0;
	CWProtocolMessage *msgElemsBinding= NULL;
	const int msgElemBindingCount=0;
	int fragmentsNum=0;
	printf("****CWAssembleJoinRequest ready to CWAssembleMessage\n");
	return CWAssembleMessage(msgPtr,&fragmentsNum,0,seqNum,CW_MSG_TYPE_VALUE_JOIN_REQUEST, msgElems,msgElemCount,msgElemsBinding,msgElemBindingCount,CW_PACKET_PLAIN);
}

bool CWAssembleConfigRequest(CWProtocolMessage **msgPtr,int seqNum)
{
	CWProtocolMessage *msgElems = new CWProtocolMessage[1];
	const int msgElemCount = 1;
	int k = -1;
	CWProtocolMessage *msgElemsBinding= NULL;
	const int msgElemBindingCount=0;
	int fragmentsNum;
	if(!(CWAssembleMsgElemVendorSpecificPayload(&(msgElems[++k]))))
		{
			int i;
			for(i=0;i<=k;i++){
				delete msgElems[i].msg;
			}
			delete msgElems;
			return false;
		}
	return CWAssembleMessage(msgPtr,&fragmentsNum,0,seqNum,CW_MSG_TYPE_VALUE_CONFIGURE_REQUEST,msgElems,msgElemCount,msgElemsBinding,msgElemBindingCount,CW_PACKET_PLAIN);
}

bool CWAssembleWtpRequest(CWProtocolMessage **msgPtr,int seqNum)
{
	CWProtocolMessage *msgElems = new CWProtocolMessage[0];
	const int msgElemCount = 0;
	CWProtocolMessage *msgElemsBinding= NULL;
	const int msgElemBindingCount=0;
	int fragmentsNum;
	return CWAssembleMessage(msgPtr,&fragmentsNum,0,seqNum,CW_MSG_TYPE_VALUE_WTP_EVENT_REQUEST,msgElems,msgElemCount,msgElemsBinding,msgElemBindingCount,CW_PACKET_PLAIN);
}

bool CWAssembleConfigUpdateResponse(CWProtocolMessage **msgPtr,int seqNum,CWProtocolMessage *vendorElem)
{
	CWProtocolMessage *msgElems = new CWProtocolMessage[1];
	const int msgElemCount = 1;
	int k = -1;
	CWProtocolMessage *msgElemsBinding= NULL;
	const int msgElemBindingCount=0;
	int fragmentsNum;
	msgElems[1].msg = new char[vendorElem->offset];
	msgElems[1].msg = vendorElem -> msg;
	msgElems[1].offset = vendorElem -> offset;
	return CWAssembleMessage(msgPtr,&fragmentsNum,0,seqNum,CW_MSG_TYPE_VALUE_CONFIGURE_UPDATE_RESPONSE,msgElems,msgElemCount,msgElemsBinding,msgElemBindingCount,CW_PACKET_PLAIN);
}

/*bool CWAssembleConfigUpdateRequest(CWProtocolMessage **msgPtr,int seqNum)
{
	CWProtocolMessage *msgElems = new CWProtocolMessage[1];
	const int msgElemCount = 1;
	int k = -1;
	CWProtocolMessage *msgElemsBinding= NULL;
	const int msgElemBindingCount=0;
	int fragmentsNum;
	if(!(CWAssembleMsgElemVendorSpecificPayload(&(msgElems[++k]))))
		{
			int i;
			for(i=0;i<=k;i++){
				delete msgElems[i].msg;
			}
			delete msgElems;
			return false;
		}
	return CWAssembleMessage(msgPtr,&fragmentsNum,0,seqNum,CW_MSG_TYPE_VALUE_CONFIGURE_REQUEST,msgElems,msgElemCount,msgElemsBinding,msgElemBindingCount,CW_PACKET_PLAIN);
}*/


void wtp::setDiscIntervalTimer()
{
	disc_interval = 5;
	//cout<<disc_interval<<'\n';
}
void wtp::incrementDiscoveryCount()
{
	disc_count ++;
}
void wtp::discReqToNotRespondingAcs()
{

}
void wtp::startSilentIntervalTimer()
{
	sleep(silent_interval);
	_fsm.silent_Int_Timer_Expire();
}
void wtp::ignoreCapwapMessage()
{

}

/*
 * Parse Join Response and return informations in *valuesPtr.
 */
bool wtp :: CWParseResultCode(CWProtocolMessage *msgPtr, int len, int *valPtr) {
	CWParseMessageElementStart();

	valPtr = new int();
	*valPtr = CWProtocolRetrieve32(msgPtr);
	if(*valPtr!=0)
		cout<<"Error code not set correctly"<<endl;
//	CWLog("Result Code: %d",	*valPtr);
	return true;
	//CWParseMessageElementEnd();
}

bool wtp::CWParseJoinResponseMessage(char *msg,int len,int *seqNumPtr)
	{
		CWControlHeaderValues controlVal;
		CWProtocolTransportHeaderValues transportVal;
		//int offsetTillMessages, i, j;
		char tmp_ABGNTypes;
		CWProtocolMessage completeMsg;

		if(msg == NULL || seqNumPtr == NULL)
			return 0;

		completeMsg.msg = msg;
		completeMsg.offset = 0;

		bool dataFlag = false;

		if(!(CWParseTransportHeader(&completeMsg, &transportVal, &dataFlag, NULL))) return false;
		/* will be handled by the caller */
		if(!(CWParseControlHeader(&completeMsg, &controlVal))) return false;

		/* different type */
		if(controlVal.messageTypeValue != CW_MSG_TYPE_VALUE_JOIN_RESPONSE)
			cout<<"Message is not Join Response as Expected"<<endl;
		*seqNumPtr = controlVal.seqNum;

		/* skip timestamp */
		//controlVal.msgElemsLen -= CW_CONTROL_HEADER_OFFSET_FOR_MSG_ELEMS;

		//ACInfoPtr = new CWACInfoValues();
		//ACInfoPtr->name = new char[1];

		cout<<completeMsg.offset<<endl;

	//	offsetTillMessages = completeMsg.offset;
		//while((completeMsg.offset-offsetTillMessages) < controlVal.msgElemsLen) {
			//	unsigned short int type=0;	/* = CWProtocolRetrieve32(&completeMsg); */
				//unsigned short int len=0;	/* = CWProtocolRetrieve16(&completeMsg); */

				//CWParseFormatMsgElem(&completeMsg,&type,&len);
			//	CWDebugLog("Parsing Message Element: %u, len: %u", type, len);

				//switch(type) {
					//case CW_MSG_ELEMENT_AC_DESCRIPTOR_CW_TYPE:
						/* will be handled by the caller */
						//if(!(CWParseACDescriptor(&completeMsg, len, ACInfoPtr))) return false;
						//break;
					//case CW_MSG_ELEMENT_IEEE80211_WTP_RADIO_INFORMATION_CW_TYPE:
						/* will be handled by the caller */
						//if(!(CWParseWTPRadioInformation_FromAC(&completeMsg, len, &tmp_ABGNTypes))) return false;
						//break;
					//case CW_MSG_ELEMENT_AC_NAME_CW_TYPE:
						/* will be handled by the caller */
						//if(!(CWParseACName(&completeMsg, len, &(ACInfoPtr->name)))) return false;
						//break;
					//case CW_MSG_ELEMENT_CW_CONTROL_IPV4_ADDRESS_CW_TYPE:
						/*
						 * just count how many interfacess we have,
						 * so we can allocate the array
						 */
						//ACInfoPtr->IPv4AddressesCount++;
						//completeMsg.offset += len;
						//break;
					//case CW_MSG_ELEMENT_CW_CONTROL_IPV6_ADDRESS_CW_TYPE:
						/*
						 * just count how many interfacess we have,
						 * so we can allocate the array
						 */
						//ACInfoPtr->IPv6AddressesCount++;
						//completeMsg.offset += len;
						//break;
					//default:
						//cout<<"Invalid Format"<<endl;
						//return false;
				//}

				/* CWDebugLog("bytes: %d/%d",
				 * 	      (completeMsg.offset-offsetTillMessages),
				 * 	      controlVal.msgElemsLen);
				 */
			//}

			/*if (completeMsg.offset != len)
				return false;

			/* actually read each interface info */
			//ACInfoPtr->IPv4Addresses = new CWProtocolIPv4NetworkInterface[ACInfoPtr->IPv4AddressesCount];
			//i = 0, j = 0;
			//completeMsg.offset = offsetTillMessages;
			//while((completeMsg.offset-offsetTillMessages) < controlVal.msgElemsLen) {

			unsigned short int type=0;	/* = CWProtocolRetrieve32(&completeMsg); */
			unsigned short int length=0;	/* = CWProtocolRetrieve16(&completeMsg); */

			CWParseFormatMsgElem(&completeMsg,&type,&length);

			//switch(type) {
				//case CW_MSG_ELEMENT_CW_CONTROL_IPV4_ADDRESS_CW_TYPE:
					/* will be handled by the caller */
			if(!(CWParseResultCode(&completeMsg, len, &valuesPtr->code)))
							return false;
							/*i++;
							break;
				default:
					completeMsg.offset += len;
					break;
						 }
			}*/
		cout<<"Join Response Parsed successfully"<<endl;

		return true;
	}

bool CWParseVendorMessage(char *msg, int len, void **valuesPtr) {
	return true;
}

bool CWEncodeDecodeVendorSpecificIE(CWProtocolMessage *msgPtr)
{
	msgPtr->msg = "Hello";
	msgPtr->offset += 10;
	return true;
}

bool CWParseConfigurationUpdateRequest (char *msg,int len,int *seqNumPtr,CWProtocolMessage *msgPtr) {

	CWControlHeaderValues controlVal;
	CWProtocolTransportHeaderValues transportVal;
	//int offsetTillMessages, i, j;
	//char tmp_ABGNTypes;
	CWProtocolMessage completeMsg;

	if(msg == NULL || seqNumPtr == NULL)
		return 0;

	completeMsg.msg = msg;
	completeMsg.offset = 0;

	bool dataFlag = false;

	if(!(CWParseTransportHeader(&completeMsg, &transportVal, &dataFlag, NULL))) return false;
	/* will be handled by the caller */
	if(!(CWParseControlHeader(&completeMsg, &controlVal))) return false;

	/* different type */
	if(controlVal.messageTypeValue != CW_MSG_TYPE_VALUE_CONFIGURE_RESPONSE)
	{
		cout<<"Message is not Configuration Status Response as Expected"<<endl;
		return false;
	}
	*seqNumPtr = controlVal.seqNum;

	/* skip timestamp */
	//controlVal.msgElemsLen -= CW_CONTROL_HEADER_OFFSET_FOR_MSG_ELEMS;

	//ACInfoPtr = new CWACInfoValues();
	//ACInfoPtr->name = new char[1];

	int offset = completeMsg.offset;
	cout<<completeMsg.offset<<endl;

	unsigned short int type=0;	/* = CWProtocolRetrieve32(&completeMsg); */
	unsigned short int length=0;	/* = CWProtocolRetrieve16(&completeMsg); */

	CWParseFormatMsgElem(&completeMsg,&type,&length);

		//switch(type) {
			//case CW_MSG_ELEMENT_CW_CONTROL_IPV4_ADDRESS_CW_TYPE:
				/* will be handled by the caller */
	//msgPtr->offset = 5
	//msgPtr->msg  //was the begin of the original message, not the begin of the Vendor specific
	memcpy(&msgPtr, &completeMsg, sizeof(CWProtocolMessage));  //save the begin of the original message
	if(!(CWEncodeDecodeVendorSpecificIE(&completeMsg)))
	{
		return false;
	}
	msgPtr->msg    = msgPtr->msg + msgPtr->offset;
	msgPtr->offset = completeMsg.offset - msgPtr->offset;	//this is the total length of vendor specific IE

	cout<<"Configure Update Request Parsed successfully"<<endl;
	return true;
}

bool CWDecodeVendorSpecificIE(CWProtocolMessage *msgPtr)
{
	return true;
}

bool wtp::CWParseConfigStatusResponseMessage(char *msg,int len,int *seqNumPtr)
	{
		CWControlHeaderValues controlVal;
		CWProtocolTransportHeaderValues transportVal;
		//int offsetTillMessages, i, j;
		//char tmp_ABGNTypes;
		CWProtocolMessage completeMsg;

		if(msg == NULL || seqNumPtr == NULL)
			return 0;

		completeMsg.msg = msg;
		completeMsg.offset = 0;

		bool dataFlag = false;

		if(!(CWParseTransportHeader(&completeMsg, &transportVal, &dataFlag, NULL))) return false;
		/* will be handled by the caller */
		if(!(CWParseControlHeader(&completeMsg, &controlVal))) return false;

		/* different type */
		if(controlVal.messageTypeValue != CW_MSG_TYPE_VALUE_CONFIGURE_RESPONSE)
		{
			cout<<"Message is not Configuration Status Response as Expected"<<endl;
			return false;
		}
		*seqNumPtr = controlVal.seqNum;

		/* skip timestamp */
		//controlVal.msgElemsLen -= CW_CONTROL_HEADER_OFFSET_FOR_MSG_ELEMS;

		//ACInfoPtr = new CWACInfoValues();
		//ACInfoPtr->name = new char[1];

		cout<<completeMsg.offset<<endl;
		unsigned short int type=0;	/* = CWProtocolRetrieve32(&completeMsg); */
		unsigned short int length=0;	/* = CWProtocolRetrieve16(&completeMsg); */

		CWParseFormatMsgElem(&completeMsg,&type,&length);

		//switch(type) {
				//case CW_MSG_ELEMENT_CW_CONTROL_IPV4_ADDRESS_CW_TYPE:
					/* will be handled by the caller */
			if(!(CWDecodeVendorSpecificIE(&completeMsg)))
							return false;
							/*i++;
							break;
				default:
					completeMsg.offset += len;
					break;
						 }
			}*/
		cout<<"Configure status Response Parsed successfully"<<endl;
		return true;
	}
	

//CWP_VendorSpecificIeDecodeEncode(CWProtocolMessage *msgPtr)
//msgPtr->msg:  	point to the beginning of the <Config Update Request> message
//msgPtr->offset:	value of the offset to the beginning of the whole Vendor Specific IE
bool CWPasreElemVendorSpecificPayloadFromRx(CWProtocolMessage *msgPtr)
{  bool ret = true;
	printf("\nCAPWAP: CWPasreElemVendorSpecificPayloadFromRx:  enter msgPtr...\n");
	if(msgPtr == NULL)
	{
		printf("CAPWAP: no for vendor payload");
		return false;
	}	
	printf("CAPWAP: CWPasreElemVendorSpecificPayloadFromRx:  start parse CW IE header");
	// retrieves 16 bits from the message, increments the current offset in bytes.
   short ie_type     = CWProtocolRetrieve16(msgPtr);
	short rx_ie_len   = CWProtocolRetrieve16(msgPtr);
	long  vendor_id   = CWProtocolRetrieve32(msgPtr);
	
	printf("CAPWAP:  Rx Vendor Specific IE:  ie_type=%04x,  vendor_id=%08x\n", ie_type, vendor_id);
	if( (ie_type!= (short) CW_IE_VENDOR_SPECIFIC_TYPE) || (vendor_id != CW_IE_VENDOR_SPECIFIC_ID) )
	{
		printf("CAPWAP:  Rx wrong Vendor Specific IE:  ie_type=%04x,  vendor_id=%08x\n", ie_type, vendor_id);
		return false;
	}
	printf("CAPWAP: CWPasreElemVendorSpecificPayloadFromRx:  done parse CW IE header");	
	uint8_t *cIE_Msg_ptr = reinterpret_cast<uint8_t *>(msgPtr->msg + msgPtr->offset);
	printf("CAPWAP: CWPasreElemVendorSpecificPayloadFromRx: cIE starting offset=%d\n", msgPtr->offset);	
	
	WTP_cIE_type *curr_cIE_ptr = nullptr;
	uint8_t  curr_offset, rx_len;
	rx_len = rx_ie_len;
	while( rx_len >= WTP_CIE_MIN_LEN)
	{
		if( ((curr_cIE_ptr = wtp_Get_cIE_Parser_Obj(cIE_Msg_ptr))!=nullptr) )
		{
			curr_cIE_ptr->deserialize(cIE_Msg_ptr);
			curr_cIE_ptr->set();
			curr_cIE_ptr->serialize(cIE_Msg_ptr);
			curr_offset = curr_cIE_ptr->getPtrOffset4NextIE();
			
		}
		else
		{
			ret = false;
			printf ("CWPasreElemVendorSpecificPayloadFromRx: unknown IE=%02x\n", cIE_Msg_ptr[0]);
			curr_offset = (cIE_Msg_ptr[3] + 4);		//cIE's data length + cIE header length
		}
		cIE_Msg_ptr += curr_offset;
		rx_len -= curr_offset;		
	}
	msgPtr->offset += rx_ie_len + CW_IE_VENDOR_SPECIFIC_ID_LEN;
	return ret;
}

	
	
void wtp::sendJoinReqtoSelectedAc()
{
	int min = disc_interval;
	int max = max_disc_interval;
	char buf[CW_BUFFER_SIZE];
	int readBytes;
	printf("***sendJoinReqtoSelectedAc enter#1\n");		//?????charles
	int seqNumber = CWGetSeqNum();
	CWProtocolMessage *msgPtr = NULL;		//original
	printf("***sendJoinReqtoSelectedAc enter#2\n");		//?????charles
	if(!(CWAssembleJoinRequest(&msgPtr,seqNumber)))
		exit(1);
	printf("***sendJoinReqtoSelectedAc: calling CWNetworkInitSocketClientWithPort\n");		//?????charles
	
	if(socket_c)
			closeSocket(&socket_c);
		if(!(CWNetworkInitSocketClientWithPort(&socket_c, CW_CONTROL_PORT)))
			cout<<"Could not initialize socket"<<endl;

		sleep( min + (rand() % (max-min)));

	printf("***sendJoinReqtoSelectedAc calling sendReceiveJoin\n");		//?????charles
		//&ACInfoPtr->IPv4Addresses[0].addr.sin_addr ;
	 if(!(sendReceiveJoin(&socket_c,CW_CONTROL_PORT,&ACInfoPtr->IPv4Addresses[0].addr,msgPtr->msg,msgPtr->offset,buf,CW_BUFFER_SIZE-1, &readBytes)))
			cout<<"Join request failed"<<endl;
	 if(!(CWParseJoinResponseMessage(buf, readBytes, &seqNumber)))
	 	{
	 		cout<<"error parsing join response"<<endl;
	 	}
	 printf("***sendJoinReqtoSelectedAc exit\n");		//?????charles
}

void wtp::sendWtpReq()
{
	int sock = 0;
	cout<<"started sending wtp request every 3 seconds"<<endl;
	//char buf[CW_BUFFER_SIZE];
	//int readBytes;
	int seqNumber = CWGetSeqNum();
	CWProtocolMessage *msgPtr = NULL;
	int portNum = 12225;
	if(!(CWAssembleWtpRequest(&msgPtr,seqNumber)))
		exit(1);

	if(sock)
		closeSocket(&sock);
	if(!(sendRequest(&sock,portNum,&ACInfoPtr->IPv4Addresses[0].addr,msgPtr->msg,msgPtr->offset)))
		cout<<"Could not send wtp event request"<<endl;
	else
		cout<<"wtp event request sent successfully"<<endl;
}

void *sendThread(void *threadid)
{
   wtp thisContext;
   long tid;
   tid = (long)threadid;
   cout << "Hello World! Thread ID, " << tid << endl;
  // while(1)
   {
	   //sleep(3);
	   thisContext.sendWtpReq();
   }
   pthread_exit(NULL);
}


void wtp::sendConfigStatusReq()
{
	char buf[CW_BUFFER_SIZE];
	int readBytes;
	int seqNumber = CWGetSeqNum();
	CWProtocolMessage *msgPtr = NULL;
	if(!(CWAssembleConfigRequest(&msgPtr,seqNumber)))
		exit(1);

	if(socket_c)
			closeSocket(&socket_c);
		if(!(CWNetworkInitSocketClientWithPort(&socket_c, CW_CONTROL_PORT)))
			cout<<"Could not initialize socket"<<endl;

		//sleep( min + (rand() % (max-min)));

		//&ACInfoPtr->IPv4Addresses[0].addr.sin_addr ;
	 if(!(sendReceiveJoin(&socket_c,CW_CONTROL_PORT,&ACInfoPtr->IPv4Addresses[0].addr,msgPtr->msg,msgPtr->offset,buf,CW_BUFFER_SIZE-1, &readBytes)))
			cout<<"Configuration status request failed"<<endl;
	if(!(CWParseConfigStatusResponseMessage(buf, readBytes, &seqNumber)))
		cout<<"error parsing configuration status response"<<endl;
	cout<<"send wtp request periodically"<<endl;
	sendWtpReq();
	/*int i =0 ;
	int rc = pthread_create(&thread, NULL,sendThread, (void *)i);
	if (rc)
	{
	   cout << "Error:unable to create thread," << rc << endl;
	   exit(-1);
	}
*/
}

void wtp::configUpdateResponse()
{
	char buf[CW_BUFFER_SIZE];
	CWProtocolMessage vendorElem;
	int readBytes;
	int seqNumber;
	int portNum = 12225;
	CWProtocolMessage *msgPtr = NULL;
	CWProtocolConfigurationUpdateRequestValues values;
	if(socket_c)
		closeSocket(&socket_c);
	if(!(CWNetworkInitSocketClientWithPort(&socket_c, 12226)))
		cout<<"Could not initialize socket"<<endl;
	if(!(receiveRequest(&socket_c,buf,CW_BUFFER_SIZE-1, &readBytes)))
		cout<<"Error receiving configuration update request"<<endl;
	else cout<<"Success"<<endl;
	if(!CWParseConfigurationUpdateRequest(buf, readBytes,&seqNumber,&vendorElem))
		cout<<"error parsing configuration status response"<<endl;
	if(!(CWAssembleConfigUpdateResponse(&msgPtr,seqNumber,&vendorElem)))
	{
		cout<<"could not assemble config update response"<<endl;
		exit(1);
	}
	cout<<"test"<<endl;
	if(socket_c)
		closeSocket(&socket_c);
	if(!(sendRequest(&socket_c,portNum,&ACInfoPtr->IPv4Addresses[0].addr,msgPtr->msg,msgPtr->offset)))
		cout<<"Could not send configuration update response"<<endl;
	else
		cout<<"configuration update response sent successfully"<<endl;
}

void wtp::startDataChannelKeepAliveTimer()
{

}
void wtp::sendKeepAlivePacket()
{

}
void wtp::startEchoIntervalTimer()
{

}
void wtp::startDataChannelDeadIntervalTimer()
{

}


void wtp::sendStationConfigResp()
{

}































































