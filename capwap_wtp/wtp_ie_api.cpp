#include <iostream>
#define  WTP_CIE_PARSE_SRC		1

#include "wtp_ie_api.hh"
#define	WTP_CIE_BASE_OV_SZ	4


#ifndef __WTP_SIM__
static WtpWiFiConfig *g_pWiFiCfg = nullptr;
#else
#define	WTP_SIM_NUM_IF		2
#endif
static map <uint8_t, WTP_cIE_type * > g_cIE_Parse_Map;


/***2*****2*********4***************************************************************
 * IE * iLen * vendor-id *  c-IE  *  c-IE ...
*********************************************************************************
* c-IE  => chameloen Vendor IE and its format as below
* ***1***********1*********1*********1*******n**********
*   m_cIE * m_IfIndex * m_Status * m_Len * Data value
* *******************************************************/
bool WTP_cIE_Channel::set()
{  
	int chan = static_cast <int> (m_chan); 
	
	m_Status = true;	
	
#ifndef __WTP_SIM__	
	if( (g_pWiFiCfg==nullptr) || (g_pWiFiCfg->GetMax_DeviceCount() < m_IfIndex) || (m_Len > WTP_IE_DEFAULT_VLEN_LIMIT) )
	{   
		m_Status = false;	 
	}
	else
	{
		m_Status = g_pWiFiCfg->radio[m_IfIndex].channel.SetVal(chan);
	}
#else
	if( (WTP_SIM_NUM_IF < m_IfIndex) || (vlen > WTP_IE_DEFAULT_VLEN_LIMIT) || (m_Len > WTP_IE_DEFAULT_VLEN_LIMIT) )
	{   
		m_Status = false;
	}
#endif
	if(m_Status==true)
	{
		m_chan = static_cast<uint8_t>(chan);
	}
	return m_Status;
}



bool WTP_cIE_Channel::get()
{
	m_Status = true;
	m_Len = WTP_IE_DEFAULT_VLEN_LIMIT;

#ifndef __WTP_SIM__		
	if ( (g_pWiFiCfg==nullptr) || (m_IfIndex > g_pWiFiCfg->GetMax_DeviceCount()) )
	{  
		m_Status = false;	
	}
	else 
	{  try
		{
			int chan =g_pWiFiCfg->radio[m_IfIndex].channel.GetVal();
			if (chan < 0)
			{ 
				m_Status    = false;
			}
			else
			{
				m_chan = static_cast<uint8_t> (chan);	
			}
		}
		catch(...)
		{
			m_Status = false;	
		}
	}
#else
	if ( m_IfIndex > WTP_SIM_NUM_IF )
	{  
		m_Status = false;	
	}
#endif
	
	if(m_Status==false)	
	{
		m_chan  = 0;
	}
	return m_Status;
}

int WTP_cIE_Channel::getPtrOffset4NextIE()
{ 
		return (WTP_CIE_BASE_OV_SZ + sizeof(m_chan)); 
}


bool WTP_cIE_Txpower::set()
{  int pwr = static_cast <int> (m_txpwr); 
	
	m_Status = true;
#ifndef __WTP_SIM__
	if( (g_pWiFiCfg==nullptr) || (g_pWiFiCfg->GetMax_DeviceCount() < m_IfIndex) || (m_Len > WTP_IE_DEFAULT_VLEN_LIMIT) )
	{ 	 
		 m_Status = false;	
	}
	else 
	{
		m_Status =  g_pWiFiCfg->radio[m_IfIndex].txpower.SetVal(pwr);
	}
#else	
	if( (WTP_SIM_NUM_IF < m_IfIndex) || (vlen > WTP_IE_DEFAULT_VLEN_LIMIT) || (m_Len > WTP_IE_DEFAULT_VLEN_LIMIT) )
	{ 	 
		 m_Status = false;
	}   
#endif
	if(m_Status==true)
	{
		m_txpwr = static_cast<uint8_t>(pwr);
	}
	return m_Status;
}


bool WTP_cIE_Txpower::get()
{	
	m_Status = true;
	m_Len = WTP_IE_DEFAULT_VLEN_LIMIT;
	
#ifndef __WTP_SIM__	
	if ( (g_pWiFiCfg==nullptr) || (m_IfIndex > g_pWiFiCfg->GetMax_DeviceCount()) )
	{  
		m_Status = false;	
	}
	else 
	{  try
		{
			int pwr =g_pWiFiCfg->radio[m_IfIndex].txpower.GetVal();
			if (pwr < 0)
			{ 
				m_Status = false;
			}
			else
			{
				m_txpwr = static_cast<uint8_t> (pwr);				
			}
		}
		catch(...)
		{
			m_Status = false;	
		}
	}
#else
	if ( m_IfIndex > WTP_SIM_NUM_IF )
	{  
		m_Status = false;	
	}
#endif
	
	if(m_Status==false)
	{
		m_txpwr   = 0;
	}	
	return m_Status;
}


int WTP_cIE_Txpower::getPtrOffset4NextIE()
{  
		return (WTP_CIE_BASE_OV_SZ + sizeof(m_txpwr)); 
}


bool WTP_cIE_SSID::set()
{  
	string new_ssid((char *) m_ssid);
	
	m_Status = true;
	cout << "WTP_cIE_SSID::set: m_ssid=" << new_ssid << endl;
	if( (strlen((char *) m_ssid) > WTP_IE_SSID_VLEN_LIMIT) || (m_Len > WTP_IE_SSID_VLEN_LIMIT) )
	{
		m_Status = false;
		return false;
	}
	
#ifndef __WTP_SIM__	
	if( (g_pWiFiCfg==nullptr) || (g_pWiFiCfg->GetMax_InterfaceCount() < m_IfIndex) || (new_ssid.length() >WTP_IE_SSID_VLEN_LIMIT)  )
	{   
		m_Status = false;	
	}
	else
	{
		m_Status = g_pWiFiCfg->wifiIface[m_IfIndex].ssid.SetVal(new_ssid);
	}
#else
	if( (WTP_SIM_NUM_IF < m_IfIndex) || (vlen > WTP_IE_SSID_VLEN_LIMIT) || (new_ssid.length() >WTP_IE_SSID_VLEN_LIMIT)  )
	{   
		m_Status = false;
	}	
#endif
	return m_Status;
}

bool WTP_cIE_SSID::get()
{	
	m_Status = true;
	m_Len = WTP_IE_SSID_VLEN_LIMIT;
	string new_ssid;
	
#ifndef __WTP_SIM__	
	memset(m_ssid, 0, WTP_IE_SSID_VLEN_LIMIT);
	if ( (g_pWiFiCfg==nullptr) || (m_IfIndex > g_pWiFiCfg->GetMax_InterfaceCount()) )
	{  
		m_Status = false;	
	}
	else 
	{  try
		{
			new_ssid =g_pWiFiCfg->wifiIface[m_IfIndex].ssid.GetVal();
			if (new_ssid.empty())
			{ 
				m_Status = false;
			}
			else
			{
				memcpy(m_ssid, new_ssid.c_str(), new_ssid.length());
			}
		}
		catch(...)
		{
			m_Status = false;	
		}
	}
#else
	if ( m_IfIndex > WTP_SIM_NUM_IF )
	{  
		m_Status = false;	
	}
	new_ssid = string(m_ssid);
#endif
	return m_Status;
}


int WTP_cIE_SSID::getPtrOffset4NextIE()
{  
		return  (WTP_CIE_BASE_OV_SZ + sizeof(m_ssid)); 
}


bool WTP_cIE_MAC::set()
{	string new_mac((char *) m_mac);

	m_Status = true;
	
	if( (strlen((char *) m_mac) > WTP_IE_MAC_VLEN_LIMIT) || (m_Len > WTP_IE_MAC_VLEN_LIMIT) )
	{
		m_Status = false;
		return false;
	}	
	
#ifndef __WTP_SIM__
	if( (g_pWiFiCfg==nullptr) || (g_pWiFiCfg->GetMax_InterfaceCount() < m_IfIndex) || (new_mac.length() > WTP_IE_MAC_VLEN_LIMIT) )
	{   
		m_Status = false;
	}
	else
	{
		m_Status = g_pWiFiCfg->wifiIface[m_IfIndex].mac.SetVal(new_mac);
	}
#else
	if( (WTP_SIM_NUM_IF < m_IfIndex) || (vlen > WTP_IE_MAC_VLEN_LIMIT) || (new_mac.length() > WTP_IE_MAC_VLEN_LIMIT) )
	{   
		m_Status = false;
	}
#endif
	return m_Status;
}

bool WTP_cIE_MAC::get()
{	
	m_Status = true;
	m_Len = WTP_IE_MAC_VLEN_LIMIT;
	
	string new_mac;
	
#ifndef __WTP_SIM__	
	memset(m_mac, 0, WTP_IE_MAC_VLEN_LIMIT);
	if ( (g_pWiFiCfg==nullptr) || (m_IfIndex > g_pWiFiCfg->GetMax_InterfaceCount()) )
	{  
		m_Status = false;	
	}
	else 
	{  try
		{
			new_mac =g_pWiFiCfg->wifiIface[m_IfIndex].mac.GetVal();
			if (new_mac.empty())
			{ 
				m_Status = false;
			}
			else
			{
				memcpy(m_mac, new_mac.c_str(), new_mac.length());				
			}
		}
		catch(...)
		{
			m_Status = false;	
		}
	}
#else
	if ( m_IfIndex > WTP_SIM_NUM_IF )
	{  
		m_Status = false;	
	}
	new_mac = string(m_mac);
#endif
	return m_Status;
}


int WTP_cIE_MAC::getPtrOffset4NextIE()
{  
		return (WTP_CIE_BASE_OV_SZ + sizeof(m_mac)); 
}

//the following radio disable API is not implemented yet
bool WTP_cIE_Radio_Disable::set()
{
	return true;
}

bool WTP_cIE_Radio_Disable::get()
{	
	return true;
}


int WTP_cIE_Radio_Disable::getPtrOffset4NextIE()
{  
		return (WTP_CIE_BASE_OV_SZ + sizeof(m_disable)); 
}


//if the initialize failed(false), the configure parser can not use.
bool wtp_initialize_cfg_access(void)
{
	
	try
	{
#ifndef __WTP_SIM__		
		if( (g_pWiFiCfg = WtpWiFiConfig::getInstance())==nullptr)
		{
			return false;
		}
		if(g_pWiFiCfg->ConfigLoad()==false)
		{
			  return false;
		}	
#endif		
		g_cIE_Parse_Map.insert(make_pair(WTP_IE_CHANNEL_CID, new WTP_cIE_Channel(0)) );	
		g_cIE_Parse_Map.insert(make_pair(WTP_IE_TXPOWER_CID, new WTP_cIE_Txpower(0)) );
		g_cIE_Parse_Map.insert(make_pair(WTP_IE_SSID_CID, new WTP_cIE_SSID(0)) );
		g_cIE_Parse_Map.insert(make_pair(WTP_IE_MAC_CID, new WTP_cIE_MAC(0)) );
		g_cIE_Parse_Map.insert(make_pair(WTP_IE_DISABLE_CID, new WTP_cIE_Radio_Disable(0)) );
		cout << "wtp_initialize_cfg_access success" << endl;
		return true;
	}
	catch(...)
	{
		cout << "wtp_initialize_cfg_access exception" << endl;
	}
	return false;	
}


//if the vendor specific IE (chameleon's config IE is invalid, return nullptr
WTP_cIE_type *wtp_Get_cIE_Parser_Obj(uint8_t *ptr_cIE)
{
	WTP_cIE_type *wtp_cfg_ptr=nullptr;
	try
	{
		if ( (ptr_cIE[0] < WTP_IE_FIRST_CID) || (ptr_cIE[0] > WTP_IE_LAST_CID) )
		{
			cout << "wtp_Get_cIE_Parser_Obj invalid cIE=" << setfill('0') << setw(2) << hex << static_cast <int> (ptr_cIE[0]) << endl;
			return nullptr;
		}
		wtp_cfg_ptr = g_cIE_Parse_Map.at(ptr_cIE[0]);
		if(wtp_cfg_ptr!=nullptr)
		{	cout << "Found wtp_Get_cIE_Parser_Obj for cIE=" << setfill('0') << setw(2) << hex << static_cast <int> (ptr_cIE[0]) << endl;
			wtp_cfg_ptr->setIfIndex(ptr_cIE[1]);
		}
	}
	catch(...)
	{
		wtp_cfg_ptr = nullptr;
		cout << "wtp_Get_cIE_Parser_Obj cannot find the IE exception . . .\n" << endl;
	}
	return wtp_cfg_ptr;	
}


int GetNumOfWiFiRadio()
{
	if(g_pWiFiCfg==nullptr)
	{
		return 0;
	}
	else
	{
		g_pWiFiCfg->GetMax_DeviceCount();
	}	
}


int GetNumOfWiFiInterface()
{
	if(g_pWiFiCfg==nullptr)
	{
		return 0;
	}
	else
	{
		g_pWiFiCfg->GetMax_InterfaceCount();
	}	
}


