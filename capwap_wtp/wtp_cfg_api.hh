#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <errno.h>
#include <unistd.h>
#include <string>
#include <map>
#include <bitset>
#include <initializer_list>

using namespace std;

typedef enum
{
	NONE,
	DHCP,
	NETWORK,
	WIRELESS,
	MAX_PARM_TYPE
} pCfg_t;

typedef enum
{
	DEVICE,
	WIFI_IF
} pWifiSec_t;

typedef enum
{	NO_ACT,	//this is
	GET,
	SET,
	ACK,
	NACK,
	NOTIFY
} pState_t;

typedef enum
{	NO_VAL,
	STR_VAL,
	ENUM_VAL,
	BOOL_VAL
} pWtpParamValType_t;



//this is the option parameter from UCI point of view.
class WtpParam
{
   pWtpParamValType_t 	m_ValType;
   pState_t					m_State;
   pWifiSec_t				m_SecType;
  public:
	//	WtpParam() { };	//this is used for reference
		WtpParam(pCfg_t c, pWifiSec_t s, string p, pWtpParamValType_t t):m_Conf(c), m_SecType(s), m_Id(p), m_ValType(t)
		{ 
			m_Path.clear(); 
			m_State = NO_ACT;
		};
		pCfg_t	m_Conf;
		string 	m_Id;
		string 	m_Path;
		void SetState(pState_t a) { m_State = a; };
		pState_t Get_State() { return m_State; };
		pWtpParamValType_t getValType()  { return m_ValType; };
		virtual ~WtpParam() {};
};


class WtpVparam: public WtpParam
//class WtpVparam: public WtpParam
{
	public:
		int  m_Val;
		//WtpVparam() { };	//this is used for reference
		WtpVparam(pCfg_t c, pWifiSec_t s, string p, int v=0):WtpParam(c, s, p, ENUM_VAL), m_Val(v) { };
		//base on the actual derive parameter to set the value and perform the checking, if the checking is OK, the state is SET.
		//if the setting on the UCI option is faild, return false and the state will be NACK, otherwise ACK if success
		//it is the responsibility for the derive parameter class to set on the corresponding UCI option.
		virtual ~WtpVparam() { };
		
		virtual bool SetVal(int val)=0;
		//it is the responsibility for the derive parameter class to get from the corresponding UCI option.
		//if the get from the UCI option is faild, return false and the state will be NACK, otherwise ACK if success
		virtual int GetVal()=0;
		//if invalid value for the device parameter, the return NULL, otherwise return valid enum name;
		virtual const string int2Str(int i)=0;
		//if valid enum name to associate with value, the return value -1, otherwise return valid index of value.
		virtual int str2Int(const string &s)=0;
};


class WtpBparam: public WtpParam
{
  public:
		bool m_Val;
		//WtpBParam():WtpParam() { };	//this is used for reference
		WtpBparam(pCfg_t c, pWifiSec_t s, string p, bool v=0):WtpParam(c, s, p, BOOL_VAL), m_Val(v) { }	;
		//if the setting on the UCI option is faild, return false and the state will be NACK, otherwise ACK if success
		//it is the responsibility for the derive parameter class to set on the corresponding UCI option.
		virtual bool SetVal(bool val)=0;
		//it is the responsibility for the derive parameter class to get from the corresponding UCI option.
		//if the get from the UCI option is faild, return false and the state will be NACK, otherwise ACK if success
		virtual bool GetVal()=0;
		virtual ~WtpBparam() {};
};


class WtpSparam: public WtpParam
{int m_Max;

  public:
		string m_Val;
		//WtpSparam() { };	//this is used for reference
		WtpSparam(pCfg_t c, pWifiSec_t s, string p, int maxv=80):WtpParam(c, s, p, STR_VAL), m_Max(maxv) { m_Val.clear(); };		
		void SetMaxLenth(int max)
		{
			m_Max = max;
		}
		//base on the actual derive parameter to set the value and perform the checking, if the checking is OK, the state is SET.
		//if the setting on the UCI option is faild, return false and the state will be NACK, otherwise ACK if success
		//it is the responsibility for the derive parameter class to set on the corresponding UCI option.
		bool SetVal(string val);
		//it is the responsibility for the derive parameter class to get from the corresponding UCI option.
		//if the get from the UCI option is faild, return false and the state will be NACK, otherwise ACK if success
		string GetVal();
		//if name is out of range, it return false.
		bool RangeCheck(const string &s)
		{
			if(s.length() > m_Max)
			{
				return false;
			}
			else
			{
				return true;
			}
		}
		virtual ~WtpSparam() {};
};


//=====WiFi-Device Options=====

//***radio: type Parameter
class WtpRadioTypeParam: public WtpSparam
{
  public:
	WtpRadioTypeParam(string val="mac80211"):WtpSparam(WIRELESS, DEVICE, "type", 12) {  };
   //implement uci_set for the option: wireless.radio[].type='mac80211'
};


//***radio: hwmode Parameter
class WtpRadioHwModeParam: public WtpVparam
{  const  map<std::string, int> DictStr2Int = { {"11b", 0}, {"11g", 1}, {"11n", 2}, {"11a", 3} };
   const  map<int, std::string> DictInt2Str = { {0, "11b"}, {1, "11g"}, {2, "11n"}, {3, "11a"} };
  public:
	WtpRadioHwModeParam(int val=0):WtpVparam(WIRELESS, DEVICE, "hwmode") { };
	bool SetVal(int val);	//implement uci_set for the option: wireless.radio[].channel='mac80211'
	int GetVal();		//implement uci_lookup_ptr for the option: wireless.radio[].channel

	//if invalid value for the device parameter, the return NULL, otherwise return valid enum name;
	const string int2Str(int i)
	{  if(i > 3)
		{  
			return NULL;
		}
	   else
		{				      
			return DictInt2Str.find(i)->second;
		}
	}

	//if valid enum name to associate with value, the return value -1, otherwise return valid index of value.
	int str2Int(const std::string &s)
	{		
		int ival = -1;
		try
		{
			ival = DictStr2Int.at(s);
		}
		catch(...)
		{
			//ifthe key is not found, return -1;
		}
		return ival;
	}
};


//***radio: htmode Parameter
class WtpRadioHtModeParam: public WtpVparam
{ const  map<string, int> DictStr2Int = { {"VHT20", 0}, {"VHT40", 1}, {"VHT80", 2}, {"VHT160", 3}};
  const  map<int, string> DictInt2Str = { {0, "VHT20"}, {1, "VHT40"}, {2, "VHT80"}, {3, "VHT160"}};
  public:
	WtpRadioHtModeParam(int val=0):WtpVparam(WIRELESS, DEVICE, "htmode", val) {  };
	bool SetVal(int val);	//implement uci_set for the option: wireless.radio[].channel='mac80211'
	int GetVal();		//implement uci_lookup_ptr for the option: wireless.radio[].channel

	//if invalid value for the device parameter, the return NULL, otherwise return valid enum name;
	const string int2Str(int i)
	{  if(i > 3)
		{  
			return NULL;
		}
	   else
		{				      
			return DictInt2Str.find(i)->second;
		}
	}	

	//if valid enum name to associate with value, the return value -1, otherwise return valid index of value.
	int str2Int(const std::string &s)
	{		
		int ival = -1;
		try
		{
			ival = DictStr2Int.at(s);
		}
		catch(...)
		{
			//ifthe key is not found, return -1;
		}
		return ival;
	}	
};


//***radio: Channel Parameter
class WtpRadioChannelParam: public WtpVparam
{
  public:
	WtpRadioChannelParam(int val=0):WtpVparam(WIRELESS, DEVICE, "channel", val) { };
	bool SetVal(int val);	//implement uci_set for the option: wireless.radio[].channel='mac80211'
	int GetVal();		//implement uci_lookup_ptr for the option: wireless.radio[].channel

	//if invalid value for the device parameter, the return NULL, otherwise return valid enum name;
   const string int2Str(int val)
	{  if(val==0)
			return "auto";
	   else if( (val > 0) && (val > 200) )
			return nullptr;
	   else
		{  char digBuf[80];
			sprintf(digBuf, "%d", val);
			return string(digBuf);
		}

	}

	 //if valid enum name to associate with value, the return value -1, otherwise return valid index of value.
	int str2Int(const string &s)
	{  if(s.compare("auto")==0)
		{
	      return 0;
		}
	   else
	   {  int ch = atoi(s.c_str());
	      if ( (ch < 0) || (ch > 200) )
				return -1;
	      else
				return ch;
      }
	}
};

//***radio: txpower Parameter
class WtpRadioTxPowerParam: public WtpVparam
{

  public:
	WtpRadioTxPowerParam(int val=0):WtpVparam(WIRELESS, DEVICE, "txpower", val) {  };
	bool SetVal(int val);	//implement uci_set for the option: wireless.radio[].channel='mac80211'
	int GetVal();		//implement uci_lookup_ptr for the option: wireless.radio[].channel

	//if invalid value for the device parameter, the return NULL, otherwise return valid enum name;
	const string int2Str(int val)
	{  
		if( (val > 0) && (val > 23) )
			return nullptr;
	   else
		{  char digBuf[80];
			sprintf(digBuf, "%d", val);
			return string(digBuf);
		}
	}

	 //if valid enum name to associate with value, the return value -1, otherwise return valid index of value.
	int str2Int(const string &s)
	{
	   int ch = atoi(s.c_str());
	   if ( (ch < 0) || (ch > 23) )
			return -1;
	   else
			return ch;
	}
};


//=====WiFi-Iface Options=====


//***WiFi-Iface: Device Parameter
class WtpWiFiIfaceDeviceParam: public WtpSparam
{
  public:
	WtpWiFiIfaceDeviceParam(string val="radio0"):WtpSparam(WIRELESS, WIFI_IF, "device", 12) {  };
	//implement uci_set for the option: wireless.wifiIface[].device='radio0'
};


//***WiFi-Iface: mode Parameter
class WtpWiFiIfaceModeParam: public WtpVparam
{ const  map<string, int> DictStr2Int = { {"ap", 0}, {"sta", 1}, {"adhoc", 2}, {"wds", 3}, {"monitor", 4}, {"mesh", 5}};
  const  map<int, string> DictInt2Str = { {0, "ap"}, {1, "sta"}, {2, "adhoc"}, {3, "wds"}, {4, "monitor"}, {5, "mesh"}};
  public:
	WtpWiFiIfaceModeParam(int val=0):WtpVparam(WIRELESS, WIFI_IF, "mode", val) {  };
	bool SetVal(int val);	//implement uci_set for the option: wireless.radio[].channel='mac80211'
	int GetVal();		//implement uci_lookup_ptr for the option: wireless.radio[].channel

	//if invalid value for the device parameter, the return NULL, otherwise return valid enum name;
	const string int2Str(int i)
	{  if(i > 5)
		{  
			return NULL;
		}
	   else
		{
			return DictInt2Str.find(i)->second;
		}
	}	

	//if valid enum name to associate with value, the return value -1, otherwise return valid index of value.
	int str2Int(const std::string &s)
	{		
		int ival = -1;
		try
		{
			ival = DictStr2Int.at(s);
		}
		catch(...)
		{
			//ifthe key is not found, return -1;
		}
		return ival;
	}		
};



//***WiFi-Iface: network Parameter
class WtpWiFiIfaceNetworkParam: public WtpSparam
{
  public:
	WtpWiFiIfaceNetworkParam(string val="lan"):WtpSparam(WIRELESS, WIFI_IF, "network", 32) { };
   //implement uci_set for the option: wireless.wifiIface[].network='lan'
};


//***WiFi-Iface: SSID Parameter
class WtpWiFiIfaceSSIDParam: public WtpSparam
{
  public:
	WtpWiFiIfaceSSIDParam(string val="OpenWrt"):WtpSparam(WIRELESS, WIFI_IF, "ssid", 32) { };
	//implement uci_set for the option: wireless.wifiIface[].ssid='OpenWrt'
};


//***WiFi-Iface: Mac Parameter
class WtpWiFiIfaceMacParam: public WtpSparam
{
	int hex2num(unsigned char c)
	{
	    if (c >= '0' && c <= '9')
		return c - '0';
	    if (c >= 'a' && c <= 'f')
		return c - 'a' + 10;
	    if (c >= 'A' && c <= 'F')
		return c - 'A' + 10;
	    return -1;
	}
	int hex2byte(const char *hex)
	{
	   int a, b;
	   a = hex2num(*hex++);
	   if (a < 0)
			return -1;
	   b = hex2num(*hex++);
	   if (b < 0)
			return -1;
	   return (a << 4) | b;
	}

  public:
	WtpWiFiIfaceMacParam(string val="00:11:22:33:44:55"):WtpSparam(WIRELESS, WIFI_IF, "macaddr", 19) { };
	//implement uci_set for the option: wireless.wifiIface[].macaddr='00:11:22:33:44:55'

	/**
 	* hwaddr_aton - Convert ASCII string to MAC address (colon-delimited format)
 	* @txt: MAC address as a string (e.g., "00:11:22:33:44:55")
 	* @addr: Buffer for the MAC address (ETH_ALEN = 6 bytes)
	 * Returns: 0 on success, -1 on failure (e.g., string not a MAC address)
 	*/
	int hwaddr_aton(const char *txt, unsigned char *addr)
	{
	   int i;
	   for (i = 0; i < 6; i++) 
		{
			int a, b;
			a = hex2num(*txt++);
			if (a < 0)
				return -1;
			b = hex2num(*txt++);
			if (b < 0)
				return -1;
			*addr++ = (a << 4) | b;
			if (i < 5 && *txt++ != ':')
				return -1;
	   }
	   return 0;
	}

	/**
 	* hwaddr_compact_aton - Convert ASCII string to MAC address (no colon delimitors format)
 	* @txt: MAC address as a string (e.g., "001122334455")
 	* @addr: Buffer for the MAC address (ETH_ALEN = 6 bytes)
 	* Returns: 0 on success, -1 on failure (e.g., string not a MAC address)
 	*/
	int hwaddr_compact_aton(const char *txt, unsigned char *addr)
	{
	   int i;
	   for (i = 0; i < 6; i++) 
		{
			int a, b;
			a = hex2num(*txt++);
			if (a < 0)
				return -1;
			b = hex2num(*txt++);
			if (b < 0)
				return -1;
			*addr++ = (a << 4) | b;
	   }
	   return 0;
	}

	/**
 	* hwaddr_aton2 - Convert ASCII string to MAC address (in any known format)
 	* @txt: MAC address as a string (e.g., 00:11:22:33:44:55 or 0011.2233.4455)
 	* @addr: Buffer for the MAC address (ETH_ALEN = 6 bytes)
 	* Returns: Characters used (> 0) on success, -1 on failure
 	*/
	int hwaddr_aton2(const char *txt, unsigned *addr)
	{
	   int i;
	   const char *pos = txt;
	   for (i = 0; i < 6; i++) 
		{
			int a, b;
			while (*pos == ':' || *pos == '.' || *pos == '-')
				pos++;
			a = hex2num(*pos++);
			if (a < 0)
				return -1;
			b = hex2num(*pos++);
			if (b < 0)
				return -1;
			*addr++ = (a << 4) | b;
	   }
	   return pos - txt;
	}

};


//***WiFi-Iface: Encryption Parameter
class WtpWiFiIfaceEncryptionParam: public WtpVparam
{ const  map<string, int> DictStr2Int = { {"wep", 0}, {"psk", 1}, {"psk2", 2}, {"wep+shared", 3}, {"wep+open", 4}, {"wep+mixed", 5}};
  const  map<int, string> DictInt2Str = { {0, "wep"}, {1, "psk"}, {2, "psk2"}, {3, "wep+shared"}, {4, "wep+open"}, {5, "wep+mixed"}};
  public:
	WtpWiFiIfaceEncryptionParam(int val=0):WtpVparam(WIRELESS, WIFI_IF, "encryption", val) { };
	bool SetVal(int val);	//implement uci_set for the option: wireless.radio[].channel='mac80211'
	int GetVal();		//implement uci_lookup_ptr for the option: wireless.radio[].channel

		//if invalid value for the device parameter, the return NULL, otherwise return valid enum name;
	const string int2Str(int i)
	{  if(i > 5)
		{  
			return NULL;
		}
	   else
		{				      
			return DictInt2Str.find(i)->second;
		}
	}	

	//if valid enum name to associate with value, the return value -1, otherwise return valid index of value.
	int str2Int(const std::string &s)
	{		
		int ival = -1;
		try
		{
			ival = DictStr2Int.at(s);
		}
		catch(...)
		{
			//ifthe key is not found, return -1;
		}
		return ival;
	}		
};


//***WiFi-Iface: key Parameter
//class WtpWiFiIfaceKeyParam: public WtpVparam, public WtpSparam
class WtpWiFiIfaceKeyParam: public WtpSparam
{
  public:
#if 0	  
	//for key indexing
	WtpWiFiIfaceKeyParam(int val):WtpVparam(WIRELESS, WIFI_IF, "key", val) { };
	bool SetVal(int val);	//implement uci_set for the option: wireless.radio[].channel='mac80211'
	int GetVal();		//implement uci_lookup_ptr for the option: wireless.radio[].channel
#endif	

	//for only WPA-PSK one key
	WtpWiFiIfaceKeyParam(string val="1"):WtpSparam(WIRELESS, WIFI_IF, "key", 32) {  };
	//implement uci_set for the option: wireless.wifiIface[].key='AABBCCDDEE11'
#if 0
	//if invalid value for the device parameter, the return NULL, otherwise return valid enum name;
	const string int2Str(int val)
	{ //not supported
	     return NULL;
	}

	//if valid enum name to associate with value, the return value -1, otherwise return valid index of value.
	int str2Int(const string &s)
	{ //not supported
	      return -1;
	}
#endif	
};



//***WiFi-Iface: key1 Parameter
class WtpWiFiIfaceKey1Param: public WtpSparam
{
  public:
	WtpWiFiIfaceKey1Param(string val="AABBCCDDEE11"):WtpSparam(WIRELESS, WIFI_IF, "key1", 32) {  };
	//implement uci_set for the option: wireless.wifiIface[].key1='AABBCCDDEE11'
};

//***WiFi-Iface: key2 Parameter
class WtpWiFiIfaceKey2Param: public WtpSparam
{
  public:
	WtpWiFiIfaceKey2Param(string val="AABBCCDDEE22"):WtpSparam(WIRELESS, WIFI_IF, "key2", 32) {  };
	//implement uci_set for the option: wireless.wifiIface[].key2='AABBCCDDEE22'
};

//***WiFi-Iface: key3 Parameter
class WtpWiFiIfaceKey3Param: public WtpSparam
{
  public:
	WtpWiFiIfaceKey3Param(string val="AABBCCDDEE33"):WtpSparam(WIRELESS, WIFI_IF, "key3", 32) {  };
	//implement uci_set for the option: wireless.wifiIface[].key3='AABBCCDDEE33'
};

//***WiFi-Iface: key4 Parameter
class WtpWiFiIfaceKey4Param: public WtpSparam
{
  public:
	WtpWiFiIfaceKey4Param(string val="AABBCCDDEE44"):WtpSparam(WIRELESS, WIFI_IF, "key4", 32) {  };
	//implement uci_set for the option: wireless.wifiIface[].key4='AABBCCDDEE44'
};


//***WiFi-Iface: MacFilter Parameter
class WtpWiFiIfaceMacFilterParam: public WtpVparam
{ const  map<string, int> DictStr2Int = { {"disable", 0}, {"whitelist", 1}, {"denylist", 2}};
  const  map<int, string> DictInt2Str = { {0, "disable"}, {1, "whitelist"}, {2, "denylist"}};
  public:
	WtpWiFiIfaceMacFilterParam(int val=0):WtpVparam(WIRELESS, WIFI_IF, "macfilter", val) { };
	bool SetVal(int val);	//implement uci_set for the option: wireless.radio[].channel='mac80211'
	int GetVal();		//implement uci_lookup_ptr for the option: wireless.radio[].channel

	//if invalid value for the device parameter, the return NULL, otherwise return valid enum name;
	const string int2Str(int i)
	{  if(i > 3)
		{  
			return NULL;
		}
	   else
		{				      
			return DictInt2Str.find(i)->second;
		}
	}	

	//if valid enum name to associate with value, the return value -1, otherwise return valid index of value.
	int str2Int(const std::string &s)
	{		
		int ival = -1;
		try
		{
			ival = DictStr2Int.at(s);
		}
		catch(...)
		{
			//ifthe key is not found, return -1;
		}
		return ival;
	}
};


//***WiFi-Iface: Maclist Parameter
class WtpWiFiIfaceMaclistParam: public WtpSparam
{
  public:
	WtpWiFiIfaceMaclistParam(string val="11:22:33:44:55:66 77:88:99:AA:BB:CC"):WtpSparam(WIRELESS, WIFI_IF, "maclist", 1024) { };
   //implement uci_set for the option: wireless.wifiIface[].maclist='11:22:33:44:55:66 77:88:99:AA:BB:CC'
};


//***WiFi-Device session
class WtpWiFiDeviceSession
{  pState_t  m_State;
   map <string, WtpParam *> m_DevMap;

   public:
    
	string m_Path;
	//===individual parameter listed below for the radio session===
	WtpRadioTypeParam 	    rdtype;
	WtpRadioHwModeParam	    hwmode;
	WtpRadioHtModeParam	    htmode;
	WtpRadioChannelParam		channel;
	WtpRadioTxPowerParam		txpower;

	//===the device session methods below===
	WtpWiFiDeviceSession();  //:m_State(NO_ACT){  };
	
	bool Is_ReadyToUse()	{  return (m_State==ACK);	}//if the radio session is not ready to use it, call the session update()
	//in order the change any parameter, the session must updated at least only once so that configure session syn-up with UCI.
	//get method from each parameter will not retireve from UCI, it returns based on what is in this configure session context.
	bool sessionUpdateFromUCI(const string &parmName, const string &parmVal);
	//based on the session update state from UCI,  setting state=ACK if the update is successful
	//otherwise the session is not usable.
	//bool sessionUpdateToUCI();
	bool addUnknownParmPath();
	//this call assume the path for session has been learned, otherwise return false.	

};


//***WiFi-Iface session
class WtpWiFiIfaceSession
{  pState_t  				  m_State;
   map <string, WtpParam *> m_IfMap;
   public:
    
	string m_Path;
	//===individual parameter listed below for the WiFi Interface session===
	WtpWiFiIfaceDeviceParam		device;
	WtpWiFiIfaceModeParam		mode;
	WtpWiFiIfaceNetworkParam	network;
	WtpWiFiIfaceSSIDParam		ssid;
	WtpWiFiIfaceMacParam			mac;
	WtpWiFiIfaceEncryptionParam	encryption;
	WtpWiFiIfaceKeyParam			key;
	WtpWiFiIfaceKey1Param		key1;
	WtpWiFiIfaceKey2Param		key2;
	WtpWiFiIfaceKey3Param		key3;
	WtpWiFiIfaceKey4Param		key4;
	WtpWiFiIfaceMacFilterParam	macfilter;
	WtpWiFiIfaceMaclistParam	maclist;

	//===the WiFi Interface session methods below===
	WtpWiFiIfaceSession();  //:m_State(NO_ACT){  };
	
	bool Is_ReadyToUse()	{  return (m_State==ACK);	}//if the iface session is not ready to use it, call the session update()
	//in order the change any parameter, the session must updated at least only once so that configure session syn-up with UCI.
	//get method from each parameter will not retireve from UCI, it returns based on what is in this configure session context.
	bool sessionUpdateFromUCI(const string &parmName, const string &parmVal);
	//based on the session update state from UCI,  setting state=ACK if the update is successful
	//otherwise the session is not usable.
	bool addUnknownParmPath();
	//this call assume the path for session has been learned, otherwise return false.
};



//***WiFi-Iface session
class WtpWiFiConfig
{  pState_t  m_State;
   #define	MaxNumOfDev	4
   #define	MaxNumOfIf	4

    //this wireless configure class is a singleton class.
    static WtpWiFiConfig *pWtpWiFiConfig;    
    WtpWiFiConfig():m_State(NO_ACT)
    {
        m_If_Cnt    = 0;
        m_Dev_Cnt   = 0;
    };
    
    int m_Dev_Cnt;
    int m_If_Cnt;	 
	 
   public:
		
	 void WtpConfigDestroy();
	//===individual session listed below for the WiFi configuration package===
	static WtpWiFiConfig *getInstance()
	{   if(pWtpWiFiConfig==nullptr)
	    {
	        pWtpWiFiConfig = new WtpWiFiConfig();
	    }
	    return pWtpWiFiConfig;
	};
	
	~WtpWiFiConfig() {   cout << "enter WtpWiFiConfig destructor" << endl;  };  //for debugging:  charles 
	WtpWiFiDeviceSession	radio[MaxNumOfDev];
	WtpWiFiIfaceSession	wifiIface[MaxNumOfIf];
	uint8_t GetMax_DeviceCount()	{  return m_Dev_Cnt; };
	uint8_t GetMax_InterfaceCount()	{  return m_If_Cnt; };

	//===the WiFi Interface session methods below===
	bool Is_ReadyToUse()	{  return (m_State==ACK);	};//if the configuration package is not ready to use it, call the package configLoad()
	//it cause all the sessions under the configuration update from the UCI configuration file for the Wireless package
	//this is typicall call the when the system start-up for the wireless feature.
	bool ConfigLoad();
	//it cause all the configurations under package save into the wireless configuration file (all the sessions within the package).
	//this is typically called when the wireless configuration is proven workable.
	bool ConfigCommit();
};




















