#include <iostream>
#include "wtp_internal.hh"
using namespace std;


WtpWirelessDB *WtpWirelessDB::pWtpWirelessDB = nullptr;

//package load and commit
bool WtpWirelessDB::load_wireless_pkg()
{ int ret;

    if(pUciCtx==NULL)
        return false;
    ret = uci_load(pUciCtx, "wireless", &pUciPkg);
	 cout << "WtpWirelessDB::load_wireless_pkg: status=" << ret << endl;     //for debugging:  charles
    return (ret==UCI_OK);
}

bool WtpWirelessDB::commit_wireless_pkg()
{ int ret;

    if(pUciCtx==NULL)
        return false;
    ret = uci_commit(pUciCtx, &pUciPkg, true);
	 cout << "WtpWirelessDB::commit_wireless_pkg: status=" << ret << endl;     //for debugging:  charles
    return (ret==UCI_OK);
}

//for assist session load update
bool WtpWirelessDB::load_device_session(WtpWiFiDeviceSession &apiDevSec, struct uci_section *pUciSec)
{   struct uci_element *e;
    struct uci_option  *pOpt;
	 bool ret = true;
    uci_foreach_element(&pUciSec->options, e)
    {   pOpt = uci_to_option(e);
		  cout << "WtpWirelessDB::load_device_session: option=" << pOpt->e.name << " = " << pOpt->v.string << endl;     //for debugging:  charles
        //ret &= apiDevSec.sessionUpdateFromUCI(pOpt->e.name, pOpt->v.string);		//charles*** note: this is surpose to be, but the return is  not working now.
        apiDevSec.sessionUpdateFromUCI(pOpt->e.name, pOpt->v.string);
    }
    if( apiDevSec.addUnknownParmPath()==false )
	 {
		ret = false; 
	 } 
    return ret;
}

bool WtpWirelessDB::load_iface_session(WtpWiFiIfaceSession &apiIfSec, struct uci_section *pUciSec)
{   struct uci_element *e;
    struct uci_option  *pOpt;
	  bool ret = true;
    uci_foreach_element(&pUciSec->options, e)
    {   pOpt = uci_to_option(e);
		  cout << "WtpWirelessDB::load_iface_session: option=" << pOpt->e.name << " = " << pOpt->v.string << endl;     //for debugging:  charles
        ret &= apiIfSec.sessionUpdateFromUCI(pOpt->e.name, pOpt->v.string);
    }
    if( apiIfSec.addUnknownParmPath()==false )
	 {
		ret = false; 
	 }
    return ret;
}

void WtpWirelessDB::wtp_reset_typelist(void)
{
	struct uci_type_list *type;
	while (type_list != NULL) {
			type = type_list;
			type_list = type_list->next;
			free(type);
	}	
}

void WtpWirelessDB::wtp_lookup_section_ref(struct uci_section *s, string &s_path)
{
	struct uci_type_list *ti = type_list;
	char sbuff[100];

   s_path = "wireless.";
	if ( !s->anonymous )
	{
	    s_path.append(s->e.name);
	    return;
	}

	/* look up in section type list */
	while (ti) {
		if (strcmp(ti->name, s->type) == 0)
			break;
		ti = ti->next;
	}
	if (!ti) {
		ti = reinterpret_cast<uci_type_list *> (malloc(sizeof(struct uci_type_list)));
		if (!ti)
			return;
		
		memset(ti, 0, sizeof(struct uci_type_list));
		ti->next = type_list;
		type_list = ti;
		ti->name = s->type;
	}

    sprintf(sbuff, "@%s[%d]", ti->name, ti->idx);
    s_path.append(sbuff);
	 ti->idx++;
}


//for option parameter set and get, should provide the complete path of the option parameter
bool WtpWirelessDB::wtp_lookup_ptr(struct uci_ptr *ptr, const string &sPath)        //uci_lookup_ptr()
{   if(pUciCtx != NULL)
    {	  string lookupStr = sPath;
        return ( uci_lookup_ptr(pUciCtx, ptr, (char *) lookupStr.c_str(), true)==UCI_OK);
    }
    else
    {
        return false;
    }
}

//this is for option with the complete fields setups (p, s, o):
bool WtpWirelessDB::wtp_ptr_set(struct uci_ptr *ptr, const char *pVal)      //uci_set()
{
    if( pUciCtx != NULL)
    {   ptr->value = pVal;  
        return (uci_set(pUciCtx, ptr)==UCI_OK);
    }
    else
    {
        return false;
    }
}





