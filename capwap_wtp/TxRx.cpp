
/*#include "socket.h"
#include <unistd.h>
#include <string.h>

int initSocket(int x, int port,struct sockaddr_in *serverAddr){
  //char buffer[1024];
  //struct sockaddr_in serverAddr;
  socklen_t addr_size;
  int yes = 1;
  //Create UDP socket
  x = socket(PF_INET, SOCK_DGRAM, 0);
  if(x<0)
	  printf("error initializing socket");

  //Configure settings in address struct
  serverAddr->sin_family = AF_INET;
  serverAddr->sin_port = htons(7891);
  serverAddr->sin_addr.s_addr = htons(INADDR_ANY);
  memset(serverAddr->sin_zero , '\0', sizeof serverAddr->sin_zero);

  //Initialize size variable to be used later on
  addr_size = sizeof serverAddr;

  if(bind(x,(struct sockaddr *)&serverAddr, addr_size)<0)
  {
	  closeSocket(x);
	  printf("failed to bind client socket\n");
  }

setsockopt(x, SOL_SOCKET, SO_BROADCAST, &yes, sizeof(yes));

 return 1;
}

bool CWNetworkSendUnsafeUnconnected(int sock,struct sockaddr_in *serverAddr, const char *buf, int len)
{
	if(!(sendto(sock,buf,len,0,(struct sockaddr *)&serverAddr,sizeof serverAddr)<0))
		return true;
	else
		return false;
}

 void closeSocket(int x)
 {
	shutdown(SHUT_RDWR, x);
	close(x);
 }

  while(1){
    printf("Type a sentence to send to server:\n");
    fgets(buffer,1024,stdin);
    printf("You typed: %s",buffer);

    nBytes = strlen(buffer) + 1;

    /*Send message to server*/
   // sendto(x,buffer,nBytes,0,(struct sockaddr *)&serverAddr,addr_size);

    /*Receive message from server*/
               /* nBytes = recvfrom(x,buffer,1024,0,NULL, NULL);

    printf("Received from server: %s\n",buffer);

  }*/

/* UDP client in the internet domain */
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

bool CWNetworkInitSocketClientWithPort(int *sock,int portNum)
{
	int n,status;
	unsigned int length;
	struct sockaddr_in src;
	length=sizeof(struct sockaddr_in);
	memset(&src, 0, sizeof(src));
	struct timeval timeout;
	timeout.tv_sec = 10;
	timeout.tv_usec = 0;

	printf("********************CWNetworkInitSocketClientWithPort Called*******************************\n");
	*sock= socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	 if (*sock < 0)
	 {
		close(*sock);
		printf("socket initialization failed\n");
		return 0;
	   }

	 src.sin_family = AF_INET;
	 src.sin_port = htons(12225);
	 src.sin_addr.s_addr = inet_addr("192.168.1.1");
	 //src.sin_addr.s_addr = INADDR_ANY;


	 status = bind(*sock, (struct sockaddr *) &src,(socklen_t) length);
	 if(status<0)
	 {
		 printf("binding failed %s\n", strerror(errno));
		 return 0;
	 }

	 int yes = 1;
	 status = setsockopt(*sock, SOL_SOCKET, SO_BROADCAST, &yes, sizeof(yes));
	 if(status<0)
	 {
		 printf("can not set broadcast option\n");
		 return 0;
	 }

	 status = setsockopt(*sock, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout,sizeof(timeout));
	 if(status<0)
	 {
		 printf("can not set timeout option\n");
		 return 0;
	 }

	return true;
}

bool sendReceiveDiscovery(int *sock,int portNum,char *msg,int len,char *buf,int len2,int *readBytesPtr)
{
   int n,status;
   unsigned int length;
   struct sockaddr_in dest,from;
   length=sizeof(struct sockaddr_in);
   memset(&dest, 0, sizeof(dest));

   dest.sin_family = AF_INET;
   dest.sin_port = htons(portNum);
   dest.sin_addr.s_addr = INADDR_BROADCAST;

   n=sendto(*sock,msg,len,0,(const struct sockaddr *)&dest,length);
   if (n < 0)
   {
	   printf("Error sending\n");
	   return 0;
   }

   if(buf == NULL || readBytesPtr == NULL)
   {
	   printf("empty argument\n");
	   return 0;
   }
   while((*readBytesPtr = recvfrom(*sock,buf,len2,0,(struct sockaddr *)&from, &length))<0)
   {
	  printf("Timeout Reached.. Retransmission begin\n");
	  close(*sock);
	  return 0;
   }
   printf("Reception complete\n");
   close(*sock);
   return 1;
}

void closeSocket(int *x)
{
	close(*x);
	*x = 0;
}

/*bool receiveMessage(int sock, void *buf, size_t len,struct sockaddr *src_addr, socklen_t *addrlen)
{
	if(recvfrom(sock,buf,len,0,src_addr,addrlen)<0)
	{
		close(sock);
	}
	return true;
}*/

//if(!(sendReceiveJoin(&socket_c,CW_CONTROL_PORT,&ACInfoPtr->IPv4Addresses[0].addr,msgPtr->msg,msgPtr->offset,buf,CW_BUFFER_SIZE-1, &readBytes)))

bool sendReceiveJoin(int *sock,int portNum,struct sockaddr_in *addr,char *msg,int len,char *buf,int len2,int *readBytesPtr)
{
   int n;
   unsigned int length;
   struct sockaddr_in from;
   length=sizeof(struct sockaddr_in);

   n=sendto(*sock,msg,len,0,(const struct sockaddr*)addr,length);
   if (n < 0)
   {
	   printf("error sending %s\n", strerror(errno));
	   return 0;
   }

   if(buf == NULL || readBytesPtr == NULL)
   {
	   printf("empty argument\n");
	   return 0;
   }
   while((*readBytesPtr = recvfrom(*sock,buf,len2,0,(struct sockaddr *)&from, &length))<0)
   {
	  printf("Timeout Reached.. Going back to discovery state\n");
	  close(*sock);
	  return 0;
   }
	   printf("Reception complete\n");
   close(*sock);
   return 1;
}

bool receiveRequest(int *sock,char *buf,int len2,int *readBytesPtr)
{
	unsigned int length;
	struct sockaddr_in from;
	length=sizeof(struct sockaddr_in);
	memset(&from, 0, sizeof(from));

	if(buf == NULL || readBytesPtr == NULL)
	{
		printf("empty argument\n");
		return 0;
	}

	while((*readBytesPtr = recvfrom(*sock,buf,len2,0,(struct sockaddr *)&from, &length))<0)
	{
		printf("Timeout Reached.. Going back to discovery state %s\n",strerror(errno));
		close(*sock);
		return 0;
	 }
	printf("Reception complete\n");
	close(*sock);
	return 1;
}

bool sendRequest(int *sock,int portNum,struct sockaddr_in *addr,char *msg,int len)
{
	int n,status;
	unsigned int length;
	struct sockaddr_in src;
	length=sizeof(struct sockaddr_in);
	memset(&src, 0, sizeof(src));
	struct timeval timeout;
	timeout.tv_sec = 10;
	timeout.tv_usec = 0;

	*sock= socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	 if (*sock < 0)
	 {
		close(*sock);
		printf("socket initialization failed\n");
		return 0;
	   }

	 src.sin_family = AF_INET;
	 src.sin_port = htons(12225);
	 src.sin_addr.s_addr = inet_addr("10.178.2.146");
	 //src.sin_addr.s_addr = INADDR_ANY;


	 status = bind(*sock, (struct sockaddr *) &src,(socklen_t) length);
	 if(status<0)
	 {
		 printf("binding failed %s\n", strerror(errno));
		 return 0;
	 }

	 int yes = 1;
	 status = setsockopt(*sock, SOL_SOCKET, SO_BROADCAST, &yes, sizeof(yes));
	 if(status<0)
	 {
		 printf("can not set broadcast option\n");
		 return 0;
	 }

	 status = setsockopt(*sock, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout,sizeof(timeout));

	 n=sendto(*sock,msg,len,0,(const struct sockaddr*)addr,length);
	 if (n < 0)
	 {
	 	 printf("error sending %s\n", strerror(errno));
	 	 return 0;
	  }

	close(*sock);
	return true;
}


















































