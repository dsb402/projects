#include <iostream>
#include "wtp_internal.hh"

class WtpWirelessDB;
WtpWirelessDB *gWtpAccess=nullptr;

WtpWiFiConfig *WtpWiFiConfig::pWtpWiFiConfig = nullptr;


bool WtpSparam::SetVal(string val)	//implement uci_set for the option: wireless.wifiIface[].ssid='openwrt101'
{ bool ret = false;

	cout << "enter WtpSparam::SetVal():  m_Path=" << m_Path << " val=" << val << endl;     //for debugging:  charles		
	if(RangeCheck(val)!=true)
		return false;
	
	//m_Path is vary by parameter object
	if( (!val.empty()) && (gWtpAccess != nullptr ) && (!m_Path.empty()) )
	{struct uci_ptr Opt;
		Opt.option = nullptr;
		Opt.o = nullptr;
		ret = gWtpAccess->wtp_lookup_ptr(&Opt, m_Path); 
		cout << "WtpSparam::SetVal():  m_Path=" << m_Path << " lookup status=" << ret << " option.o" << Opt.o << endl;     //for debugging:  charles		
		//if( (ret==true) && (Opt.o !=nullptr) )  //comment out because if the parameter is not exist, allow to add it.
		//{
				ret = gWtpAccess->wtp_ptr_set(&Opt, val.c_str());
				cout << "WtpSparam::SetVal():  m_Path=" << m_Path << " option.o->e.name=" << Opt.o->e.name << " Val=" << val << " Set status=" << ret << endl;     //for debugging:  charles
		//}
	}
	return ret;
}

string WtpSparam::GetVal()		//implement uci_lookup_ptr for the option: wireless.wifiIface[].ssid
{
	//m_Path is vary by parameter object
	if((gWtpAccess != nullptr ) && (!m_Path.empty()) )
	{struct uci_ptr Opt;
		Opt.option = nullptr;
		Opt.o = nullptr;
		if( (gWtpAccess->wtp_lookup_ptr(&Opt, m_Path)==true) && (Opt.o !=nullptr) && (Opt.o->v.string !=nullptr) )
		{
			string vStr(Opt.o->v.string);
			cout << "WtpSparam::GetVal():  m_Path=" << m_Path << " = " << vStr << endl;     //for debugging:  charles
			return vStr;
		}
	}
	cout << "WtpSparam::GetVal(): failed" << endl;     //for debugging:  charles
	return nullptr;
}


//=====WiFi-Device Options=====
//***radio: hwmode Parameter
bool WtpRadioHwModeParam::SetVal(int val)	//implement uci_set for the option: wireless.radio[].channel='mac80211'
{
	return false;
}
int WtpRadioHwModeParam::GetVal()		//implement uci_lookup_ptr for the option: wireless.radio[].channel
{
	return -1;
}



//***radio: htmode Parameter
bool WtpRadioHtModeParam::SetVal(int val)	//implement uci_set for the option: wireless.radio[].channel='mac80211'
{
	return false;
}

int WtpRadioHtModeParam::GetVal()		//implement uci_lookup_ptr for the option: wireless.radio[].channel
{
	return -1;
}


//***radio: Channel Parameter
bool WtpRadioChannelParam::SetVal(int val)	//implement uci_set for the option: wireless.radio[].channel='46'
{ bool ret = false;
	//int2Str() is vary by parameter derive class
	string chanStr = int2Str(val);
	cout << "WtpRadioChannelParam::SetVal():  int2Str=" << chanStr << " Initial Status=" << ret << endl;     //for debugging:  charles	
	//m_Path is vary by parameter object
	if( (!chanStr.empty()) && (gWtpAccess != nullptr ) && (!m_Path.empty()) )
	{struct uci_ptr Opt;
		Opt.option = nullptr;
		Opt.o = nullptr;
		cout << "WtpRadioChannelParam::m_Path before wtp_lookup_ptr =" << m_Path << endl;
		ret = gWtpAccess->wtp_lookup_ptr(&Opt, m_Path); 
		cout << "WtpRadioChannelParam::SetVal():  m_Path=" << m_Path << " lookup status=" << ret << " option.o" << hex << Opt.o << endl;     //for debugging:  charles		
		//if( (ret==true) && (Opt.o !=nullptr) )
		//{		
				ret = gWtpAccess->wtp_ptr_set(&Opt, chanStr.c_str());
				cout << "WtpRadioChannelParam::SetVal():  m_Path=" << m_Path << " option.o->e.name=" << Opt.o->e.name << " Val=" << chanStr;     //for debugging:  charles
		//}
	}
	cout << "  WtpRadioChannelParam::SetVal(): status=" << ret << endl;     //for debugging:  charles
	return ret;
}

int WtpRadioChannelParam::GetVal()		//implement uci_lookup_ptr for the option: wireless.radio[].channel
{int ret = -1;
	//m_Path is vary by parameter object
	if((gWtpAccess != nullptr ) && (!m_Path.empty()) )
	{struct uci_ptr Opt;
		Opt.option = nullptr;
		Opt.o = nullptr;
		if( (gWtpAccess->wtp_lookup_ptr(&Opt, m_Path)==true) && (Opt.o !=nullptr) && (Opt.o->v.string !=nullptr) )
		{
			cout << "WtpRadioChannelParam::GetVal():  m_Path=" << m_Path << " lookup status=" << ret << ", option.o=" << hex << Opt.o << endl;     //for debugging:  charles	
			string vStr(Opt.o->v.string);
			//str2Int() is vary by parameter derive class
			ret = str2Int(vStr);
			cout << "WtpRadioChannelParam::GetVal():  m_Path=" << m_Path << " option.o->e.name=" << Opt.o->e.name << " Val=" << Opt.o->v.string << ",  iVal=" << dec << ret << endl;     //for debugging:  charles
		}
	}
	cout << "  WtpRadioChannelParam::GetVal(): status=" << ret << endl;     //for debugging:  charles
	return ret;
}


//***radio: txpower Parameter
bool WtpRadioTxPowerParam::SetVal(int val)	//implement uci_set for the option: wireless.radio[].txpower='2'
{ bool ret = false;
	//int2Str() is vary by parameter derive class
	string pwrStr = int2Str(val);
	cout << "WtpRadioTxPowerParam::SetVal():  int2Str=" << pwrStr << endl;     //for debugging:  charles	
	//m_Path is vary by parameter object
	cout << "WtpRadioTxPowerParam::SetVal():  m_Path=" << m_Path << endl;
	if( (!pwrStr.empty()) && (gWtpAccess != nullptr ) && (!m_Path.empty()) )
	{struct uci_ptr Opt;
		Opt.option = nullptr;
		Opt.o = nullptr;
		ret = gWtpAccess->wtp_lookup_ptr(&Opt, m_Path);
		cout << "WtpRadioTxPowerParam::SetVal():  m_Path=" << m_Path << " lookup status=" << ret << " option.o=" << hex << Opt.o << endl;     //for debugging:  charles	
		//if( (ret==true) && (Opt.o !=nullptr) ) *********comment-out because if the parameter is not exist, then allow to add it.
		//{
				ret = gWtpAccess->wtp_ptr_set(&Opt, pwrStr.c_str());
				cout << "WtpRadioTxPowerParam::SetVal():  m_Path=" << m_Path << " option.o->e.name=" << Opt.o->e.name << " Val=" << pwrStr << endl;     //for debugging:  charles
		//}
	}
	cout << "  WtpRadioTxPowerParam::SetVal(): status=" << ret << endl;     //for debugging:  charles
	return ret;
}

int WtpRadioTxPowerParam::GetVal()		//implement uci_lookup_ptr for the option: wireless.radio[].txpower
{int ret = -1;
	//m_Path is vary by parameter object
	cout << "WtpRadioTxPowerParam::GetVal():  m_Path=" << m_Path << endl;
	if((gWtpAccess != nullptr ) && (!m_Path.empty()) )
	{struct uci_ptr Opt;
		Opt.option = nullptr;
		Opt.o = nullptr;		
		if( (gWtpAccess->wtp_lookup_ptr(&Opt, m_Path)==true) && (Opt.o !=nullptr) && (Opt.o->v.string !=nullptr) )
		{
			cout << "WtpRadioTxPowerParam::GetVal():  m_Path=" << m_Path << " lookup status=" << ret << " option.o=" << hex << Opt.o << endl;     //for debugging:  charles	
			string vStr(Opt.o->v.string);
			//str2Int() is vary by parameter derive class
			ret = str2Int(vStr);
			cout << "WtpRadioTxPowerParam::GetVal():  m_Path=" << m_Path << " option.o->e.name=" << Opt.o->e.name << " Val=" << Opt.o->v.string << ",  iVal=" << dec << ret;     //for debugging:  charles
		}
	}
	cout << "  WtpRadioTxPowerParam::GetVal(): status=" << ret << endl;     //for debugging:  charles
	return ret;
}


//=====WiFi-Iface Options=====
//***WiFi-Iface: mode Parameter
bool WtpWiFiIfaceModeParam::SetVal(int val)	//implement uci_set for the option: wireless.radio[].channel='mac80211'
{
	return false;
}

int WtpWiFiIfaceModeParam::GetVal()		//implement uci_lookup_ptr for the option: wireless.radio[].channel
{
	return -1;
}


//***WiFi-Iface: Encryption Parameter
bool WtpWiFiIfaceEncryptionParam::SetVal(int val)	//implement uci_set for the option: wireless.radio[].channel='mac80211'
{
	return false;
}

int WtpWiFiIfaceEncryptionParam::GetVal()		//implement uci_lookup_ptr for the option: wireless.radio[].channel
{
	return -1;
}



//***WiFi-Iface: MacFilter Parameter
bool WtpWiFiIfaceMacFilterParam::SetVal(int val)	//implement uci_set for the option: wireless.radio[].channel='mac80211'
{
	return false;
}

int WtpWiFiIfaceMacFilterParam::GetVal()		//implement uci_lookup_ptr for the option: wireless.radio[].channel
{
	return -1;
}



bool SetSessionToParmUpdateFromUCI(WtpParam *pParm , const string &parmVal)
{   bool found_type = true;
		 
    if(pParm->getValType()==STR_VAL)
    {
        WtpSparam *psParm = reinterpret_cast <WtpSparam *> (pParm);
        psParm->SetVal(parmVal);
        psParm->SetState(ACK);
    }
    else if(pParm->getValType()==BOOL_VAL)
    {   WtpBparam *pbParm = reinterpret_cast <WtpBparam *> (pParm);
		  string bparm = parmVal;
        if( (bparm=="disable") || (bparm=="DISABLE") )
        {
            pbParm->SetVal(true);
        }
        else
        {
             pbParm->SetVal(false);
        }
        pbParm->SetState(ACK);
    }
    else if(pParm->getValType()==ENUM_VAL)
    {   WtpVparam *pvParm = reinterpret_cast <WtpVparam *> (pParm);
        int enumVal = pvParm->str2Int(parmVal);
        if( enumVal >= 0 )
        {
            pvParm->SetVal(enumVal);
            pvParm->SetState(ACK);
        }
        else
        {   cout << "SetSessionToParmUpdateFromUCI(Value-Enum):  option=" << pParm->m_Path << " = "<< parmVal << " failed" << endl;     //for debugging:  charles
            //found_type = false;  //charles important note: *****some enum for parameter device::htmode, iface:: encryption still have problem in mapping, so temporary set to true for all case now
        }
    }
    else
    {
        found_type = false;
    }
    cout << "option=" << pParm->m_Path << " = "<< parmVal << " found status=" << found_type << endl;     //for debugging:  charles
    return found_type;
}


//***WiFi-Device session
WtpWiFiDeviceSession::WtpWiFiDeviceSession()
{
    m_State  = NO_ACT;
	 m_Path.clear();
	 //WtpParam *pParam = new WtpRadioTypeParam("mac80211");
	 cout << "enter WtpWiFiDeviceSession constructor" << endl;     //for debugging:  charles
	 //m_DevMap.insert(make_pair(rdtype.m_Id, reinterpret_cast<WtpParam *> (&rdtype)));
    m_DevMap.insert(make_pair(rdtype.m_Id, &rdtype));
    m_DevMap.insert(make_pair(hwmode.m_Id, &hwmode));
    m_DevMap.insert(make_pair(htmode.m_Id, &htmode));
    m_DevMap.insert(make_pair(channel.m_Id, &channel));
    m_DevMap.insert(make_pair(txpower.m_Id, &txpower));
	 cout << "enter WtpWiFiDeviceSession constructor:  # of entry in Map=" << m_DevMap.size() << endl;     //for debugging:  charles
}

//in order the change any parameter, the session must updated at least only 
//once so that configure session syn-up with UCI.
//get method from each parameter will not retireve from UCI, it returns based 
//on what is in this configure session context.
bool WtpWiFiDeviceSession::sessionUpdateFromUCI(const string &parmName, const string &parmVal)
//based on the session update state from UCI,  setting state=ACK if the update 
//is successful
//otherwise the session is not usable.
{   struct uci_ptr ptr;
    cout << "\nWtpWiFiDeviceSession::sessionUpdateFromUCI parmName=" << parmName << endl;     //for debugging:  charles
 
    WtpParam *pParm = NULL;
    try
    {
        pParm = m_DevMap.at(parmName);
    }
    catch(...)
    {
        pParm=NULL;
    }
    if(pParm!=NULL)
    {		  
        pParm->m_Path = m_Path + "." + parmName;
		  cout << "sessionUpdateFromUCI:" << "Parm Path=" << pParm->m_Path << " Parm Name" << parmName << " = "<< parmVal << endl;     //for debugging:  charles
        return SetSessionToParmUpdateFromUCI(pParm, parmVal);
    }
    else
        return false;
}

bool WtpWiFiDeviceSession::addUnknownParmPath()
{ bool ret=false;
	map <string, WtpParam *>::iterator it;
	
	if(!m_Path.empty() )
	{	ret = true;
		for (it=m_DevMap.begin(); it!=m_DevMap.end(); ++it)
		{
			WtpParam *ptr = it->second;
			if(ptr->m_Path.empty())
			{
				ptr->m_Path = m_Path + "." +  ptr->m_Id;
			}
		}
	}
	return ret;
}


//***WiFi-Iface session
WtpWiFiIfaceSession::WtpWiFiIfaceSession()
{
    m_State  = NO_ACT;
	 m_Path.clear();
	 cout << "enter WtpWiFiIfaceSession constructor" << endl;     //for debugging:  charles
    m_IfMap.insert(make_pair(device.m_Id, &device));
    m_IfMap.insert(make_pair(mode.m_Id, &mode));
    m_IfMap.insert(make_pair(network.m_Id, &network));
    m_IfMap.insert(make_pair(ssid.m_Id, &ssid));
    m_IfMap.insert(make_pair(mac.m_Id, &mac));
    m_IfMap.insert(make_pair(encryption.m_Id, &encryption));
    m_IfMap.insert(make_pair(key.m_Id, &key));
    m_IfMap.insert(make_pair(key1.m_Id, &key1));
    m_IfMap.insert(make_pair(key2.m_Id, &key2));
    m_IfMap.insert(make_pair(key3.m_Id, &key3));
    m_IfMap.insert(make_pair(key4.m_Id, &key4));
    m_IfMap.insert(make_pair(macfilter.m_Id, &macfilter));
    m_IfMap.insert(make_pair(maclist.m_Id, &maclist));
	 cout << "enter WtpWiFiIfaceSession constructor:  # of entry in Map=" << m_IfMap.size() << endl;     //for debugging:  charles
}
//in order the change any parameter, the session must updated at least only 
//once so that configure session syn-up with UCI.
//get method from each parameter will not retireve from UCI, it returns based 
//on what is in this configure session context.
bool WtpWiFiIfaceSession::sessionUpdateFromUCI(const string &parmName, const string &parmVal)
//based on the session update state from UCI,  setting state=ACK if the update 
//is successful
//otherwise the session is not usable.
{
   WtpParam *pParm = NULL;
	 cout << "\nWtpWiFiIfaceSession::sessionUpdateFromUCI parmName=" << parmName << endl;     //for debugging:  charles
    try
    {
        pParm = m_IfMap.at(parmName);
    }
    catch(...)
    {
        pParm=NULL;
    }
    if(pParm!=NULL)
    {
         pParm->m_Path = m_Path + "." + parmName;
			return SetSessionToParmUpdateFromUCI(pParm, parmVal);
    }

    else
	 {
		   cout << "WtpWiFiIfaceSession::sessionUpdateFromUCI " << parmName << " not found . . ." << endl;     //for debugging:  charles
		  //charles: importance note:  ****temporary force return true
		  return true;
        //return false;
	 }
}


bool WtpWiFiIfaceSession::addUnknownParmPath()
{ bool ret=false;
	map <string, WtpParam *>::iterator it;
	
	cout << "WtpWiFiIfaceSession::addUnknownParmPath Session-Path=" << m_Path << endl;     //for debugging:  charles
	if(!m_Path.empty() )
	{	ret = true;
		for (it=m_IfMap.begin(); it!=m_IfMap.end(); ++it)
		{
			WtpParam *ptr = it->second;
			if(ptr->m_Path.empty())
			{
				ptr->m_Path = m_Path + "." +  ptr->m_Id;
				cout << "WtpWiFiIfaceSession::addUnknownParmPath Parm-Path=" << ptr->m_Path << endl;     //for debugging:  charles
			}
		}
	}
	return ret;
}


//***WiFi-Iface session
//it cause all the sessions under the configuration update from the UCI 
//configuration file for the Wireless package
//this is typicall call the when the system start-up for the wireless feature.
bool WtpWiFiConfig::ConfigLoad()
{   m_If_Cnt    = 0;
    m_Dev_Cnt   = 0;
	 bool is_ok = true;
	 
	 if(gWtpAccess==nullptr)
	 {
		 gWtpAccess =  WtpWirelessDB::getInstance();
	 }
	 
    if(gWtpAccess!=nullptr)
    {   cout << "WtpWiFiConfig::ConfigLoad: load_wireless_pkg" << endl;     //for debugging:  charles
		  if(!gWtpAccess->load_wireless_pkg())
        {
            return false;
        }
        else if(gWtpAccess->pUciPkg==nullptr)
        {
            return false;
        }
        cout << "WtpWiFiConfig::ConfigLoad: load_wireless_pkg is_ok=" << is_ok << endl;     //for debugging:  charles
        struct uci_element *e;
        struct uci_section *s;
        gWtpAccess->wtp_reset_typelist();
        uci_foreach_element( &gWtpAccess->pUciPkg->sections, e)
        {
            s = uci_to_section(e);
            if(m_Dev_Cnt == MaxNumOfDev)
            {
                //perror("number of wifi-device exceed the the max limit = %d\n", MaxNumOfDev);
					 perror("number of wifi-device exceed the the max limit\n");
                continue;
            }
            //else if (strcmp(pWirelessPkg->DevTypeName.c_str(), s->type) == 0)
				else if (::strcmp(gWtpAccess->DevTypeName.c_str(), s->type) == 0)
            {
                gWtpAccess->wtp_lookup_section_ref(s, radio[m_Dev_Cnt].m_Path);
					 cout << "WtpWiFiConfig::ConfigLoad: radio[" << m_Dev_Cnt << "].m_Path=" << radio[m_Dev_Cnt].m_Path << endl;     //for debugging:  charles
                is_ok &= gWtpAccess->load_device_session(radio[m_Dev_Cnt], s);
					 cout << "WtpWiFiConfig::ConfigLoad(1): trace is_ok=" << is_ok << endl;
                m_Dev_Cnt++;
            }
            else if(m_If_Cnt == MaxNumOfIf)
            {
                //perror("number of wifi-iface exceed the the max limit = %d \n", MaxNumOfIf);
					 perror("number of wifi-iface exceed the the max limit");
                continue;
            }
            else if (strcmp(gWtpAccess->DevIfName.c_str(), s->type) == 0)
            {
                gWtpAccess->wtp_lookup_section_ref(s, wifiIface[m_If_Cnt].m_Path);
					 cout << "WtpWiFiConfig::ConfigLoad: wifiIface[" << m_If_Cnt << "].m_Path=" << wifiIface[m_If_Cnt].m_Path << endl;     //for debugging:  charles
                is_ok &= gWtpAccess->load_iface_session(wifiIface[m_If_Cnt], s);
					 cout << "WtpWiFiConfig::ConfigLoad(2): trace is_ok=" << is_ok << endl;
                m_If_Cnt++;
            }
        }
        cout << "WtpWiFiConfig::ConfigLoad(3): trace is_ok=" << is_ok << endl;
        if( (m_If_Cnt > 0) && (m_Dev_Cnt>0) && (is_ok==true) )
		  {
			  m_State = ACK;
		  }
		  else
		  {
			  is_ok==false;
		  }
		  cout << "WtpWiFiConfig::ConfigLoad(4): trace is_ok=" << is_ok << endl;
		  cout << "exit WtpWiFiConfig::ConfigLoad: m_Dev_Cnt=" << m_Dev_Cnt << ",  m_If_Cnt=" << m_If_Cnt << ", is_ok=" << is_ok << endl;     //for debugging:  charles
        gWtpAccess->wtp_reset_typelist();
    }
    return is_ok;
}

//it cause all the configurations under package save into the wireless 
//configuration file (all the sessions within the package).
//this is typically called when the wireless configuration is proven workable.
bool WtpWiFiConfig::ConfigCommit()
{
	gWtpAccess =  WtpWirelessDB::getInstance();
}

void WtpWiFiConfig::WtpConfigDestroy()
{
	cout << "enter WtpWiFiConfig::WtpConfigDestroy " << endl;     //for debugging:  charles
	if(::gWtpAccess!=nullptr)
	{
		delete ::gWtpAccess;
	}		
	if(pWtpWiFiConfig!=nullptr)
	{
		delete pWtpWiFiConfig;
		pWtpWiFiConfig = nullptr;
	}
	cout << "enter WtpWiFiConfig::WtpConfigDestroy " << endl;     //for debugging:  charles
	gWtpAccess 	   = nullptr;
	pWtpWiFiConfig = nullptr;
}























