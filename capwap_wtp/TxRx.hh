#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>


extern bool CWNetworkInitSocketClientWithPort(int *sockPtr, int portSocket);
extern void closeSocket(int *x);
extern bool sendReceiveDiscovery(int *sock,int portNum, char *msg,int len,char *buf,int len2,int *readBytesPtr);
extern bool sendReceiveJoin(int *sock,int portNum,struct sockaddr_in *addr,char *msg,int len,char *buf,int len2,int *readBytesPtr);
extern bool receiveRequest(int *sock,char *buf,int len2,int *readBytesPtr);
extern bool sendResponse(int *sock,struct sockaddr_in *addr,char *msg,int len);
extern bool sendRequest(int *sock,int portNum,struct sockaddr_in *addr,char *msg,int len);
