#include <linux/types.h>
#include <stdint.h>
#include <iomanip>

#ifndef __WTP_SIM__
#include "wtp_cfg_api.hh"

#else
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <errno.h>
#include <unistd.h>
#include <string>
#include <map>
#include <bitset>
#include <initializer_list>
#endif


#define	CW_IE_VENDOR_SPECIFIC_TYPE		37
#define	CW_IE_VENDOR_SPECIFIC_HRD_LEN	8
#define	CW_IE_VENDOR_SPECIFIC_ID_LEN	4
#define	CW_IE_VENDOR_SPECIFIC_ID   	0xCAE0888A

/***2*****2*********4***************************************************************
 * IE * iLen * vendor-id *  c-IE  *  c-IE ...
*********************************************************************************
* c-IE  => chameloen Vendor IE and its format as below
* ***1***********1*********1*********1*******n**********
*   m_cIE * m_IfIndex * m_Status * m_Len * Data value
* *******************************************************/
#define	WTP_IE_DEFAULT_VLEN_LIMIT			1
#define	WTP_IE_SSID_VLEN_LIMIT				18
#define	WTP_IE_MAC_VLEN_LIMIT				18

#define	WTP_IE_CHANNEL_CID				0x01
#define	WTP_IE_TXPOWER_CID				0x02
#define	WTP_IE_SSID_CID					0x03
#define	WTP_IE_MAC_CID						0x04
#define	WTP_IE_DISABLE_CID				0x05
#define	WTP_IE_FIRST_CID					WTP_IE_CHANNEL_CID
#define	WTP_IE_LAST_CID					WTP_IE_DISABLE_CID
#define  WTP_CIE_MIN_LEN					5

//====Header file==========
class WTP_cIE_type
{ 
public:
//protected:	
	uint8_t m_cIE;			//byte[0]  in the IE message
	uint8_t m_IfIndex;	//byte[1]  in the IE message
	uint8_t m_Status;		//byte[2]  in the IE message,  default 0
	uint8_t m_Len;			//byte[3]  in the IE message

//public:
   WTP_cIE_type(uint8_t  msgid,  uint8_t  vLenMax): m_cIE(msgid), m_Len(vLenMax),  m_IfIndex(0),  m_Status(0) {  };
 
	//expect the derive class can deserialize the byte stream into derive class object values.  
	//The method will check last value byte of the byte stream for the IE by reading it.
	//if invalid memory access, it will return false. Or IE value len too big,  it will return false.
	virtual bool deserialize(uint8_t *pIEBuf)=0;
	
	//it will return the sizeof(*this) of the derive class, which it well how bytes need to allocate for serialize buffer,
	//or how many byte to advance from current IE's byte pointer to next IE's byte pointer.
	virtual int getPtrOffset4NextIE()=0;

	//serialize will fill-in IE-ID=byte[0], If-Index=byte[1], Value Len=byte[2] and follow by Value byte(s) from byte[3] and on
	//if interface index too big(beyond system can support,  it will return false. Or if the last value byte is not accessible
	//it will return false. 
	virtual bool serialize(uint8_t *pIEBuf)=0;

	//pVBuf is points from byte[4] of current IE for value area(which it will be set), and actual length of the data
	//actual-length will use when the CapWap sending the vendor Specific IE, which it goes to byte[2]=Value-Len field.
	//Set mothed encode or decoding from CapWap Vendor specific IE is different implement in WTP or AC.
   virtual bool  set() = 0;

	//pBuf is the points to byte[4] of the current IE. Expect the Get the mothed retrieve information from UCI network interface
	//and fill the data from pVbuf pointer.  Or for Get method may implement differently encoding or decoding from CapWap
	//in WTP or AC.
	virtual bool get() = 0;  

	void setProcessStatus(bool IsOK)  { m_Status 	= IsOK;  };
	void setIfIndex(uint8_t  ifidx)	{  m_IfIndex 	= ifidx; };	//this is use for encoding only.  It is optional, default from the capwap msg or 0
	virtual ~WTP_cIE_type() { };
};


class WTP_cIE_Channel: public WTP_cIE_type
{	uint8_t  m_chan;   // in byte[4] in the IE message
   public:
	WTP_cIE_Channel(uint8_t if_idx):WTP_cIE_type(WTP_IE_CHANNEL_CID, WTP_IE_DEFAULT_VLEN_LIMIT), m_chan(0)
	{  
		m_IfIndex = if_idx;
		cout << "WTP_cIE_Channel constructor: IF#=" << setw(2) << dec << static_cast <int> (m_IfIndex) << endl;  		
	};

	int getPtrOffset4NextIE();
	bool set();
	bool get();
	
	bool deserialize(uint8_t *pIEBufBegin)
	{
		 //WTP_cIE_Channel *pMsg = reinterpret_cast<WTP_cIE_Channel *> (pIEBufBegin);
		//check mMsg and  mIfIndex  and mLen from pMsg
		//*this = *pMsg;
		m_cIE 		= pIEBufBegin[0];
		m_IfIndex 	= pIEBufBegin[1];
		m_Status 	= pIEBufBegin[2];
		m_Len 		= pIEBufBegin[3];
		m_chan 		= pIEBufBegin[4];		
		cout << "WTP_cIE_Channel::deserialize: m_cIE=0x" << setfill('0') << setw(2) << hex <<  static_cast <int> (m_cIE);
		cout << ",  m_IfIndex=0x" << setfill('0') << setw(2) << hex <<  static_cast <int> (m_IfIndex);
		cout << ",  m_Status=0x" << setfill('0') << setw(2) << hex <<  static_cast <int> (m_Status);
		cout << ",  m_Len=0x" << setfill('0') << setw(2) << hex <<  static_cast <int> (m_Len);
		cout << ",  m_chan=0x" << setfill('0') << setw(2) << hex <<  static_cast <int> (m_chan) << endl;
	};
	
	bool serialize(uint8_t *pIEBufBegin)
	{
		//WTP_cIE_Channel *pMsg = reinterpret_cast<WTP_cIE_Channel *> (pIEBufBegin);
		//*pMsg = *this;
		pIEBufBegin[0] = m_cIE;
		pIEBufBegin[1] = m_IfIndex;
		pIEBufBegin[2] = m_Status;
		pIEBufBegin[3] = m_Len;
		pIEBufBegin[4] = m_chan;
		int sz = getPtrOffset4NextIE();
		cout << "WTP_cIE_Channel::serialize[" << sz <<"]:";
		for(int i=0; i<sz; i++)
		{
			cout << " 0x" << setfill('0') << setw(2) << hex << static_cast <int> (pIEBufBegin[i]);
		}
		cout << endl;
	};
};

class WTP_cIE_Txpower: public WTP_cIE_type
{	uint8_t m_txpwr;   // in byte[4] in the IE message
   public:
	WTP_cIE_Txpower(uint8_t if_idx):WTP_cIE_type(WTP_IE_TXPOWER_CID, WTP_IE_DEFAULT_VLEN_LIMIT), m_txpwr(0)
	{ 	
		m_IfIndex = if_idx;
		cout << "WTP_cIE_Txpower constructor: IF#=" << static_cast <int> (m_IfIndex) << endl;  		
	};

	int getPtrOffset4NextIE();	
	bool set();
	bool get();
	
	bool deserialize(uint8_t *pIEBufBegin)
	{
		 //WTP_cIE_Txpower *pMsg = reinterpret_cast<WTP_cIE_Txpower *> (pIEBufBegin);
		//check mMsg and  mIfIndex  and mLen from pMsg
		//*this = *pMsg;
		m_cIE 		= pIEBufBegin[0];
		m_IfIndex 	= pIEBufBegin[1];
		m_Status 	= pIEBufBegin[2];
		m_Len 		= pIEBufBegin[3];
		m_txpwr 		= pIEBufBegin[4];
		cout << "WTP_cIE_Txpower::deserialize: m_cIE=0x" << setfill('0') << setw(2) << hex << static_cast <int> (m_cIE);
		cout << ",  m_IfIndex=0x" << setfill('0') << setw(2) << hex <<  static_cast <int> (m_IfIndex);
		cout << ",  m_Status=0x" << setfill('0') << setw(2) << hex <<  static_cast <int> (m_Status);
		cout << ",  m_Len=0x" << setfill('0') << setw(2) << hex <<  static_cast <int> (m_Len);
		cout << ",  m_txpwr=0x" << setfill('0') << setw(2) << hex <<  static_cast <int> (m_txpwr) << endl;
	};
	
	bool serialize(uint8_t *pIEBufBegin)
	{
		//WTP_cIE_Txpower *pMsg = reinterpret_cast<WTP_cIE_Txpower *> (pIEBufBegin);
		//*pMsg = *this;
		int sz = getPtrOffset4NextIE();
		pIEBufBegin[0] = m_cIE;
		pIEBufBegin[1] = m_IfIndex;
		pIEBufBegin[2] = m_Status;
		pIEBufBegin[3] = m_Len;
		pIEBufBegin[4] = m_txpwr;
		cout << "WTP_cIE_Txpower::serialize[" << sz <<"]:";
		for(int i=0; i<sz; i++)
		{
			cout << " 0x" << setfill('0') << setw(2) << hex << static_cast <int> (pIEBufBegin[i]);
		}
		cout << endl;		
	};
};

class WTP_cIE_SSID: public WTP_cIE_type
{	
	uint8_t m_ssid[WTP_IE_SSID_VLEN_LIMIT];   // from byte[3] and 16  bytes value in the IE message
   public:
	WTP_cIE_SSID(uint8_t if_idx):WTP_cIE_type(WTP_IE_SSID_CID, WTP_IE_SSID_VLEN_LIMIT)
	{  m_IfIndex = if_idx;
		memset(m_ssid, 0, sizeof(m_ssid));  cout << "WTP_cIE_SSID constructor: IF#=" << static_cast <int> (m_IfIndex) << endl; 		
	};

	int getPtrOffset4NextIE();
	bool set();
	bool get();
	
	bool deserialize(uint8_t *pIEBufBegin)
	{
		// WTP_cIE_SSID *pMsg = reinterpret_cast<WTP_cIE_SSID *> (pIEBufBegin);
		//check mMsg and  mIfIndex  and mLen from pMsg
		//*this = *pMsg;
		m_cIE 		= pIEBufBegin[0];
		m_IfIndex 	= pIEBufBegin[1];
		m_Status 	= pIEBufBegin[2];
		m_Len 		= pIEBufBegin[3];
		memcpy(m_ssid, &(pIEBufBegin[4]), WTP_IE_MAC_VLEN_LIMIT);		
		cout << "WTP_cIE_SSID::deserialize: m_cIE=0x" << setfill('0') << setw(2) << hex <<  static_cast <int> (m_cIE);
		cout << ",  m_IfIndex=0x" << setfill('0') << setw(2) << hex <<  static_cast <int> (m_IfIndex);
		cout << ",  m_Status=0x" << setfill('0') << setw(2) << hex <<  static_cast <int> (m_Status);
		cout << ",  m_Len=0x" << setfill('0') << setw(2) << hex <<  static_cast <int> (m_Len);
		cout << ",  m_ssid=";
		for(int i=0; i<WTP_IE_MAC_VLEN_LIMIT; i++)
			cout << " 0x" << setfill('0') << setw(2) << hex << static_cast <int> (m_ssid[i]);
		cout << endl;
	};
	
	bool serialize(uint8_t *pIEBufBegin)
	{
		//WTP_cIE_SSID *pMsg = reinterpret_cast<WTP_cIE_SSID *> (pIEBufBegin);
		//*pMsg = *this;
		pIEBufBegin[0] = m_cIE;
		pIEBufBegin[1] = m_IfIndex;
		pIEBufBegin[2] = m_Status;
		pIEBufBegin[3] = m_Len;
		memcpy( &(pIEBufBegin[4]), m_ssid, WTP_IE_SSID_VLEN_LIMIT);
		int sz = getPtrOffset4NextIE();
		cout << "WTP_cIE_SSID::serialize[" << sz <<"]:";
		for(int i=0; i<sz; i++)
		{
			cout << " 0x" << setfill('0') << setw(2) << hex << static_cast <int> (pIEBufBegin[i]);
		}
		cout << endl;
	};
};

class WTP_cIE_MAC: public WTP_cIE_type
{	uint8_t m_mac[WTP_IE_MAC_VLEN_LIMIT];   // from byte[3] and 16  bytes value in the IE message
   public:
	WTP_cIE_MAC(uint8_t if_idx):WTP_cIE_type(WTP_IE_MAC_CID, WTP_IE_MAC_VLEN_LIMIT)
	{  
		m_IfIndex = if_idx;
		memset(m_mac, 0, sizeof(m_mac)); cout << "WTP_cIE_SSID constructor: IF#=" << static_cast <int> (m_IfIndex) << endl; 		
	};

	int getPtrOffset4NextIE();
	bool set();
	bool get();
	
	bool deserialize(uint8_t *pIEBufBegin)
	{
		 //WTP_cIE_MAC *pMsg = reinterpret_cast<WTP_cIE_MAC *> (pIEBufBegin);
		//check mMsg and  mIfIndex  and mLen from pMsg
		//*this = *pMsg;
		m_cIE 		= pIEBufBegin[0];
		m_IfIndex 	= pIEBufBegin[1];
		m_Status 	= pIEBufBegin[2];
		m_Len 		= pIEBufBegin[3];
		memcpy(m_mac, &(pIEBufBegin[4]), WTP_IE_MAC_VLEN_LIMIT);
		cout << "WTP_cIE_MAC::deserialize: m_cIE=0x" << setfill('0') << setw(2) << hex <<  static_cast <int> (m_cIE);
		cout << ",  m_IfIndex=0x" << setfill('0') << setw(2) << hex <<  static_cast <int> (m_IfIndex);
		cout << ",  m_Status=0x" << setfill('0') << setw(2) << hex <<  static_cast <int> (m_Status);
		cout << ",  m_Len=0x" << setfill('0') << setw(2) << hex <<  static_cast <int> (m_Len);
		cout << ",  m_mac=";
		for(int i=0; i<WTP_IE_MAC_VLEN_LIMIT; i++)
			cout << " 0x" << setfill('0') << setw(2) << hex << static_cast <int> (m_mac[i]);
		cout << endl;
	};
	
	bool serialize(uint8_t *pIEBufBegin)
	{
		//WTP_cIE_MAC *pMsg = reinterpret_cast<WTP_cIE_MAC *> (pIEBufBegin);
		//*pMsg = *this;
		pIEBufBegin[0] = m_cIE;
		pIEBufBegin[1] = m_IfIndex;
		pIEBufBegin[2] = m_Status;
		pIEBufBegin[3] = m_Len;
		memcpy( &(pIEBufBegin[4]), m_mac, WTP_IE_MAC_VLEN_LIMIT);
		int sz = getPtrOffset4NextIE();
		cout << "WTP_cIE_MAC::serialize[" << sz <<"]:";
		for(int i=0; i<sz; i++)
		{
			cout << " 0x" << setfill('0') << setw(2) << hex << static_cast <int> (pIEBufBegin[i]);
		}
		cout << endl;		
	};
};


class WTP_cIE_Radio_Disable: public WTP_cIE_type
{	uint8_t  m_disable;   // in byte[4] in the IE message
   public:
	WTP_cIE_Radio_Disable(uint8_t if_idx):WTP_cIE_type(WTP_IE_DISABLE_CID, WTP_IE_DEFAULT_VLEN_LIMIT), m_disable(0)
	{ 
		m_IfIndex = if_idx;
		cout << "WTP_cIE_Radio_Disable constructor: IF#=" << static_cast <int> (m_IfIndex) << endl; 		
	};
	
	int getPtrOffset4NextIE();	
	bool set();
	bool get();
	
	bool deserialize(uint8_t *pIEBufBegin)
	{
		 WTP_cIE_Radio_Disable *pMsg = reinterpret_cast<WTP_cIE_Radio_Disable *> (pIEBufBegin);
		//check mMsg and  mIfIndex  and mLen from pMsg
		*this = *pMsg;
	};
	
	bool serialize(uint8_t *pIEBufBegin)
	{
		WTP_cIE_Radio_Disable *pMsg = reinterpret_cast<WTP_cIE_Radio_Disable *> (pIEBufBegin);
		*pMsg = *this;
	};
};


extern bool wtp_initialize_cfg_access(void);
extern WTP_cIE_type *wtp_Get_cIE_Parser_Obj(uint8_t *ptr_cIE);
int GetNumOfWiFiRadio();
int GetNumOfWiFiInterface();
