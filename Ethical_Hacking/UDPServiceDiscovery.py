import sys
from scapy.all import *
ans,unans=sr(IP(dst="10.10.111.1")/UDP(dport=53)/DNS(rd=1,qd=DNSQR(qname="www.thepacketgeek.com")),timeout=5,retry=5)
#print unans[DNS].summary()
print ans[DNS].summary()

conf.checkIPaddr = False
fam,hw = get_if_raw_hwaddr(conf.iface)
dhcp_discover = Ether(dst="02:00:81:62:01:02")/IP(src="10.10.111.107",dst="10.10.111.1")/UDP(sport=68,dport=67)\
/BOOTP(chaddr=hw)/DHCP(options=[("message-type","discover"),"end"])
ans,unans = srp(dhcp_discover,multi=True,timeout=5,retry=5)

ans.summary()

for p in ans:
	print p[1][Ether].src, p[1][IP].src
