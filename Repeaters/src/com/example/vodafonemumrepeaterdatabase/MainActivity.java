package com.example.vodafonemumrepeaterdatabase;

import java.util.Calendar;
import android.location.LocationManager;
import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Service;
import android.content.Intent;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;


public class MainActivity extends Activity {
	
	String repeater_type_val[]={ "2G-10dBm",
			                 "2G-20dBm",
			                 "3G-20dBm",
			                 "3G-30dBm",
			                 "2G+3G"
			};
	String repeater_make_val[]={"Astra DB1",
			                "(PHDCOMM)JAS HOME REPEATER",
			                "Astra DB2",
			                "ASTRA DB3",
			                "ASTRA IN3",
			                "ASTRA IND",
			                "Cellcomm",
			                "Grentech",
			                "Hyon Home Repeater",
			                "Kaveri Repeater",
			                "Microqual",
			                "Mikom 303B",
			                "Remotek 900",
			                "Shyam DB3",
			                "Shyam DB5",
			                "Shyam DB5 BCCH",
			                "Shyam Home Repeater",
			                "WRI 900",
			                "WRI Repeater",
			                "WRI Train Repeater"
	};
	
	String zone_val[]={"West","South","Malad","Thane","Vashi"
			
	};
	
	String status_val[]={"No Problem Found",
			         "Faulty",
			         "Maintenance Required"
	};
	
	String reading_val[]={"Yagi",
			         "On Road",
			         "Living Room",
			         "Bed Room",
			         "Office",
			         "Cabin"
	};
	
	String purpose_val[]={"Repeater Audit",
			              "Installation",
			              "Maintenance"};
	
	String s1Text,s2Text,s3Text,s4Text,s5Text,s6Text;
	TextView time;
	TextView time_val;
	TextView date;
	TextView date_val;
	TextView serial_no;
	EditText serial_val;
	TextView part_no;
	EditText part_val;
	TextView SAP;
	EditText SAP_val;
	TextView WBS_ID;
	EditText WBS_val;
	TextView repeater_type;
	Spinner s1;
	TextView repeater_make;
	Spinner s2;
	TextView zone;
	Spinner s3;
	TextView purpose;
	Spinner s6;
	TextView status;
	Spinner s4;
	TextView site_name;
	EditText site_name_val;
	TextView address;
	EditText address_val;
	TextView contacted_person;
	TextView empty;
	TextView name;
	EditText name_val;
	TextView mobile_no;
	EditText mobile_no_val;
	TextView alt_no;
	EditText alt_no_val;
	TextView installation_date;
	EditText installation_date_val;
    TextView sim_no;
	EditText sim_no_val;
	TextView signal_strength;
	TextView signal_strength_val;
	TextView net_type;
	TextView net_type_val;
	TextView cell_id;
	TextView cell_id_val;
	TextView lac;
	TextView lac_val;
	TextView lat;
	TextView lat_val;
	TextView longi;
	TextView long_val;
	TextView AZM;
	EditText AZM_val;
	TextView EcNo;
	EditText EcNo_val;
	TextView dl;
	EditText dl_val;
	TextView ul;
	EditText ul_val;
	TextView rep_loc;
	EditText rep_loc_val;
	TextView cov_area;
	EditText cov_area_val;
	TextView omni;
	EditText omni_val;
	TextView panel;
	EditText panel_val;
	TextView count;
	EditText count_val;
	TextView reading;
	Spinner s5;
	TextView remark;
	EditText remark_val;
	Button send;
	
	
	double latitude = 0;
	double longitude = 0;
	int sig_strength = 0;
	String networkType = "";
	
	SignalStrengthListener mSignalStrengthListener;
	TelephonyManager mTelephonyManager;
	GsmCellLocation gsmCellLocation;
	LocationManager mLocationManager;



	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	
	time= (TextView)this.findViewById(R.id.time);
	time_val= (TextView)this.findViewById(R.id.time_value);
	date=(TextView)this.findViewById(R.id.date);
	date_val=(TextView)this.findViewById(R.id.date_value);
	serial_no=(TextView)this.findViewById(R.id.serial_No);
	serial_val=(EditText)this.findViewById(R.id.serial_No_Value);
	part_no=(TextView)this.findViewById(R.id.part_No);
	part_val=(EditText)this.findViewById(R.id.part_No_Value);
	SAP= (TextView)this.findViewById(R.id.SAP);
	SAP_val=(EditText)this.findViewById(R.id.SAP_Value);
	WBS_ID=(TextView)this.findViewById(R.id.WBS_ID);
	WBS_val=(EditText)this.findViewById(R.id.WBS_ID_Value);
	repeater_type=(TextView)this.findViewById(R.id.repeaterType);
	s1=(Spinner)this.findViewById(R.id.repeaterType_val);
	repeater_make= (TextView)this.findViewById(R.id.repeaterMake);
	s2=(Spinner)this.findViewById(R.id.repeaterMake_val);
	zone=(TextView)this.findViewById(R.id.zone);
	s3=(Spinner)this.findViewById(R.id.zone_val);
	purpose=(TextView)this.findViewById(R.id.purpose);
	s6=(Spinner)this.findViewById(R.id.purpose_val);
	status=(TextView)this.findViewById(R.id.status);
	s4=(Spinner)this.findViewById(R.id.status_val);
	site_name=(TextView)this.findViewById(R.id.SiteName);
	site_name_val=(EditText)this.findViewById(R.id.SiteName_val);
	address= (TextView)this.findViewById(R.id.address);
	address_val=(EditText)this.findViewById(R.id.address_val);
	contacted_person=(TextView)this.findViewById(R.id.contact_person);
    empty=(TextView)this.findViewById(R.id.empty);
    name=(TextView)this.findViewById(R.id.name);
    name_val=(EditText)this.findViewById(R.id.name_val);
    mobile_no=(TextView)this.findViewById(R.id.phone);
    mobile_no_val=(EditText)this.findViewById(R.id.phone_val);
    alt_no=(TextView)this.findViewById(R.id.Alt);
    alt_no_val=(EditText)this.findViewById(R.id.Alt_val);
    installation_date=(TextView)this.findViewById(R.id.InstallationDate);
    installation_date_val=(EditText)this.findViewById(R.id.InstallationDate_val);
    sim_no=(TextView)this.findViewById(R.id.repeaterSimNo);
    sim_no_val=(EditText)this.findViewById(R.id.repeaterSimNo__val);
    signal_strength=(TextView)this.findViewById(R.id.signal_strength_text);
    signal_strength_val=(TextView)this.findViewById(R.id.signal_strength_value);
    net_type=(TextView)this.findViewById(R.id.network_type_text);
    net_type_val=(TextView)this.findViewById(R.id.network_type_value);
    cell_id=(TextView)this.findViewById(R.id.cell_id_text);
    cell_id_val=(TextView)this.findViewById(R.id.cell_id_value);
    lac=(TextView)this.findViewById(R.id.lac_text);
    lac_val=(TextView)this.findViewById(R.id.lac_value);
    lat=(TextView)this.findViewById(R.id.lat_text);
    lat_val=(TextView)this.findViewById(R.id.lat_value);
    longi=(TextView)this.findViewById(R.id.long_text);
    long_val=(TextView)this.findViewById(R.id.long_value);
    AZM=(TextView)this.findViewById(R.id.AZM);
    AZM_val=(EditText)this.findViewById(R.id.AZM_val);
    EcNo=(TextView)this.findViewById(R.id.EcNo);
    EcNo_val=(EditText)this.findViewById(R.id.EcNo_val);
    dl=(TextView)this.findViewById(R.id.DL_speed);
    dl_val=(EditText)this.findViewById(R.id.DL_speed_val);
    ul=(TextView)this.findViewById(R.id.UL_speed);
    ul_val=(EditText)this.findViewById(R.id.UL_speed_val);
    rep_loc=(TextView)this.findViewById(R.id.repeater_Location);
    rep_loc_val=(EditText)this.findViewById(R.id.repeater_Location_val);
    cov_area=(TextView)this.findViewById(R.id.Coverage_Area);
    cov_area_val=(EditText)this.findViewById(R.id.Caverage_Area_val);
    panel=(TextView)this.findViewById(R.id.Panel_Antenna);
    panel_val=(EditText)this.findViewById(R.id.Panel_Antenna_val);
    omni=(TextView)this.findViewById(R.id.Omni_antenna);
    omni_val=(EditText)this.findViewById(R.id.Omni_antenna_val);
    count=(TextView)this.findViewById(R.id.Count);
    count_val=(EditText)this.findViewById(R.id.count_val);
    reading=(TextView)this.findViewById(R.id.reading);
    s5=(Spinner)this.findViewById(R.id.reading_val);
    remark=(TextView)this.findViewById(R.id.Remarks);
    remark_val=(EditText)this.findViewById(R.id.remarks_val);
    send=(Button)this.findViewById(R.id.button1);
    
 // Initialize Telephony Manager and put listener to Signal Strength
 		this.mTelephonyManager = (TelephonyManager) this
 				.getSystemService(Service.TELEPHONY_SERVICE);
 		this.mLocationManager = (LocationManager) this
 				.getSystemService(Service.LOCATION_SERVICE);

    
	getTimeAndDate();
	getSignalStrength();
	getNetworkType();
	getCellInformation();
	getLatLong();

	// class dropbox
	ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
			android.R.layout.simple_spinner_item,repeater_type_val);
	s1.setAdapter(adapter);
	
	adapter = new ArrayAdapter<String>(this,
			android.R.layout.simple_spinner_item,repeater_make_val);
	s2.setAdapter(adapter);
	
	adapter = new ArrayAdapter<String>(this,
			android.R.layout.simple_spinner_item,zone_val);
	s3.setAdapter(adapter);
	
	adapter = new ArrayAdapter<String>(this,
			android.R.layout.simple_spinner_item,status_val);
	s4.setAdapter(adapter);
	
	adapter = new ArrayAdapter<String>(this,
			android.R.layout.simple_spinner_item,reading_val);
	s5.setAdapter(adapter);
	
	adapter = new ArrayAdapter<String>(this,
			android.R.layout.simple_spinner_item,purpose_val);
	s6.setAdapter(adapter);


	s1.setSelection(0);
	s2.setSelection(0);
	s3.setSelection(0);
	s4.setSelection(0);
	s5.setSelection(0);
	s6.setSelection(0);
	s1.setOnItemSelectedListener(new OnItemSelectedListener(){
		
		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			// TODO Auto-generated method stub
	 s1Text=repeater_type_val[arg2];
			
		}
	});
    
	s2.setOnItemSelectedListener(new OnItemSelectedListener(){
		
		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			// TODO Auto-generated method stub
	 s2Text=repeater_make_val[arg2];
			
		}
	});
	
    s3.setOnItemSelectedListener(new OnItemSelectedListener(){
		
		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			// TODO Auto-generated method stub
	 s3Text=zone_val[arg2];
			
		}
	});

   s4.setOnItemSelectedListener(new OnItemSelectedListener(){
		
		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			// TODO Auto-generated method stub
	 s4Text=status_val[arg2];
			
		}
	});
   
   s5.setOnItemSelectedListener(new OnItemSelectedListener(){
		
		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			// TODO Auto-generated method stub
	 s5Text=reading_val[arg2];
			
		}
	});
   
   s6.setOnItemSelectedListener(new OnItemSelectedListener(){
		
		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			// TODO Auto-generated method stub
	 s6Text=purpose_val[arg2];
			
		}
	});
   

   
   send.setOnClickListener(new OnClickListener() {

		@Override
		public void onClick(View arg0) {
			// TODO Auto-generated method stub
		    Intent i = new Intent(MainActivity.this,
			SendDataService.class);
			i.putExtra("date", date_val.getText().toString());
			i.putExtra("time", time_val.getText().toString());
			i.putExtra("serial no", serial_val.getText().toString());
			i.putExtra("part No", part_val.getText().toString());
			i.putExtra("SAP", SAP_val.getText().toString());
			i.putExtra("WBS ID", WBS_val.getText().toString());
			i.putExtra("repeater type", s1Text.toString());
			i.putExtra("repeater make", s2Text.toString());
			i.putExtra("zone", s3Text.toString());
			i.putExtra("purpose",s6Text.toString());
			i.putExtra("status", s4Text.toString());
			i.putExtra("site name", site_name_val.getText().toString());
			i.putExtra("address", address_val.getText().toString());
			i.putExtra("contact person name", name_val.getText().toString());
			i.putExtra("phone", mobile_no_val.getText().toString());
			i.putExtra("alt", alt_no_val.getText().toString());
			i.putExtra("installation date",installation_date_val.getText().toString());
			i.putExtra("SIM No",sim_no_val.getText().toString());
			i.putExtra("latitude", lat_val.getText().toString());
			i.putExtra("longitude", long_val.getText().toString());
			i.putExtra("signal_strength_value",signal_strength_val.getText().toString());
			i.putExtra("network_type", net_type_val.getText().toString());
			i.putExtra("cell_id", cell_id_val.getText().toString());
			i.putExtra("lac_value", lac_val.getText().toString());
			i.putExtra("EcNo", EcNo_val.getText().toString());
			i.putExtra("dl speed", dl_val.getText().toString());
			i.putExtra("ul speed", ul_val.getText().toString());
			i.putExtra("AZM",AZM_val.getText().toString());
			i.putExtra("rep location",rep_loc_val.getText().toString());
			i.putExtra("coverage area",cov_area_val.getText().toString());
			i.putExtra("omni", omni_val.getText().toString());
			i.putExtra("panel", panel_val.getText().toString());
			i.putExtra("count", count_val.getText().toString());
			i.putExtra("reading",s5Text.toString());
			i.putExtra("Remarks", remark_val.getText().toString());
			startService(i);
			}	
	});
	}
   
   private void getTimeAndDate() {
		Calendar c = Calendar.getInstance();
		time_val.setText(c.get(Calendar.HOUR_OF_DAY) + ":"
				+ c.get(Calendar.MINUTE));
		date_val.setText(c.get(Calendar.YEAR) + "-"
				+ (c.get(Calendar.MONTH) + 1) + "-"
				+ c.get(Calendar.DAY_OF_MONTH));

	}

   private void getSignalStrength() {
		// start the signal strength listener
		mSignalStrengthListener = new SignalStrengthListener();
		mTelephonyManager.listen(mSignalStrengthListener,SignalStrengthListener.LISTEN_SIGNAL_STRENGTHS);

	}

	@SuppressLint("NewApi")
	private void getCellInformation() {
		// TODO Auto-generated method stub
		gsmCellLocation = (GsmCellLocation) this.mTelephonyManager.getCellLocation();
		int netType = this.mTelephonyManager.getNetworkType();
		if (netType == 8 || netType == 10 || netType == 15 || netType == 9)
			cell_id_val.setText(Integer.toString((gsmCellLocation.getCid()) % 65536));
		else
			cell_id_val.setText(Integer.toString(gsmCellLocation.getCid()));
		lac_val.setText(Integer.toString(gsmCellLocation.getLac()));
	}

	private void getLatLong() {
		// TODO Auto-generated method stub
		GPSTracker gpsTracker = new GPSTracker(getApplicationContext());
		if (gpsTracker.canGetLocation()) {
			this.lat_val.setText("" + gpsTracker.latitude);
			this.long_val.setText("" + gpsTracker.longitude);
		} else {
			gpsTracker.showSettingsAlert();
		}
	}

	private void getNetworkType() {
		// TODO Auto-generated method stub

		Log.d("Network Type",
				"TYPE: " + this.mTelephonyManager.getNetworkType());

		switch (this.mTelephonyManager.getNetworkType()) {
		case 7:
			net_type_val.setText("1xRTT");
			break;
		case 4:
			net_type_val.setText("CDMA");
			break;
		case 2:
			net_type_val.setText("EDGE");
			break;
		case 14:
			net_type_val.setText("eHRPD");
			break;
		case 5:
			net_type_val.setText("EVDO rev. 0");
			break;
		case 6:
			net_type_val.setText("EVDO rev. A");
			break;
		case 12:
			net_type_val.setText("EVDO rev. B");
			break;
		case 1:
			net_type_val.setText("GPRS");
			break;
		case 8:
			net_type_val.setText("HSDPA");
			break;
		case 10:
			net_type_val.setText("HSPA");
			break;
		case 15:
			net_type_val.setText("HSPA+");
			break;
		case 9:
			net_type_val.setText("HSUPA");
			break;
		case 11:
			net_type_val.setText("iDen");
			break;
		case 13:
			net_type_val.setText("LTE");
			break;
		case 3:
			net_type_val.setText("UMTS");
			break;
		case 0:
			net_type_val.setText("Unknown");
			break;
		}

		networkType = net_type_val.getText().toString();

	}

	/*
	 * @SuppressLint("NewApi") private void getNeighboringCellInfo(){
	 * List<NeighboringCellInfo> cellInfo =
	 * mTelephonyManager.getNeighboringCellInfo(); for(NeighboringCellInfo info:
	 * cellInfo){ String networkType =
	 * this.getNetworkType(info.getNetworkType()); int cid = info.getCid(); int
	 * lac = info.getLac(); int psc = info.getPsc(); int rssi = info.getRssi();
	 * 
	 * Log.d(this.getClass().getSimpleName(),
	 * "Neighbouring Network Type: "+networkType);
	 * Log.d(this.getClass().getSimpleName(), "Neighbouring CID: "+cid);
	 * Log.d(this.getClass().getSimpleName(), "Neighbouring LAC: "+lac);
	 * Log.d(this.getClass().getSimpleName(), "Neighbouring PSC: "+psc);
	 * Log.d(this.getClass().getSimpleName(), "Neighbouring RSSI: "+rssi);
	 * 
	 * 
	 * // Use above Variables as per the requirement } }
	 */

	// Class which Listens to Signal Strength
	public class SignalStrengthListener extends PhoneStateListener {
		@Override
		public void onSignalStrengthsChanged(
				android.telephony.SignalStrength signalStrength) {

			// get the signal strength (a value between 0 and 31)
			int strengthAmplitude = signalStrength.getGsmSignalStrength();
			sig_strength = strengthAmplitude;
			String result = "Excellent";
			if ((strengthAmplitude * 2 - 113) > -50) {
				result = "Excellent";
			} else if ((strengthAmplitude * 2 - 113) > -60
					&& (strengthAmplitude * 2 - 113) < -50) {
				result = "Very Good";
			} else if ((strengthAmplitude * 2 - 113) > -80
					&& (strengthAmplitude * 2 - 113) < -60) {
				result = "Good";
			} else if ((strengthAmplitude * 2 - 113) > -90
					&& (strengthAmplitude * 2 - 113) < -80)
				result = "Average";
			else if ((strengthAmplitude * 2 - 113) < -90)
				result = "Weak";

			// do something with it (in this case we update a text view)
			signal_strength_val.setText(result + " ("
					+ (strengthAmplitude * 2 - 113) + ")");
			super.onSignalStrengthsChanged(signalStrength);
			
			//if(mCellIdValue.getText()==gsmCellLocation.getCid().toString());
			getCellInformation();
			
		}
	}





	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	
	

}
