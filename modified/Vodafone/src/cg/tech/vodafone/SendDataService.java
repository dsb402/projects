package cg.tech.vodafone;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeoutException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;


import android.app.IntentService;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;


public class SendDataService extends Service{
	
	// ===========================================================
    // Constants
    // ===========================================================
	
	private static final String TAG = SendDataService.class.getSimpleName();
	private static final String url = "http://58.146.103.42:8085/chaitanya/";
	
	// ===========================================================
    // Fields
    // ===========================================================
	
	private ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
	
	private DefaultHttpClient httpClient;
	private HttpEntity httpEntity;
	private HttpResponse httpResponse;
	private HttpPost httpPost;
	private boolean mobileDataEnabled;
	
	String response = null;
	
	public SendDataService() {

	}

	@SuppressWarnings("unchecked")
	@Override
	public int onStartCommand(Intent intent, int flags, int startId){
		Log.d(TAG, "onStartCommand Called");
		
		// Get Extras 
		Bundle extras = intent.getExtras();
		
		// Get Data from Bundle
		params = new ArrayList<NameValuePair>();
        Set<String> keys = extras.keySet();
        Iterator<String> it = keys.iterator();
        while (it.hasNext()) {
            String key = it.next();
            Log.e("SendData","[" + key + "=" + extras.get(key)+"]");
            BasicNameValuePair p = new BasicNameValuePair(key, (String) extras.get(key));
            params.add(p);
        }
	        
		
		// Set Connection TimeOut
    	HttpParams basicparams = new BasicHttpParams();
    	HttpConnectionParams.setConnectionTimeout(basicparams, 3000);
    	
		// Init Fields
		httpClient = new DefaultHttpClient(basicparams);
		httpPost = new HttpPost(url);
		
		new SendDataTask().execute(params);
		
		
		return startId;
	}
	
	public void retrySending(){
		Log.d(TAG, "Retrying .. 3 seconds TimeOut");
		
		try{
			new CountDownTimer(3000, 1000){
				@SuppressWarnings("unchecked")
				@Override
				public void onFinish() {
					// TODO Auto-generated method stub
					Log.d(TAG, "CountDown Timer Finished");
					new SendDataTask().execute(params);
				}

				@Override
				public void onTick(long millisUntilFinished) {
					// TODO Auto-generated method stub
					Log.d(TAG, "CountDownTimer: "+millisUntilFinished/1000 +" seconds remainig");
				}	
			}.start();
		}catch (Exception e){
			e.printStackTrace();
		}
		
	}
	
	public boolean isNetworkAvailable() {
		ConnectivityManager cm = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()
				&& cm.getActiveNetworkInfo().isAvailable()
				&& cm.getActiveNetworkInfo().isConnected()) {
			return true;
		} else {
			return false;
		}
	}
	
	public String sendData(){
		
		try {
			// If parameters are there
			if(params.size() > 0){
				httpPost.setEntity(new UrlEncodedFormEntity(params));
			}
			
			// Execute and get Response
			httpResponse = httpClient.execute(httpPost);
			
			//  Get Response
			httpEntity = httpResponse.getEntity();
			response = EntityUtils.toString(httpEntity);
	        
	        // Log
	        Log.d(TAG, "Response: "+response);
	        
	        // Show Toast
	        Handler mHandler = new Handler(getMainLooper());
	        mHandler.post(new Runnable() {
	            @Override
	            public void run() {
	            	 Toast.makeText(getApplicationContext(), response, Toast.LENGTH_LONG).show();
	            }
	        });
	        
		} catch (Exception e) {
			
			if(e instanceof TimeoutException){
				// Timeout Occurred
				Log.d("SendData Exception" ,"TimeoutException Occured");
				//retrySending();
			}else if(e instanceof HttpHostConnectException){
				// Server Not Reachable. Service Up-Down. Invalid Address
				Log.d("SendData Exception" ,"HttpHostConnectException Occured");
				//retrySending();
			}else if(e instanceof ConnectTimeoutException){
				// ConnectTimeoutException Occurred
				Log.d("SendData Exception" ,"ConnectTimeoutException Occured");
				//retrySending();
			}else{
				Log.d("SendData Exception" ,"Unknown Exception Occured");
				e.printStackTrace();
				//retrySending();
			}
		}
	
		
		return response;
		
	}
	
	
	public class SendDataTask extends AsyncTask<ArrayList<NameValuePair>, String, String> {

		@Override
		protected String doInBackground(ArrayList<NameValuePair>... arg0) {
			
			// Check If Network Available
		    if(isNetworkAvailable()){
		    	sendData();
		    }else{
		    	Log.d(TAG, "Network Not Available");
		    	retrySending();
		    }
			
			return response;
		}
		
		@Override
		protected void onPostExecute(String resultData) {
			if(resultData != null){
				// Sucess
			}else{
				// Failed
				retrySending();
				this.cancel(false);
			}
		}
	}

	
	
	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	

}
