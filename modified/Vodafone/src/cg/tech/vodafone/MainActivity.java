package cg.tech.vodafone;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.telephony.CellInfoGsm;
import android.telephony.NeighboringCellInfo;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.text.format.Time;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

public class MainActivity extends Activity {

	String[] cust_class = { "Enterprise", "HNI", "SME", "Retailor",
			"Distributor", "ESD", "OGM", "Retention", "others" };

	String[] room_type = { "Res-Living Room", "Res-Bedroom1", "Res-Bedroom2",
			"Res-Bedroom3", "Res-Terrace", "Res-Balcony", "Res-Lobby",
			"COR- Small Office", "COR-Medium Office", "COR- Big Office",
			"On Roads", "others" };

	String[] solution_type = { "No Problem Found/Temporary Problem",
			"2G Home Repeater", "3G Home Repeater", "2G Full Repeater",
			"3G Full Repeater", "IBS Extension Required",
			"Femto Feasibility to be checked", "Repeater Extension Required",
			"Repeater Maintainance", "STS Not Affordable",
			"STS not Technically Feasible" };
	String[] long_term_solution = { "New IBS to be planned",
			"New Monopole to be planned", "Sector Addition", "Sector Split",
			"Site Planned", "No Site Planned", "Optimisation Planned" };
	String[] req_type = { "2G", "3G", "2G+3G" };
	String[] zone_type = { "WestZone", "SouthZone", "ThaneZone", "VashiZone",
			"MaladZone" };
	String[] building_type = { "Normal Residential(<8 floor)",
			"Normal High Rise(8-25 floors),", "Mega High Rise(>25 floors)",
			"Residential Township", "Residential + commercial Township",
			"Commercial/Industrial Estate", "Corporate Center", "Hotel", "Mall" };

	String s1Text, s2Text, s3Text, s4Text, s5Text, s6Text, s7Text;

	TextView time;
	TextView time_val;
	TextView date;
	TextView date_val;
	TextView sr_number;
	TextView sr_number_value;
	TextView contacted_person;
	TextView empty;
	TextView name;
	EditText name_val;
	TextView phone;
	EditText phone_val;
	TextView engr_name;
	EditText engr_name_val;
	TextView zone;
	Spinner zone_val;
	TextView cust_req;
	Spinner cust_req_val;
	TextView customer_class;
	Spinner s1;
	TextView others;
	EditText others_val;
	TextView bldg_type;
	Spinner bldg_type_val;
	TextView room;
	TextView floor;
	TextView floor_val;
	Spinner s2;

	SignalStrengthListener mSignalStrengthListener;
	TelephonyManager mTelephonyManager;
	GsmCellLocation gsmCellLocation;
	LocationManager mLocationManager;

	TextView mSignalStrengthValue;
	TextView mNetworkTypeValue;
	TextView mCellIdValue;
	TextView mLacValue;
	TextView mPsc;
	TextView mPsc_val;
	TextView mLatValue;
	TextView mLongValue;
	TextView mLocalityValue;
	TextView mPostalCodeValue;
	TextView mAddressLineValue;
	TextView solution;
	Spinner s3;
	TextView LTS;
	Spinner LTS_val;
	TextView remarks;
	TextView empty1;
	EditText remarks_val;
	Button send;

	double latitude = 0;
	double longitude = 0;
	int signal_strength = 0;
	String networkType = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// Initialize TextViews
		time = (TextView) this.findViewById(R.id.time);
		time_val = (TextView) this.findViewById(R.id.time_value);
		date = (TextView) this.findViewById(R.id.date);
		date_val = (TextView) this.findViewById(R.id.date_value);
		sr_number = (TextView) this.findViewById(R.id.SR_No);
		sr_number_value = (EditText) this.findViewById(R.id.SR_No_Value);
		contacted_person = (TextView) this.findViewById(R.id.contact_person);
		empty = (TextView) this.findViewById(R.id.empty);
		name = (TextView) this.findViewById(R.id.name);
		name_val = (EditText) this.findViewById(R.id.name_val);
		phone = (TextView) this.findViewById(R.id.phone);
		phone_val = (EditText) this.findViewById(R.id.phone_val);
		engr_name = (TextView) this.findViewById(R.id.engr_name);
		engr_name_val = (EditText) this.findViewById(R.id.engr_name_val);
		zone = (TextView) this.findViewById(R.id.zone);
		zone_val = (Spinner) this.findViewById(R.id.zone_val);
		cust_req = (TextView) this.findViewById(R.id.cust_req);
		cust_req_val = (Spinner) this.findViewById(R.id.req_val);
		customer_class = (TextView) this.findViewById(R.id.cust_class);
		s1 = (Spinner) this.findViewById(R.id.class_val);

		bldg_type = (TextView) this.findViewById(R.id.bldg_type);
		bldg_type_val = (Spinner) this.findViewById(R.id.bldg_type_val);
		room = (TextView) this.findViewById(R.id.room);
		floor = (TextView) this.findViewById(R.id.floors);
		floor_val = (EditText) this.findViewById(R.id.floors_val);

		s2 = (Spinner) this.findViewById(R.id.room_val);

		others = (TextView) this.findViewById(R.id.others);
		others_val = (EditText) this.findViewById(R.id.others_val);
		solution = (TextView) this.findViewById(R.id.solution);
		LTS_val = (Spinner) this.findViewById(R.id.LTS_val);
		s3 = (Spinner) this.findViewById(R.id.solution_val);
		remarks = (TextView) this.findViewById(R.id.remarks);
		remarks_val = (EditText) this.findViewById(R.id.remarks_val);
		empty1 = (TextView) this.findViewById(R.id.empty1);

		this.mSignalStrengthValue = (TextView) this
				.findViewById(R.id.signal_strength_value);
		this.mNetworkTypeValue = (TextView) this
				.findViewById(R.id.network_type_value);
		this.mCellIdValue = (TextView) this.findViewById(R.id.cell_id_value);
		this.mLacValue = (TextView) this.findViewById(R.id.lac_value);
		this.mPsc = (TextView) this.findViewById(R.id.psc);
		this.mPsc_val = (TextView) this.findViewById(R.id.psc_value);
		this.mLatValue = (TextView) this.findViewById(R.id.lat_value);
		this.mLongValue = (TextView) this.findViewById(R.id.long_value);
		this.mLocalityValue = (TextView) this.findViewById(R.id.locality_value);
		this.mPostalCodeValue = (TextView) this
				.findViewById(R.id.postal_code_value);
		this.mAddressLineValue = (TextView) this
				.findViewById(R.id.address_line_value);

		this.send = (Button) this.findViewById(R.id.button1);

		// Initialize Telephony Manager and put listener to Signal Strength
		this.mTelephonyManager = (TelephonyManager) this
				.getSystemService(Service.TELEPHONY_SERVICE);
		this.mLocationManager = (LocationManager) this
				.getSystemService(Service.LOCATION_SERVICE);

		getTimeAndDate();
		getSignalStrength();
		getNetworkType();
		getCellInformation();
		getLatLong();

		// class dropbox
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, cust_class);
		s1.setAdapter(adapter);

		// room dropbox
		adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, room_type);
		s2.setAdapter(adapter);

		// solution dropbox
		adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, solution_type);
		s3.setAdapter(adapter);
		// zone dropbox
		adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, zone_type);
		zone_val.setAdapter(adapter);

		// cust_req dropbox
		adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, req_type);
		cust_req_val.setAdapter(adapter);

		// bldg_type dropbox
		adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, building_type);
		bldg_type_val.setAdapter(adapter);

		// LTS_sol dropbox
		adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, long_term_solution);
		LTS_val.setAdapter(adapter);

		zone_val.setSelection(0);
		s1.setSelection(0);
		s2.setSelection(0);
		s3.setSelection(0);
		cust_req_val.setSelection(0);
		LTS_val.setSelection(0);
		bldg_type_val.setSelection(0);
		
		s1.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				s1Text = cust_class[arg2];
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});

		s2.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				s2Text = room_type[arg2];
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});

		s3.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				s3Text = solution_type[arg2];
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});

		cust_req_val.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				s4Text = req_type[arg2];
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});

		LTS_val.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				s5Text = long_term_solution[arg2];
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});

		zone_val.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				s6Text = zone_type[arg2];
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});

		bldg_type_val.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				s7Text = building_type[arg2];
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});

		send.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (((Integer.parseInt(floor_val.getText().toString())) + 1) < 0) {
					Log.d(this.getClass().getSimpleName(), "Condition OK");
					floor_val.setText("");
					return;
				} else {
					Intent i = new Intent(MainActivity.this,SendDataService.class);
					i.putExtra("Date", date_val.getText().toString());
					i.putExtra("Time", time_val.getText().toString());
					i.putExtra("SR_No", sr_number_value.getText().toString());
					i.putExtra("Contacted_Person_Name", name_val.getText()
							.toString());
					i.putExtra("Contact_person_phone", phone_val.getText()
							.toString());
					i.putExtra("Engineer_Name", engr_name_val.getText()
							.toString());
					i.putExtra("zone", s6Text.toString());
					i.putExtra("Customer Requirement", s4Text.toString());
					i.putExtra("customer_class", s1Text.toString());
					i.putExtra("others", others_val.getText().toString());
					i.putExtra("building type", s7Text.toString());
					i.putExtra("Room", s2Text.toString());
					i.putExtra("Floor", floor_val.getText().toString());
					i.putExtra("Psc", mPsc_val.getText().toString());
					i.putExtra("latitude", mLatValue.getText().toString());
					i.putExtra("longitude", mLongValue.getText().toString());
					i.putExtra("signal_strength_value", mSignalStrengthValue
							.getText().toString());
					i.putExtra("network_type", mNetworkTypeValue.getText()
							.toString());
					i.putExtra("cell_id", mCellIdValue.getText().toString());
					i.putExtra("lac_value", mLacValue.getText().toString());
					i.putExtra("locality", mLocalityValue.getText().toString());
					i.putExtra("postal_code", mPostalCodeValue.getText()
							.toString());
					i.putExtra("address_line", mAddressLineValue.getText()
							.toString());
					i.putExtra("Solution", s3Text.toString());
					i.putExtra("LTS_val", s5Text.toString());
					i.putExtra("Remarks", remarks_val.getText().toString());
					i.putExtra("empty1", empty1.getText().toString());
					startService(i);
				}
			}
		});

	}

	private void getTimeAndDate() {
		Calendar c = Calendar.getInstance();
		time_val.setText(c.get(Calendar.HOUR_OF_DAY) + ":"
				+ c.get(Calendar.MINUTE));
		date_val.setText(c.get(Calendar.YEAR) + "-"
				+ (c.get(Calendar.MONTH) + 1) + "-"
				+ c.get(Calendar.DAY_OF_MONTH));

	}

	private void getSignalStrength() {
		// start the signal strength listener
		mSignalStrengthListener = new SignalStrengthListener();
		mTelephonyManager.listen(mSignalStrengthListener,
				SignalStrengthListener.LISTEN_SIGNAL_STRENGTHS);

	}

	@SuppressLint("NewApi")
	private void getCellInformation() {
		// TODO Auto-generated method stub
		gsmCellLocation = (GsmCellLocation) this.mTelephonyManager
				.getCellLocation();
		int netType = this.mTelephonyManager.getNetworkType();
		if (netType == 8 || netType == 10 || netType == 15 || netType == 9)
			mCellIdValue
					.setText(Integer.toString((gsmCellLocation.getCid()) % 65536));
		else
			mCellIdValue.setText(Integer.toString(gsmCellLocation.getCid()));
		mLacValue.setText(Integer.toString(gsmCellLocation.getLac()));
		mPsc_val.setText(Integer.toString(gsmCellLocation.getPsc()));
	}

	private void getLatLong() {
		// TODO Auto-generated method stub
		GPSTracker gpsTracker = new GPSTracker(getApplicationContext());
		if (gpsTracker.canGetLocation()) {
			this.mLatValue.setText("" + gpsTracker.latitude);
			this.mLongValue.setText("" + gpsTracker.longitude);
			this.mLocalityValue.setText(""
					+ gpsTracker.getLocality(getApplicationContext()));
			this.mPostalCodeValue.setText(""
					+ gpsTracker.getPostalCode(getApplicationContext()));
			this.mAddressLineValue.setText(""
					+ gpsTracker.getAddressLine(getApplicationContext()));
		} else {
			gpsTracker.showSettingsAlert();
		}
	}

	private void getNetworkType() {
		// TODO Auto-generated method stub

		Log.d("Network Type",
				"TYPE: " + this.mTelephonyManager.getNetworkType());

		switch (this.mTelephonyManager.getNetworkType()) {
		case 7:
			mNetworkTypeValue.setText("1xRTT");
			break;
		case 4:
			mNetworkTypeValue.setText("CDMA");
			break;
		case 2:
			mNetworkTypeValue.setText("EDGE");
			break;
		case 14:
			mNetworkTypeValue.setText("eHRPD");
			break;
		case 5:
			mNetworkTypeValue.setText("EVDO rev. 0");
			break;
		case 6:
			mNetworkTypeValue.setText("EVDO rev. A");
			break;
		case 12:
			mNetworkTypeValue.setText("EVDO rev. B");
			break;
		case 1:
			mNetworkTypeValue.setText("GPRS");
			break;
		case 8:
			mNetworkTypeValue.setText("HSDPA");
			break;
		case 10:
			mNetworkTypeValue.setText("HSPA");
			break;
		case 15:
			mNetworkTypeValue.setText("HSPA+");
			break;
		case 9:
			mNetworkTypeValue.setText("HSUPA");
			break;
		case 11:
			mNetworkTypeValue.setText("iDen");
			break;
		case 13:
			mNetworkTypeValue.setText("LTE");
			break;
		case 3:
			mNetworkTypeValue.setText("UMTS");
			break;
		case 0:
			mNetworkTypeValue.setText("Unknown");
			break;
		}

		networkType = mNetworkTypeValue.getText().toString();

	}

	/*
	 * @SuppressLint("NewApi") private void getNeighboringCellInfo(){
	 * List<NeighboringCellInfo> cellInfo =
	 * mTelephonyManager.getNeighboringCellInfo(); for(NeighboringCellInfo info:
	 * cellInfo){ String networkType =
	 * this.getNetworkType(info.getNetworkType()); int cid = info.getCid(); int
	 * lac = info.getLac(); int psc = info.getPsc(); int rssi = info.getRssi();
	 * 
	 * Log.d(this.getClass().getSimpleName(),
	 * "Neighbouring Network Type: "+networkType);
	 * Log.d(this.getClass().getSimpleName(), "Neighbouring CID: "+cid);
	 * Log.d(this.getClass().getSimpleName(), "Neighbouring LAC: "+lac);
	 * Log.d(this.getClass().getSimpleName(), "Neighbouring PSC: "+psc);
	 * Log.d(this.getClass().getSimpleName(), "Neighbouring RSSI: "+rssi);
	 * 
	 * 
	 * // Use above Variables as per the requirement } }
	 */

	// Class which Listens to Signal Strength
	public class SignalStrengthListener extends PhoneStateListener {
		@Override
		public void onSignalStrengthsChanged(
				android.telephony.SignalStrength signalStrength) {

			// get the signal strength (a value between 0 and 31)
			int strengthAmplitude = signalStrength.getGsmSignalStrength();
			signal_strength = strengthAmplitude;
			String result = "Excellent";
			if ((strengthAmplitude * 2 - 113) > -50) {
				result = "Excellent";
			} else if ((strengthAmplitude * 2 - 113) > -60
					&& (strengthAmplitude * 2 - 113) < -50) {
				result = "Very Good";
			} else if ((strengthAmplitude * 2 - 113) > -80
					&& (strengthAmplitude * 2 - 113) < -60) {
				result = "Good";
			} else if ((strengthAmplitude * 2 - 113) > -90
					&& (strengthAmplitude * 2 - 113) < -80)
				result = "Average";
			else if ((strengthAmplitude * 2 - 113) < -90)
				result = "Weak";

			// do something with it (in this case we update a text view)
			mSignalStrengthValue.setText(result + " ("
					+ (strengthAmplitude * 2 - 113) + ")");
			super.onSignalStrengthsChanged(signalStrength);
		}
	}

}
