Hello,

I have graduated from New York University with specialization in Networking in January 2016 and actively looking for full time job opportunity in the field of Telecommunication and Software Engineering.

I have worked as Software Engineer for Chameleon Wireless,Inc. ,Electrical Design Engineer for PSE&G and Wireless Network Engineer for Vodafone India Limited(Telecommunication Service Provider) previously.

I am Cisco Certified Network Associate having expertise in the area of Telecommunication and Software Engineering.

This repository includes following projects that I have worked on.

1. CAPWAP Wireless Terminal Point(WTP) implementation to build home based Wi-Fi prototype using c++, Linux, Wirehsark, UDP sockets.
2. Python scripts to perform ethical hacking.
3. Android application for automating the process of Network Complaint Analysis.
4. Android application: Repeater Database Management System.
5. Android application: Location Based Server testing application
6. Centralised database using MS Access.
7. Reproducible research using GENI
8. Paymodoro Application using Phonegap.

To be able to run Android applications, you need to follow steps mentioned below:
1. Download ADT bundle from developers.android.com. 
2. Install it.
3. Import project and run it in emulator.

or you can transfer apk file in your android emulator or android phone and run the application.

Kindly get back to me if you have any questions.
Contact information:
dsb402@nyu.edu
+1(201)-884-0138