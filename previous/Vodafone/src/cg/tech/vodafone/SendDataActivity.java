package cg.tech.vodafone;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

public class SendDataActivity extends Activity{
	
	String date = "";
	String time = "";
	String sr_no = "";
	String contacted_name = "";
	String contacted_phone = "";
	String engr_name = "";
	String zone = "";
	String cust_req = "";
	String cust_class = "";
	String others = "";
	String bldg_type = "";
	String room = "";
	String floor = "";
	String mSignalStrength = "";
	String mNetworkType = "";
	String mCellId = "";
	String mLac = "";
	String mPsc = "";
	String mLatitude = "";
	String mLongitude = "";
	String mLocality = "";
	String mPostalCode = "";
	String mAddressLine = "";
	String solution = "";
	String LTS = "";
	String empty = "";
	String remarks = "";
	
	
	
	String url = "http://crmvodafone.site11.com/vodafone/document.php";
	ArrayList<NameValuePair> dataList;
	
	@SuppressWarnings("unchecked")
	@Override
	    protected void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.activity_send);
	        
	        Bundle extras = this.getIntent().getExtras();
	        if(extras != null){
	        	date = extras.getString("Date");
	        	time = extras.getString("Time");
	        	sr_no = extras.getString("SR_No");
	        	contacted_name = extras.getString("Contacted_Person_Name");
	        	contacted_phone = extras.getString("Contact_person_phone");
	        	engr_name = extras.getString("Engineer_Name");
	        	zone= extras.getString("zone");
	        	cust_req = extras.getString("Customer Requirement");
	        	cust_class = extras.getString("customer_class");
	        	others = extras.getString("others");
	        	bldg_type = extras.getString("building type");
	        	room = extras.getString("Room");
	        	floor = extras.getString("Floor");
	        	mSignalStrength	= extras.getString("signal_strength_value");
	        	mNetworkType	= extras.getString("network_type");
	        	mCellId	= extras.getString("cell_id");
	        	mLac	= extras.getString("lac_value");
	        	mPsc    = extras.getString("Psc_value");
	        	mLatitude	= extras.getString("latitude");
	        	mLongitude	= extras.getString("longitude");
	        	mLocality	= extras.getString("locality");
	        	mPostalCode	= extras.getString("postal_code");
	        	mAddressLine	= extras.getString("address_line");
	        	solution = extras.getString("Solution");
	        	LTS = extras.getString("LTS_val");
	        	empty = extras.getString("empty1");
	        	remarks = extras.getString("Remarks");
	        	
	        }
	        
	        dataList = new ArrayList<NameValuePair>();
	        Set<String> keys = extras.keySet();
	        Iterator<String> it = keys.iterator();
	        while (it.hasNext()) {
	            String key = it.next();
	            Log.e("SendData","[" + key + "=" + extras.get(key)+"]");
	            BasicNameValuePair p = new BasicNameValuePair(key, (String) extras.get(key));
	            dataList.add(p);
	        }
	        
	        new SendDataTask().execute(dataList);
	 }


	
	public class SendDataTask extends AsyncTask<ArrayList<NameValuePair>, String, JSONObject> {

		@Override
		protected JSONObject doInBackground(ArrayList<NameValuePair>... arg0) {
			
			JSONParser jsonParser = new JSONParser();
			JSONObject obj = jsonParser.getJSONFromUrl(url,arg0[0]);
			Log.d(this.getClass().getSimpleName(), "Data:\n"+obj.toString());
			
			return obj;
		}
		
		@Override
		protected void onPostExecute(JSONObject resultData) {
			Toast.makeText(getApplicationContext(), "Data Sent\nResponse Recived: "+resultData.toString(), Toast.LENGTH_LONG).show();
			finish();
		}
	}
	
}
