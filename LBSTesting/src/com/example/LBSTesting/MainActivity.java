																																																																																												package com.example.LBSTesting;

import java.util.Calendar;

import com.example.latlongi.R;

import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.app.Activity;
import android.app.Service;
import android.content.Intent;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
	TextView Ts;
	TextView mLatValue;
	TextView mLongValue;
	TextView mCellIdValue;
	TextView mLacValue;
	TextView lat_decimal,long_decimal;
	TextView accuracy_val;
	EditText MIN_val;
	EditText remarks;
	Button SMS,Email,Refresh;
	LocationManager mLocationManager;
	TelephonyManager mTelephonyManager;
	Location location;
	GsmCellLocation gsmCellLocation;
	double latitude = 0;
	double longitude = 0;
	String MailID = "archana.adiacha@vodafone.com";
	String number = "9820018747";
	RadioGroup coordinator;
	RadioButton select;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
	    super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		this.Ts = (TextView)this.findViewById(R.id.ts_value);
		this.mLatValue = (TextView) this.findViewById(R.id.lat_value);
		this.mLongValue = (TextView) this.findViewById(R.id.long_value);
		this.MIN_val = (EditText)this.findViewById(R.id.MIN_Value);
		this.mCellIdValue = (TextView) this.findViewById(R.id.cell_id_value);
		this.lat_decimal = (TextView)this.findViewById(R.id.lat_decimal);
		this.long_decimal = (TextView)this.findViewById(R.id.long_decimal);
		SMS= (Button)findViewById(R.id.send);
		Email=(Button)findViewById(R.id.EMail);
		Refresh=(Button)findViewById(R.id.Refresh);
		this.mLacValue = (TextView) this.findViewById(R.id.lac_value);
		this.remarks = (EditText)this.findViewById(R.id.remarks_val);
		
		this.mTelephonyManager = (TelephonyManager) this
				.getSystemService(Service.TELEPHONY_SERVICE);
		this.mLocationManager = (LocationManager) this
				.getSystemService(Service.LOCATION_SERVICE);
		
		
		
		SMS.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				String Latitude=mLatValue.getText().toString();
				String Longitude=mLongValue.getText().toString();
			    String MIN = MIN_val.getText().toString();
			    String CellId=mCellIdValue.getText().toString();
			    String Lac = mLacValue.getText().toString();
			    String remarksValue = remarks.getText().toString();
			    String ts=Ts.getText().toString();
			    
			   /*Intent email= new Intent(Intent.ACTION_SEND);
			   email.putExtra(Intent.EXTRA_EMAIL,new String[]{"cdplb20@gmail.com"});
			   email.putExtra(Intent.EXTRA_SUBJECT, "Lat-Long-MIN");
			   email.putExtra(Intent.EXTRA_TEXT, MIN+Latitude+Longitude);
			   email.setType("message/rfc822");
			   startActivity(Intent.createChooser(email, "Choose an Email client"));*/
			    
			    String Message= "Timestamp"+ts+"\nMobile No:"+MIN+"\nLatitude:"+Latitude+"\nLongitude:"+Longitude+"\nCellId:"+CellId+"\nLAC:"+Lac+"\nComments:"+remarksValue;
			    String separator = "; ";
		        if(android.os.Build.MANUFACTURER.equalsIgnoreCase("samsung")){
		          separator = ", ";
		        } 
               
		        Intent sendIntent = new Intent(Intent.ACTION_VIEW);
			    sendIntent.putExtra("sms_body",Message);
			    //String address[]={"9819818621","9820018747"};
			    //for(int i=0;i<address.length;i++)
			    sendIntent.putExtra("address", "9819818621"+separator+number);
			    sendIntent.setType("vnd.android-dir/mms-sms");
				startActivity(sendIntent);
				/*try{
				SmsManager smsManager=SmsManager.getDefault();
				smsManager.sendTextMessage("9819818621", null,MIN+Latitude+Longitude, null, null);
				
				Toast.makeText(getApplicationContext(), "SMS Sent!", Toast.LENGTH_LONG).show();
				}
				
				catch(Exception e)
				{
		        Toast.makeText(getApplicationContext(), "SMS Failed,Please try again later!", Toast.LENGTH_LONG).show();
		        e.printStackTrace();
		        System.out.print(e);
				}*/
				}
			
			
	        
		});
		
		Refresh.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
				getLatLong();
				getCellInformation();
				getTimestamp();
				
				
			}
			
		});
		
	  Email.setOnClickListener(new OnClickListener()
	  {

		@Override
		public void onClick(View arg0) {
			// TODO Auto-generated method stub
			
			
			String Latitude=mLatValue.getText().toString();
			String Longitude=mLongValue.getText().toString();
		    String MIN = MIN_val.getText().toString();
		    String CellId=mCellIdValue.getText().toString();
		    String Lac = mLacValue.getText().toString();
		    String remarksValue = remarks.getText().toString();
		    String ts=Ts.getText().toString();
		  String Message= "Timestamp:"+ts+"\nMobile No:"+MIN+"\nLatitude:"+Latitude+"\nLongitude:"+Longitude+"\nCellId:"+CellId+"\nLAC:"+Lac+"\nComments:"+remarksValue;
		  String separator = "; ";
	        if(android.os.Build.MANUFACTURER.equalsIgnoreCase("samsung")){
	          separator = ", ";
	        }
		    
			Intent email= new Intent(Intent.ACTION_SEND);
			email.putExtra(Intent.EXTRA_EMAIL,new String[]{"deepali.bhanushali@vodafone.com"+separator+MailID});
			email.putExtra(Intent.EXTRA_SUBJECT, "LBS Test");
			email.putExtra(Intent.EXTRA_TEXT, Message);
			email.setType("message/rfc822");
			startActivity(Intent.createChooser(email, "Choose an Email client"));	
			}
		 });
	
		getLatLong();
		getCellInformation();
		getTimestamp();
		selectCoordinator();
		
	}
	
	  private void getLatLong() {
		// TODO Auto-generated method stub
		GPSTracker gpsTracker = new GPSTracker(getApplicationContext());
		
		if (gpsTracker.canGetLocation()) {
			
			double lat = gpsTracker.latitude;
			double longi = gpsTracker.longitude;
			this.lat_decimal.setText(""+lat);
			this.long_decimal.setText(""+longi);
			System.out.println(lat);
			System.out.println(longi);
			
			double deg_lat, min_lat, sec_lat;
			double deg_long, min_long, sec_long;
			deg_lat=(int)lat;
			min_lat=(lat-deg_lat)*60;
			sec_lat=(min_lat-(int)min_lat)*60;
			
			this.mLatValue.setText("" +String.format("%02d",(int)deg_lat)+""+String.format("%02d",(int)min_lat)+""+String.format("%02d",(int)sec_lat));
			deg_long=(int)longi;
			min_long=(longi-deg_long)*60;
		    sec_long=(min_long-(int)min_long)*60;
			this.mLongValue.setText("" +String.format("%02d",(int)deg_long)+""+String.format("%02d",(int)min_long)+""+String.format("%02d",(int)sec_long));
			/*mLocationManager = new LocationManager();
			mTelephonyManager.listen(mSignalStrengthListener,
					SignalStrengthListener.LISTEN_SIGNAL_STRENGTHS);*/
		}/*else {
			gpsTracker.showSettingsAlert();
		}*/
		
		/*if(location.hasAccuracy())
		{
		accuracy = location.getAccuracy();
		accuracy_val.setText(""+accuracy);
		}*/
	}
	public void selectCoordinator()
	{
		
		this.coordinator = (RadioGroup)this.findViewById(R.id.Archana);
		
		int selectedId = coordinator.getCheckedRadioButtonId();
		select = (RadioButton) findViewById(selectedId);
		coordinator.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) { 
                select = (RadioButton) findViewById(checkedId);
                String selected = select.getText().toString(); 
                if(selected.trim().equalsIgnoreCase("Archana"))
				{
					
					MailID = "archana.adiacha@vodafone.com";
					number = "9820018747";
					
				}
				else
				{
					MailID = "roamingmumbai@vodafone.com";
					number = "9619816220";
				}
                
                Toast.makeText(getApplicationContext(), MailID+number+selected,2000).show();
				
			}
            
	    });
		
		}
	  public void getTimestamp()
	  {
		    Calendar c = Calendar.getInstance();
			String hh = String.format("%02d",c.get(Calendar.HOUR_OF_DAY));
			String mm = String.format("%02d", c.get(Calendar.MINUTE));
			String ss = String.format("%02d", c.get(Calendar.SECOND));
			int yy = c.get(Calendar.YEAR);
			String month = String.format("%02d", c.get(Calendar.MONTH) + 1);
			String dd = String.format("%02d", c.get(Calendar.DAY_OF_MONTH));
			Ts.setText(String.valueOf(yy)+month+dd+hh+mm+ss);
	  }
	  
	  private void getCellInformation() {
			// TODO Auto-generated method stub
			gsmCellLocation = (GsmCellLocation) this.mTelephonyManager
					.getCellLocation();
			int netType = this.mTelephonyManager.getNetworkType();
			if (netType == 8 || netType == 10 || netType == 15 || netType == 9)
				mCellIdValue.setText(Integer.toString((gsmCellLocation.getCid()) % 65536));
			else
				mCellIdValue.setText(Integer.toString(gsmCellLocation.getCid()));
			mLacValue.setText(Integer.toString(gsmCellLocation.getLac()));
		}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
